<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id');

            $table->string('title', 255);
            $table->text('description');
            $table->decimal('front_price', 10, 2);
            $table->decimal('front_discount', 10, 2)->nullable();
            $table->tinyInteger('status')->default(1)->comment('0 => unpublished, 1 => published');
            $table->tinyInteger('stock_status')->default(0)->comment('0 => out of stock, 1-9 => in stock/limited/.... ');
            $table->string('front_image', 255);
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
