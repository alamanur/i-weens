<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodFieldChangeOtherToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->decimal('amount_recived',  10, 2)->nullable();
            $table->string('payment_type', 50)->default('bkash');

            $table->string('trx_id')->nullable()->change();
            $table->string('trx_phone')->nullable()->change();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn(['amount_recived', 'payment_type']);

            $table->string('trx_id')->nullable(false)->change();
            $table->string('trx_phone')->nullable(false)->change();
        });
    }
}
