<?php

use Illuminate\Database\Seeder;

class ProductImagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_images')->delete();
        
        \DB::table('product_images')->insert(array (
            0 => 
            array (
                'id' => 1,
                'product_id' => 3,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/1504937099iASJfA9hIG.jpeg',
                'thumb_image' => 'images/products/thumb/1504937099sCcpzhkOOS.jpeg',
            ),
            1 => 
            array (
                'id' => 2,
                'product_id' => 4,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/1504937124DrEWwuuIKZ.jpeg',
                'thumb_image' => 'images/products/thumb/1504937124RvCAhbN1fW.jpeg',
            ),
            2 => 
            array (
                'id' => 3,
                'product_id' => 5,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/1504937152myuZVjFEMj.jpeg',
                'thumb_image' => 'images/products/thumb/150493715255tOrPWz8b.jpeg',
            ),
            3 => 
            array (
                'id' => 4,
                'product_id' => 5,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/1504937786HAU14o8peF.jpeg',
                'thumb_image' => 'images/products/thumb/1504937786hVwhbRTG3K.jpeg',
            ),
            4 => 
            array (
                'id' => 5,
                'product_id' => 5,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/1504937786ANDAtU9lVv.jpeg',
                'thumb_image' => 'images/products/thumb/1504937786lie4TlsbJi.jpeg',
            ),
            5 => 
            array (
                'id' => 6,
                'product_id' => 5,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/1504937789x2pvUKd0ju.jpeg',
                'thumb_image' => 'images/products/thumb/1504937789La96szCfdh.jpeg',
            ),
            6 => 
            array (
                'id' => 7,
                'product_id' => 5,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/15049377906L5Aa3uljC.jpeg',
                'thumb_image' => 'images/products/thumb/15049377909JHYejFGLG.jpeg',
            ),
            7 => 
            array (
                'id' => 8,
                'product_id' => 5,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/15049377915I56tRmQWX.jpeg',
                'thumb_image' => 'images/products/thumb/1504937791B5P9Lj2sa0.jpeg',
            ),
            8 => 
            array (
                'id' => 9,
                'product_id' => 4,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/1504937805teIqbd4ebU.jpeg',
                'thumb_image' => 'images/products/thumb/1504937805Esbnieo43a.jpeg',
            ),
            9 => 
            array (
                'id' => 10,
                'product_id' => 4,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/1504937806KyThcASIkd.jpeg',
                'thumb_image' => 'images/products/thumb/1504937806UvzhAjEepl.jpeg',
            ),
            10 => 
            array (
                'id' => 11,
                'product_id' => 4,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/1504937807RdxNkoXgrh.jpeg',
                'thumb_image' => 'images/products/thumb/1504937807e5z3EZjLsO.jpeg',
            ),
            11 => 
            array (
                'id' => 12,
                'product_id' => 4,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/1504937807nlbF9ETclo.jpeg',
                'thumb_image' => 'images/products/thumb/1504937807nxBy5C933v.jpeg',
            ),
            12 => 
            array (
                'id' => 13,
                'product_id' => 4,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/15049378088d1YT4MGSi.jpeg',
                'thumb_image' => 'images/products/thumb/15049378086NwHYLQtWT.jpeg',
            ),
            13 => 
            array (
                'id' => 14,
                'product_id' => 3,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/1504937821QHEWNCgwGn.jpeg',
                'thumb_image' => 'images/products/thumb/1504937821hPWBKCwfcP.jpeg',
            ),
            14 => 
            array (
                'id' => 15,
                'product_id' => 3,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/1504937821BvYiFtF21L.jpeg',
                'thumb_image' => 'images/products/thumb/1504937822gHPG59QOxX.jpeg',
            ),
            15 => 
            array (
                'id' => 16,
                'product_id' => 3,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/1504937823kCoZPrygjH.jpeg',
                'thumb_image' => 'images/products/thumb/15049378235daO3we3jn.jpeg',
            ),
            16 => 
            array (
                'id' => 17,
                'product_id' => 3,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/1504937823R0VbhLEC4J.jpeg',
                'thumb_image' => 'images/products/thumb/1504937823p5sqxHCP4Z.jpeg',
            ),
            17 => 
            array (
                'id' => 18,
                'product_id' => 3,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/1504937823RjdWmQobPr.jpeg',
                'thumb_image' => 'images/products/thumb/1504937824fd3UgCyoJC.jpeg',
            ),
            18 => 
            array (
                'id' => 19,
                'product_id' => 2,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/1504937880a5PunW3Z66.jpeg',
                'thumb_image' => 'images/products/thumb/1504937880qsrJukmciQ.jpeg',
            ),
            19 => 
            array (
                'id' => 20,
                'product_id' => 2,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/1504937880lcfw7B5oDO.jpeg',
                'thumb_image' => 'images/products/thumb/1504937880Au0v4st3pI.jpeg',
            ),
            20 => 
            array (
                'id' => 21,
                'product_id' => 2,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/15049378823vyjYbbE7l.jpeg',
                'thumb_image' => 'images/products/thumb/1504937882s5R0huNXxe.jpeg',
            ),
            21 => 
            array (
                'id' => 22,
                'product_id' => 2,
                'product_stock_id' => NULL,
                'large_image' => 'images/products/large/1504937882btedbGubrk.jpeg',
                'thumb_image' => 'images/products/thumb/15049378825kuXcB0pSx.jpeg',
            ),
        ));
        
        
    }
}