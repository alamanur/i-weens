<?php

use Illuminate\Database\Seeder;

class ProductEntryRecordsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_entry_records')->delete();
        
        \DB::table('product_entry_records')->insert(array (
            0 => 
            array (
                'id' => 1,
                'product_id' => 5,
                'size' => NULL,
                'color' => NULL,
                'quantity' => 10,
                'price' => '3000.00',
                'discount' => NULL,
                'created_at' => '2017-09-09 06:11:39',
                'updated_at' => '2017-09-09 06:11:39',
            ),
            1 => 
            array (
                'id' => 2,
                'product_id' => 4,
                'size' => 'S',
                'color' => NULL,
                'quantity' => 5,
                'price' => '2000.00',
                'discount' => '100.00',
                'created_at' => '2017-09-09 06:12:45',
                'updated_at' => '2017-09-09 06:12:45',
            ),
            2 => 
            array (
                'id' => 3,
                'product_id' => 4,
                'size' => 'M',
                'color' => NULL,
                'quantity' => 5,
                'price' => '2000.00',
                'discount' => '100.00',
                'created_at' => '2017-09-09 06:12:54',
                'updated_at' => '2017-09-09 06:12:54',
            ),
            3 => 
            array (
                'id' => 4,
                'product_id' => 3,
                'size' => NULL,
                'color' => 'Red',
                'quantity' => 7,
                'price' => '5000.00',
                'discount' => NULL,
                'created_at' => '2017-09-09 06:13:17',
                'updated_at' => '2017-09-09 06:13:17',
            ),
            4 => 
            array (
                'id' => 5,
                'product_id' => 3,
                'size' => NULL,
                'color' => 'Black',
                'quantity' => 8,
                'price' => '5000.00',
                'discount' => NULL,
                'created_at' => '2017-09-09 06:13:31',
                'updated_at' => '2017-09-09 06:13:31',
            ),
            5 => 
            array (
                'id' => 6,
                'product_id' => 3,
                'size' => NULL,
                'color' => 'White',
                'quantity' => 4,
                'price' => '5000.00',
                'discount' => NULL,
                'created_at' => '2017-09-09 06:13:49',
                'updated_at' => '2017-09-09 06:13:49',
            ),
            6 => 
            array (
                'id' => 7,
                'product_id' => 2,
                'size' => '38',
                'color' => 'Red',
                'quantity' => 10,
                'price' => '5000.00',
                'discount' => '1000.00',
                'created_at' => '2017-09-09 06:14:27',
                'updated_at' => '2017-09-09 06:14:27',
            ),
            7 => 
            array (
                'id' => 8,
                'product_id' => 2,
                'size' => '40',
                'color' => 'Red',
                'quantity' => 5,
                'price' => '5000.00',
                'discount' => '1000.00',
                'created_at' => '2017-09-09 06:14:40',
                'updated_at' => '2017-09-09 06:14:40',
            ),
            8 => 
            array (
                'id' => 9,
                'product_id' => 2,
                'size' => '42',
                'color' => 'Red',
                'quantity' => 5,
                'price' => '5000.00',
                'discount' => '1000.00',
                'created_at' => '2017-09-09 06:14:49',
                'updated_at' => '2017-09-09 06:14:49',
            ),
            9 => 
            array (
                'id' => 10,
                'product_id' => 2,
                'size' => '38',
                'color' => 'White',
                'quantity' => 5,
                'price' => '5000.00',
                'discount' => '1000.00',
                'created_at' => '2017-09-09 06:15:12',
                'updated_at' => '2017-09-09 06:15:12',
            ),
            10 => 
            array (
                'id' => 11,
                'product_id' => 2,
                'size' => '40',
                'color' => 'White',
                'quantity' => 5,
                'price' => '5000.00',
                'discount' => '1000.00',
                'created_at' => '2017-09-09 06:15:19',
                'updated_at' => '2017-09-09 06:15:19',
            ),
            11 => 
            array (
                'id' => 12,
                'product_id' => 2,
                'size' => '38',
                'color' => 'Black',
                'quantity' => 5,
                'price' => '5000.00',
                'discount' => '1000.00',
                'created_at' => '2017-09-09 06:15:33',
                'updated_at' => '2017-09-09 06:15:33',
            ),
        ));
        
        
    }
}