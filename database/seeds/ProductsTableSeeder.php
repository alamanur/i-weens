<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'category_id' => 1,
                'title' => 'Casio',
                'description' => '<p>Sed lectus. Curabitur at lacus ac velit ornare lobortis. Nullam vel sem. Etiam iaculis nunc ac metus. Fusce risus nisl, viverra et, tempor et, pretium in, sapien.</p>

<p>Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Praesent adipiscing. Phasellus magna. Praesent porttitor, nulla vitae posuere iaculis, arcu nisl dignissim dolor, a pretium mi sem ut ipsum. Pellentesque posuere.</p>

<p>Fusce a quam. Nam pretium turpis et arcu. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Fusce ac felis sit amet ligula pharetra condimentum. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus.</p>

<p>Phasellus a est. Etiam iaculis nunc ac metus. Phasellus consectetuer vestibulum elit. Aenean vulputate eleifend tellus. Quisque malesuada placerat nisl.</p>

<p>Praesent ac massa at ligula laoreet iaculis. Nunc interdum lacus sit amet orci. Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. Praesent venenatis metus at tortor pulvinar varius. Aliquam eu nunc.</p>',
                'front_price' => '2000.00',
                'front_discount' => '100.00',
                'status' => 1,
                'stock_status' => 0,
                'front_image' => 'images/products/front/1504937065egigljHz4w.jpeg',
                'created_at' => '2017-07-23 00:00:00',
                'updated_at' => '2017-09-09 06:04:25',
            ),
            1 => 
            array (
                'id' => 2,
                'category_id' => 1,
                'title' => 'Rolex',
                'description' => '<p>Sed lectus. Curabitur at lacus ac velit ornare lobortis. Nullam vel sem. Etiam iaculis nunc ac metus. Fusce risus nisl, viverra et, tempor et, pretium in, sapien.</p>

<p>Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Praesent adipiscing. Phasellus magna. Praesent porttitor, nulla vitae posuere iaculis, arcu nisl dignissim dolor, a pretium mi sem ut ipsum. Pellentesque posuere.</p>

<p>Fusce a quam. Nam pretium turpis et arcu. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Fusce ac felis sit amet ligula pharetra condimentum. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus.</p>

<p>Phasellus a est. Etiam iaculis nunc ac metus. Phasellus consectetuer vestibulum elit. Aenean vulputate eleifend tellus. Quisque malesuada placerat nisl.</p>

<p>Praesent ac massa at ligula laoreet iaculis. Nunc interdum lacus sit amet orci. Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. Praesent venenatis metus at tortor pulvinar varius. Aliquam eu nunc.</p>',
                'front_price' => '5000.00',
                'front_discount' => '1000.00',
                'status' => 1,
                'stock_status' => 1,
                'front_image' => 'images/products/front/1504937868pWjXMdadX7.jpeg',
                'created_at' => '2017-07-24 00:00:00',
                'updated_at' => '2017-09-09 06:17:48',
            ),
            2 => 
            array (
                'id' => 3,
                'category_id' => 1,
                'title' => 'Lorem ipsum dolor sit amet.',
                'description' => '<p>Sed lectus. Curabitur at lacus ac velit ornare lobortis. Nullam vel sem. Etiam iaculis nunc ac metus. Fusce risus nisl, viverra et, tempor et, pretium in, sapien.</p>

<p>Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Praesent adipiscing. Phasellus magna. Praesent porttitor, nulla vitae posuere iaculis, arcu nisl dignissim dolor, a pretium mi sem ut ipsum. Pellentesque posuere.</p>

<p>Fusce a quam. Nam pretium turpis et arcu. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Fusce ac felis sit amet ligula pharetra condimentum. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus.</p>

<p>Phasellus a est. Etiam iaculis nunc ac metus. Phasellus consectetuer vestibulum elit. Aenean vulputate eleifend tellus. Quisque malesuada placerat nisl.</p>

<p>Praesent ac massa at ligula laoreet iaculis. Nunc interdum lacus sit amet orci. Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. Praesent venenatis metus at tortor pulvinar varius. Aliquam eu nunc.</p>',
                'front_price' => '5000.00',
                'front_discount' => NULL,
                'status' => 1,
                'stock_status' => 1,
                'front_image' => 'images/products/front/15049370992JeztOpNa3.jpeg',
                'created_at' => '2017-09-09 06:04:59',
                'updated_at' => '2017-09-09 06:13:50',
            ),
            3 => 
            array (
                'id' => 4,
                'category_id' => 1,
                'title' => 'Lorem ipsum.',
                'description' => '<p>Sed lectus. Curabitur at lacus ac velit ornare lobortis. Nullam vel sem. Etiam iaculis nunc ac metus. Fusce risus nisl, viverra et, tempor et, pretium in, sapien.</p>

<p>Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Praesent adipiscing. Phasellus magna. Praesent porttitor, nulla vitae posuere iaculis, arcu nisl dignissim dolor, a pretium mi sem ut ipsum. Pellentesque posuere.</p>

<p>Fusce a quam. Nam pretium turpis et arcu. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Fusce ac felis sit amet ligula pharetra condimentum. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus.</p>

<p>Phasellus a est. Etiam iaculis nunc ac metus. Phasellus consectetuer vestibulum elit. Aenean vulputate eleifend tellus. Quisque malesuada placerat nisl.</p>

<p>Praesent ac massa at ligula laoreet iaculis. Nunc interdum lacus sit amet orci. Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. Praesent venenatis metus at tortor pulvinar varius. Aliquam eu nunc.</p>',
                'front_price' => '2000.00',
                'front_discount' => '100.00',
                'status' => 1,
                'stock_status' => 1,
                'front_image' => 'images/products/front/1504937124THmG37fFOB.jpeg',
                'created_at' => '2017-09-09 06:05:24',
                'updated_at' => '2017-09-09 06:12:54',
            ),
            4 => 
            array (
                'id' => 5,
                'category_id' => 1,
                'title' => 'Lorem ipsum dolor.',
                'description' => '<p>Sed lectus. Curabitur at lacus ac velit ornare lobortis. Nullam vel sem. Etiam iaculis nunc ac metus. Fusce risus nisl, viverra et, tempor et, pretium in, sapien.</p>

<p>Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Praesent adipiscing. Phasellus magna. Praesent porttitor, nulla vitae posuere iaculis, arcu nisl dignissim dolor, a pretium mi sem ut ipsum. Pellentesque posuere.</p>

<p>Fusce a quam. Nam pretium turpis et arcu. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Fusce ac felis sit amet ligula pharetra condimentum. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus.</p>

<p>Phasellus a est. Etiam iaculis nunc ac metus. Phasellus consectetuer vestibulum elit. Aenean vulputate eleifend tellus. Quisque malesuada placerat nisl.</p>

<p>Praesent ac massa at ligula laoreet iaculis. Nunc interdum lacus sit amet orci. Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. Praesent venenatis metus at tortor pulvinar varius. Aliquam eu nunc.</p>',
                'front_price' => '3000.00',
                'front_discount' => NULL,
                'status' => 1,
                'stock_status' => 1,
                'front_image' => 'images/products/front/1504937152lr1tkebnvm.jpeg',
                'created_at' => '2017-09-09 06:05:52',
                'updated_at' => '2017-09-09 06:11:39',
            ),
        ));
        
        
    }
}