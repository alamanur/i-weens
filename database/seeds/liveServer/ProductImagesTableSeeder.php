<?php

use Illuminate\Database\Seeder;

class ProductImagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_images')->delete();
        
        \DB::table('product_images')->insert(array (
            0 => 
            array (
                'id' => 69,
                'product_id' => 39,
                'large_image' => 'images/products/large/1503829514BmdszYNsbe.jpeg',
                'thumb_image' => 'images/products/thumb/1503829514HkAQe1lblH.jpeg',
                'product_stock_id' => NULL,
            ),
            1 => 
            array (
                'id' => 71,
                'product_id' => 41,
                'large_image' => 'images/products/large/1503832468h13rRcRmWn.jpeg',
                'thumb_image' => 'images/products/thumb/1503832468vOqVJ6PjqI.jpeg',
                'product_stock_id' => NULL,
            ),
            2 => 
            array (
                'id' => 72,
                'product_id' => 42,
                'large_image' => 'images/products/large/1503834375elNlLZ3ILl.jpeg',
                'thumb_image' => 'images/products/thumb/1503834375U9zRrahY7r.jpeg',
                'product_stock_id' => NULL,
            ),
            3 => 
            array (
                'id' => 73,
                'product_id' => 43,
                'large_image' => 'images/products/large/1503834834guYpzmM0vi.jpeg',
                'thumb_image' => 'images/products/thumb/1503834834iXBFOw7hIU.jpeg',
                'product_stock_id' => NULL,
            ),
            4 => 
            array (
                'id' => 74,
                'product_id' => 44,
                'large_image' => 'images/products/large/1503835813URTs8eywSg.jpeg',
                'thumb_image' => 'images/products/thumb/1503835813T6F7L3fqlQ.jpeg',
                'product_stock_id' => NULL,
            ),
            5 => 
            array (
                'id' => 75,
                'product_id' => 45,
                'large_image' => 'images/products/large/1503836048j7aeZVQAva.jpeg',
                'thumb_image' => 'images/products/thumb/1503836048a0vzdPx8kn.jpeg',
                'product_stock_id' => NULL,
            ),
            6 => 
            array (
                'id' => 76,
                'product_id' => 46,
                'large_image' => 'images/products/large/1503900361tyJ5lUchmH.jpeg',
                'thumb_image' => 'images/products/thumb/1503900361zzmCcAwTay.jpeg',
                'product_stock_id' => NULL,
            ),
            7 => 
            array (
                'id' => 77,
                'product_id' => 47,
                'large_image' => 'images/products/large/1503900815ChqKeRXQq1.jpeg',
                'thumb_image' => 'images/products/thumb/1503900815CHdNfhUybx.jpeg',
                'product_stock_id' => NULL,
            ),
            8 => 
            array (
                'id' => 78,
                'product_id' => 48,
                'large_image' => 'images/products/large/1503901965GroEYDoUBz.jpeg',
                'thumb_image' => 'images/products/thumb/1503901965wL2vhvd5qt.jpeg',
                'product_stock_id' => NULL,
            ),
            9 => 
            array (
                'id' => 79,
                'product_id' => 49,
                'large_image' => 'images/products/large/1503902407KjLc1JpMZp.jpeg',
                'thumb_image' => 'images/products/thumb/1503902407UQH6rEm9G1.jpeg',
                'product_stock_id' => NULL,
            ),
            10 => 
            array (
                'id' => 80,
                'product_id' => 50,
                'large_image' => 'images/products/large/1503903457WqwwSvVlUK.jpeg',
                'thumb_image' => 'images/products/thumb/1503903457DsdgrgHG4k.jpeg',
                'product_stock_id' => NULL,
            ),
            11 => 
            array (
                'id' => 81,
                'product_id' => 51,
                'large_image' => 'images/products/large/15039037870iK0nkCys7.jpeg',
                'thumb_image' => 'images/products/thumb/1503903787TsY9EK8ep2.jpeg',
                'product_stock_id' => NULL,
            ),
            12 => 
            array (
                'id' => 82,
                'product_id' => 52,
                'large_image' => 'images/products/large/1503904436ZOrrHeVuX2.jpeg',
                'thumb_image' => 'images/products/thumb/1503904436xjbhJqNwWM.jpeg',
                'product_stock_id' => NULL,
            ),
            13 => 
            array (
                'id' => 83,
                'product_id' => 53,
                'large_image' => 'images/products/large/15039048226IqqJd98oD.jpeg',
                'thumb_image' => 'images/products/thumb/1503904822kcnGnj1rmm.jpeg',
                'product_stock_id' => NULL,
            ),
            14 => 
            array (
                'id' => 84,
                'product_id' => 54,
                'large_image' => 'images/products/large/1503905291LpoWMrsVk4.jpeg',
                'thumb_image' => 'images/products/thumb/1503905291LmxMkLd7Ve.jpeg',
                'product_stock_id' => NULL,
            ),
            15 => 
            array (
                'id' => 85,
                'product_id' => 55,
                'large_image' => 'images/products/large/1503905684upxXWouTuq.jpeg',
                'thumb_image' => 'images/products/thumb/1503905684nIbvuTu1xz.jpeg',
                'product_stock_id' => NULL,
            ),
            16 => 
            array (
                'id' => 86,
                'product_id' => 56,
                'large_image' => 'images/products/large/1503905852jfRkGyGY8w.jpeg',
                'thumb_image' => 'images/products/thumb/1503905852xDMLRt3MIz.jpeg',
                'product_stock_id' => NULL,
            ),
            17 => 
            array (
                'id' => 87,
                'product_id' => 57,
                'large_image' => 'images/products/large/1503906163h0UgLe8bNL.jpeg',
                'thumb_image' => 'images/products/thumb/1503906163fQ9XWkb3SG.jpeg',
                'product_stock_id' => NULL,
            ),
            18 => 
            array (
                'id' => 88,
                'product_id' => 58,
                'large_image' => 'images/products/large/1503906272NAHFjNtqsK.jpeg',
                'thumb_image' => 'images/products/thumb/1503906272RuUngM4NMV.jpeg',
                'product_stock_id' => NULL,
            ),
            19 => 
            array (
                'id' => 89,
                'product_id' => 59,
                'large_image' => 'images/products/large/1503906387kjDtsqN6Xt.jpeg',
                'thumb_image' => 'images/products/thumb/1503906387JPKVhmqAjr.jpeg',
                'product_stock_id' => NULL,
            ),
            20 => 
            array (
                'id' => 90,
                'product_id' => 60,
                'large_image' => 'images/products/large/1503907436IUJvaveSDA.jpeg',
                'thumb_image' => 'images/products/thumb/1503907436oznwBTjdAx.jpeg',
                'product_stock_id' => NULL,
            ),
            21 => 
            array (
                'id' => 91,
                'product_id' => 61,
                'large_image' => 'images/products/large/1503907546aDAE7wtWV0.jpeg',
                'thumb_image' => 'images/products/thumb/15039075465lahtnNzap.jpeg',
                'product_stock_id' => NULL,
            ),
            22 => 
            array (
                'id' => 92,
                'product_id' => 62,
                'large_image' => 'images/products/large/1503907844ylALQgUNcq.jpeg',
                'thumb_image' => 'images/products/thumb/1503907844oB2vNzMUXL.jpeg',
                'product_stock_id' => NULL,
            ),
            23 => 
            array (
                'id' => 93,
                'product_id' => 63,
                'large_image' => 'images/products/large/1503908173qMZBPdK5eM.jpeg',
                'thumb_image' => 'images/products/thumb/1503908173C8gxHVYnkV.jpeg',
                'product_stock_id' => NULL,
            ),
            24 => 
            array (
                'id' => 94,
                'product_id' => 64,
                'large_image' => 'images/products/large/1503908291tuitzB2MOS.jpeg',
                'thumb_image' => 'images/products/thumb/15039082919qNUxlyxrK.jpeg',
                'product_stock_id' => NULL,
            ),
            25 => 
            array (
                'id' => 95,
                'product_id' => 65,
                'large_image' => 'images/products/large/15039086464aoAPPDugA.jpeg',
                'thumb_image' => 'images/products/thumb/1503908646AVnbBDhWWr.jpeg',
                'product_stock_id' => NULL,
            ),
            26 => 
            array (
                'id' => 96,
                'product_id' => 66,
                'large_image' => 'images/products/large/1503908786KKK7P7yRFL.jpeg',
                'thumb_image' => 'images/products/thumb/1503908786LsM0ES8iIf.jpeg',
                'product_stock_id' => NULL,
            ),
            27 => 
            array (
                'id' => 97,
                'product_id' => 67,
                'large_image' => 'images/products/large/1503914579FLS03x3p1a.jpeg',
                'thumb_image' => 'images/products/thumb/1503914579Y9atOCC9d9.jpeg',
                'product_stock_id' => NULL,
            ),
            28 => 
            array (
                'id' => 98,
                'product_id' => 68,
                'large_image' => 'images/products/large/1503914873QTgrwdtyba.jpeg',
                'thumb_image' => 'images/products/thumb/1503914873ajDhUeSgn0.jpeg',
                'product_stock_id' => NULL,
            ),
            29 => 
            array (
                'id' => 99,
                'product_id' => 69,
                'large_image' => 'images/products/large/15039153962RDhuSXUZj.jpeg',
                'thumb_image' => 'images/products/thumb/1503915396qvVelMBZNe.jpeg',
                'product_stock_id' => NULL,
            ),
            30 => 
            array (
                'id' => 100,
                'product_id' => 70,
                'large_image' => 'images/products/large/1503915592y5Az3Vvi0w.jpeg',
                'thumb_image' => 'images/products/thumb/150391559231vSOsKBFp.jpeg',
                'product_stock_id' => NULL,
            ),
            31 => 
            array (
                'id' => 101,
                'product_id' => 71,
                'large_image' => 'images/products/large/1503915966ERjEd89elT.jpeg',
                'thumb_image' => 'images/products/thumb/1503915966oqbyjnWnOZ.jpeg',
                'product_stock_id' => NULL,
            ),
            32 => 
            array (
                'id' => 102,
                'product_id' => 72,
                'large_image' => 'images/products/large/1503916278DxrFKzWcHP.jpeg',
                'thumb_image' => 'images/products/thumb/15039162783qiSowAhWw.jpeg',
                'product_stock_id' => NULL,
            ),
            33 => 
            array (
                'id' => 103,
                'product_id' => 73,
                'large_image' => 'images/products/large/1503920028DUaP3e6H9I.jpeg',
                'thumb_image' => 'images/products/thumb/1503920028ZhePlw3tDT.jpeg',
                'product_stock_id' => NULL,
            ),
            34 => 
            array (
                'id' => 104,
                'product_id' => 74,
                'large_image' => 'images/products/large/1503920279OLiQx96Tks.jpeg',
                'thumb_image' => 'images/products/thumb/1503920279tttM1cqUpA.jpeg',
                'product_stock_id' => NULL,
            ),
            35 => 
            array (
                'id' => 105,
                'product_id' => 75,
                'large_image' => 'images/products/large/15039207628Lvhf4kgkP.jpeg',
                'thumb_image' => 'images/products/thumb/1503920762lxhNOQSlAj.jpeg',
                'product_stock_id' => NULL,
            ),
        ));
        
        
    }
}