<?php

use Illuminate\Database\Seeder;

class ProductEntryRecordsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('product_entry_records')->delete();
        
        \DB::table('product_entry_records')->insert(array (
            0 => 
            array (
                'id' => 36,
                'product_id' => 39,
                'size' => NULL,
                'color' => 'Black',
                'quantity' => 1,
                'price' => '1700.00',
                'discount' => '200.00',
                'created_at' => '2017-08-27 10:25:56',
                'updated_at' => '2017-08-27 10:25:56',
            ),
            1 => 
            array (
                'id' => 38,
                'product_id' => 41,
                'size' => NULL,
                'color' => 'Black',
                'quantity' => 3,
                'price' => '1600.00',
                'discount' => '200.00',
                'created_at' => '2017-08-27 11:14:54',
                'updated_at' => '2017-08-27 11:14:54',
            ),
            2 => 
            array (
                'id' => 39,
                'product_id' => 42,
                'size' => NULL,
                'color' => 'Green',
                'quantity' => 4,
                'price' => '2600.00',
                'discount' => '400.00',
                'created_at' => '2017-08-27 11:46:52',
                'updated_at' => '2017-08-27 11:46:52',
            ),
            3 => 
            array (
                'id' => 40,
                'product_id' => 43,
                'size' => NULL,
                'color' => 'Coffi',
                'quantity' => 2,
                'price' => '2600.00',
                'discount' => '400.00',
                'created_at' => '2017-08-27 11:54:52',
                'updated_at' => '2017-08-27 11:54:52',
            ),
            4 => 
            array (
                'id' => 41,
                'product_id' => 44,
                'size' => NULL,
                'color' => 'Pink',
                'quantity' => 2,
                'price' => '2600.00',
                'discount' => '400.00',
                'created_at' => '2017-08-27 12:10:49',
                'updated_at' => '2017-08-27 12:10:49',
            ),
            5 => 
            array (
                'id' => 42,
                'product_id' => 46,
                'size' => NULL,
                'color' => 'coffee',
                'quantity' => 5,
                'price' => '2600.00',
                'discount' => '400.00',
                'created_at' => '2017-08-28 06:07:58',
                'updated_at' => '2017-08-28 06:07:58',
            ),
            6 => 
            array (
                'id' => 43,
                'product_id' => 47,
                'size' => NULL,
                'color' => 'Black',
                'quantity' => 5,
                'price' => '2600.00',
                'discount' => '400.00',
                'created_at' => '2017-08-28 06:14:05',
                'updated_at' => '2017-08-28 06:14:05',
            ),
            7 => 
            array (
                'id' => 44,
                'product_id' => 48,
                'size' => NULL,
                'color' => 'Blue',
                'quantity' => 4,
                'price' => '2600.00',
                'discount' => '400.00',
                'created_at' => '2017-08-28 06:33:28',
                'updated_at' => '2017-08-28 06:33:28',
            ),
            8 => 
            array (
                'id' => 45,
                'product_id' => 49,
                'size' => NULL,
                'color' => 'Silver',
                'quantity' => 1,
                'price' => '4000.00',
                'discount' => '700.00',
                'created_at' => '2017-08-28 06:40:57',
                'updated_at' => '2017-08-28 06:40:57',
            ),
            9 => 
            array (
                'id' => 46,
                'product_id' => 50,
                'size' => NULL,
                'color' => 'Yellow',
                'quantity' => 1,
                'price' => '2250.00',
                'discount' => '350.00',
                'created_at' => '2017-08-28 06:58:14',
                'updated_at' => '2017-08-28 06:58:14',
            ),
            10 => 
            array (
                'id' => 47,
                'product_id' => 51,
                'size' => NULL,
                'color' => 'Silver',
                'quantity' => 1,
                'price' => '2250.00',
                'discount' => '350.00',
                'created_at' => '2017-08-28 07:03:35',
                'updated_at' => '2017-08-28 07:03:35',
            ),
            11 => 
            array (
                'id' => 48,
                'product_id' => 52,
                'size' => NULL,
                'color' => 'Blue',
                'quantity' => 1,
                'price' => '2250.00',
                'discount' => '350.00',
                'created_at' => '2017-08-28 07:14:15',
                'updated_at' => '2017-08-28 07:14:15',
            ),
            12 => 
            array (
                'id' => 49,
                'product_id' => 53,
                'size' => NULL,
                'color' => NULL,
                'quantity' => 3,
                'price' => '1650.00',
                'discount' => '250.00',
                'created_at' => '2017-08-28 07:21:26',
                'updated_at' => '2017-08-28 07:21:26',
            ),
            13 => 
            array (
                'id' => 50,
                'product_id' => 52,
                'size' => NULL,
                'color' => NULL,
                'quantity' => 2,
                'price' => '2250.00',
                'discount' => '350.00',
                'created_at' => '2017-08-28 07:24:32',
                'updated_at' => '2017-08-28 07:24:32',
            ),
            14 => 
            array (
                'id' => 51,
                'product_id' => 51,
                'size' => NULL,
                'color' => NULL,
                'quantity' => 2,
                'price' => '2250.00',
                'discount' => '350.00',
                'created_at' => '2017-08-28 07:25:34',
                'updated_at' => '2017-08-28 07:25:34',
            ),
            15 => 
            array (
                'id' => 52,
                'product_id' => 54,
                'size' => NULL,
                'color' => 'Blue',
                'quantity' => 1,
                'price' => '2250.00',
                'discount' => '350.00',
                'created_at' => '2017-08-28 07:29:06',
                'updated_at' => '2017-08-28 07:29:06',
            ),
            16 => 
            array (
                'id' => 53,
                'product_id' => 55,
                'size' => NULL,
                'color' => 'Green',
                'quantity' => 1,
                'price' => '1550.00',
                'discount' => '250.00',
                'created_at' => '2017-08-28 07:35:03',
                'updated_at' => '2017-08-28 07:35:03',
            ),
            17 => 
            array (
                'id' => 54,
                'product_id' => 56,
                'size' => NULL,
                'color' => 'Orange',
                'quantity' => 1,
                'price' => '1550.00',
                'discount' => '250.00',
                'created_at' => '2017-08-28 07:38:07',
                'updated_at' => '2017-08-28 07:38:07',
            ),
            18 => 
            array (
                'id' => 55,
                'product_id' => 57,
                'size' => NULL,
                'color' => 'Black',
                'quantity' => 1,
                'price' => '1550.00',
                'discount' => '250.00',
                'created_at' => '2017-08-28 07:43:12',
                'updated_at' => '2017-08-28 07:43:12',
            ),
            19 => 
            array (
                'id' => 56,
                'product_id' => 58,
                'size' => NULL,
                'color' => 'Red',
                'quantity' => 1,
                'price' => '1550.00',
                'discount' => '250.00',
                'created_at' => '2017-08-28 07:44:55',
                'updated_at' => '2017-08-28 07:44:55',
            ),
            20 => 
            array (
                'id' => 57,
                'product_id' => 59,
                'size' => NULL,
                'color' => 'coffee',
                'quantity' => 1,
                'price' => '1550.00',
                'discount' => '250.00',
                'created_at' => '2017-08-28 07:46:43',
                'updated_at' => '2017-08-28 07:46:43',
            ),
            21 => 
            array (
                'id' => 58,
                'product_id' => 60,
                'size' => NULL,
                'color' => 'coffee',
                'quantity' => 4,
                'price' => '2900.00',
                'discount' => '500.00',
                'created_at' => '2017-08-28 08:04:12',
                'updated_at' => '2017-08-28 08:04:12',
            ),
            22 => 
            array (
                'id' => 59,
                'product_id' => 61,
                'size' => NULL,
                'color' => 'Black',
                'quantity' => 4,
                'price' => '2900.00',
                'discount' => '500.00',
                'created_at' => '2017-08-28 08:07:07',
                'updated_at' => '2017-08-28 08:07:07',
            ),
            23 => 
            array (
                'id' => 60,
                'product_id' => 62,
                'size' => NULL,
                'color' => 'Black',
                'quantity' => 5,
                'price' => '2900.00',
                'discount' => '500.00',
                'created_at' => '2017-08-28 08:11:59',
                'updated_at' => '2017-08-28 08:11:59',
            ),
            24 => 
            array (
                'id' => 61,
                'product_id' => 63,
                'size' => NULL,
                'color' => 'Pink',
                'quantity' => 4,
                'price' => '3300.00',
                'discount' => '500.00',
                'created_at' => '2017-08-28 08:16:43',
                'updated_at' => '2017-08-28 08:16:43',
            ),
            25 => 
            array (
                'id' => 62,
                'product_id' => 65,
                'size' => NULL,
                'color' => 'Black',
                'quantity' => 5,
                'price' => '3300.00',
                'discount' => '500.00',
                'created_at' => '2017-08-28 08:25:00',
                'updated_at' => '2017-08-28 08:25:00',
            ),
            26 => 
            array (
                'id' => 63,
                'product_id' => 66,
                'size' => NULL,
                'color' => 'White',
                'quantity' => 5,
                'price' => '3300.00',
                'discount' => '500.00',
                'created_at' => '2017-08-28 08:27:10',
                'updated_at' => '2017-08-28 08:27:10',
            ),
            27 => 
            array (
                'id' => 64,
                'product_id' => 67,
                'size' => NULL,
                'color' => 'pink/ladies',
                'quantity' => 3,
                'price' => '3300.00',
                'discount' => '500.00',
                'created_at' => '2017-08-28 10:03:32',
                'updated_at' => '2017-08-28 10:03:32',
            ),
            28 => 
            array (
                'id' => 65,
                'product_id' => 68,
                'size' => NULL,
                'color' => 'coffee/ladies',
                'quantity' => 4,
                'price' => '3300.00',
                'discount' => '500.00',
                'created_at' => '2017-08-28 10:08:34',
                'updated_at' => '2017-08-28 10:08:34',
            ),
            29 => 
            array (
                'id' => 66,
                'product_id' => 69,
                'size' => NULL,
                'color' => 'black/ladies',
                'quantity' => 4,
                'price' => '3300.00',
                'discount' => '500.00',
                'created_at' => '2017-08-28 10:16:58',
                'updated_at' => '2017-08-28 10:16:58',
            ),
            30 => 
            array (
                'id' => 67,
                'product_id' => 70,
                'size' => NULL,
                'color' => 'white/ladies',
                'quantity' => 4,
                'price' => '3300.00',
                'discount' => '500.00',
                'created_at' => '2017-08-28 10:20:18',
                'updated_at' => '2017-08-28 10:20:18',
            ),
            31 => 
            array (
                'id' => 68,
                'product_id' => 71,
                'size' => NULL,
                'color' => '3 color',
                'quantity' => 12,
                'price' => '3300.00',
                'discount' => '500.00',
                'created_at' => '2017-08-28 10:26:45',
                'updated_at' => '2017-08-28 10:26:45',
            ),
            32 => 
            array (
                'id' => 69,
                'product_id' => 72,
                'size' => NULL,
                'color' => 'black/blue',
                'quantity' => 1,
                'price' => '3300.00',
                'discount' => '500.00',
                'created_at' => '2017-08-28 10:31:44',
                'updated_at' => '2017-08-28 10:31:44',
            ),
            33 => 
            array (
                'id' => 70,
                'product_id' => 73,
                'size' => NULL,
                'color' => 'coffee',
                'quantity' => 4,
                'price' => '5200.00',
                'discount' => '1000.00',
                'created_at' => '2017-08-28 11:36:12',
                'updated_at' => '2017-08-28 11:36:12',
            ),
            34 => 
            array (
                'id' => 71,
                'product_id' => 74,
                'size' => NULL,
                'color' => 'Black',
                'quantity' => 1,
                'price' => '5200.00',
                'discount' => '1000.00',
                'created_at' => '2017-08-28 11:38:13',
                'updated_at' => '2017-08-28 11:38:13',
            ),
            35 => 
            array (
                'id' => 72,
                'product_id' => 75,
                'size' => NULL,
                'color' => 'White',
                'quantity' => 4,
                'price' => '2900.00',
                'discount' => '500.00',
                'created_at' => '2017-08-28 11:46:19',
                'updated_at' => '2017-08-28 11:46:19',
            ),
            36 => 
            array (
                'id' => 73,
                'product_id' => 64,
                'size' => NULL,
                'color' => 'Pink',
                'quantity' => 4,
                'price' => '3300.00',
                'discount' => '500.00',
                'created_at' => '2017-08-28 12:03:12',
                'updated_at' => '2017-08-28 12:03:12',
            ),
            37 => 
            array (
                'id' => 74,
                'product_id' => 49,
                'size' => NULL,
                'color' => 'silver/L',
                'quantity' => 1,
                'price' => '4200.00',
                'discount' => '1000.00',
                'created_at' => '2017-08-29 07:44:30',
                'updated_at' => '2017-08-29 07:44:30',
            ),
        ));
        
        
    }
}