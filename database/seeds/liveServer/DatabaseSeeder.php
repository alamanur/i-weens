<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(ProductImagesTableSeeder::class);
        $this->call(ProductStocksTableSeeder::class);
        $this->call(ProductEntryRecordsTableSeeder::class);
    } 
    // art iseed categories,products,product_images,product_stocks,product_entry_records,users
}
