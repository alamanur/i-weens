<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 3,
                'name' => 'alam',
                'phone' => '1234567890',
                'email' => 'alam@demo.go',
                'password' => 'alam@demo.go',
                'remember_token' => NULL,
                'created_at' => '2017-08-09 19:00:00',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 4,
                'name' => 'alam2',
                'phone' => '123456789',
                'email' => 'alam2@demo.go',
                'password' => 'apatoto nai',
                'remember_token' => NULL,
                'created_at' => '2017-08-09 19:00:00',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}