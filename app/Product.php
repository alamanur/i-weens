<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $guarded = [];

	public function images()
    {
        return $this->hasMany(\App\ProductImage::class);
    }

	public function manage()
    {
        return $this->hasMany(\App\ManageProduct::class);
    }

	public function category()
    {
        return $this->belongsTo(\App\Category::class);
    }
}
