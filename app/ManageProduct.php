<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManageProduct extends Model
{
    protected $table = 'product_stocks';
    protected $guarded = [];
    // protected $fillable = ['discount'];
	// public $timestamps = false;

	public function product()
	{
		return $this->belongsTo(\App\Product::class);
	}

	// public function entryRecord()
	// {
	// 	return $this->hasMany(\App\ProductEntryRecord::class, 'product_stock_id');
	// }
}
