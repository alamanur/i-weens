<?php

namespace App\Http\Middleware;

use Closure;
use Cart;

class RedirectEmptyCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( Cart::content()->count() < 1 )
            return redirect('my-cart')->with('error', "Your cart is empty");
        return $next($request);
    }
}
