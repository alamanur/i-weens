<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductImage;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;


class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "All Products";
        $products = Product::with('category')->latest()->paginate(111);

        return view('backend.Products.index', compact('products', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $categories = \App\Category::all();
        return view('backend.Products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */

    public function store()
    {
        $this->validateWhileCreate();
       
        $frontImage = $this->storeImage(request('front_image'), '/images/products/front/', 230, 230);
        $thumbImage = $this->storeImage(request('front_image'), '/images/products/thumb/', 60, 60);
        $largeImage = $this->storeImage(request('front_image'), '/images/products/large/', 800, 800);
        
        $product = Product::create([
                'category_id' => request('category_id'),
                'title'       => request('title'),
                'description' => request('description'),
                'front_price' => request('front_price'),
                'front_discount' => request('front_discount'),
                'front_image' => $frontImage,
                            ])->images()->create([
                                    'large_image' => $largeImage, 
                                    'thumb_image' => $thumbImage
                                ]);

        return back()->with("success", "Successfully created product {$product->title}");        
    }

    /**
     * Validating a newly created Product data. 
     */
    public function validateWhileCreate()
    {
        $this->validate(request(), [
                "category_id" => "required|integer",
                "title" => "required",
                "description" => "required",
                "front_price" => "required|numeric",
                "front_discount" => "nullable|numeric|max:".request('front_price'),
                // "front_image" => "required|image|max:1500|dimensions:ratio=1/1",
                "front_image" => "required|image|max:1500",
            ], [
                "category_id.required" => "A Category is Required"
            ]);
    }

    /**
    * Store file
    * @param $file  request()->file('name')
    * @param $location  image location with forward slashes
    * @param $width  width of the resized image
    * @param $height  width of the resized image
    * @return path to the saved image
    *
    */
    public function storeImage($file, $location, $width, $height)
    {        
        $ext= $file->guessClientExtension();
        $path = trim($location, '/').'/'.time().str_random(10). '.' . $ext;
        // ini_set('memory_limit','500M');
        if(Image::make($file)
                ->resize($width, $height)
                ->save($path)
            )
            return $path;
    }

    

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        // dd($product);
        $categories = \App\Category::all();
        return view('backend.Products.edit', compact('categories', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Product $product)
    {
        $this->ValidateWhileUpadte();
        $product->category_id = request('category_id');
        $product->title = request('title');
        $product->description = request('description');
        $product->front_price = request('front_price');
        $product->front_discount = request('front_discount');
        if(request()->hasFile('front_image'))
        {
            if(file_exists($product->front_image)) 
                unlink($product->front_image);         
            $product->front_image 
            = $this->storeImage(request('front_image'), '/images/products/front/', 230, 230);
        }
        $product->save();

        return redirect()
                ->route('products.index')
                ->with('success', "successfully updated product {$product->title}");
    }

    /**
     * Validation for Product Update. 
     */
    public function ValidateWhileUpadte()
    {
        $this->validate(request(), [
                "category_id" => "required|integer",
                "title" => "required",
                "description" => "required",
                "front_price" => "required|numeric",
                "front_discount" => "nullable|numeric|max:".request('front_price'),
                // "front_image" => "image|max:1500|dimensions:ratio=1/1",
                "front_image" => "image|max:1500",
            ], [
                "category_id.required" => "A Category is Required"
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        return "this functionality is pending";
    }

    public function gallery(Product $product)
    {
        if(request()->ajax())
        {
            if( request()->hasFile('image'))
            {
                $this->validate(request(), [
                    'image' => 'required|image|max:1500',
                    ]);

                $image = \App\ProductImage::create([
                    'product_id' => $product->id,
                    'large_image' => $this->storeImage(request('image'), '/images/products/large/', 800, 800),
                    'thumb_image' => $this->storeImage(request('image'), '/images/products/thumb/', 60, 60),
                ]);
                return response()
                    ->json([
                        'message' => 'Successfully uploaded Gallery Image',
                        'src'   => '
                                  <div class="col-md-2" id="image_' . $image->id . '">
                                    <div class="thumbnail relative">              
                                        <img src="' . asset($image->large_image) . '" style="width:100%">
                                        <div class="caption text-center">                
                                          <span href="#" class="abs-btn" onclick="remove_image(\''. route('products.gallery.remove', ['image' => $image->id ]) . '\', \'#image_' . $image->id . '\')"><i class="fa fa-trash fa-2x text-red"></i></span>               
                                        </div>              
                                    </div>
                                  </div>', 
                     ]);
            }
            
        }
        $title = "Manage Gallery";
        $product->load('images');
        // dd($product);
        return view('backend.Products.gallery', compact('product', 'title'));
    }

    public function remove(ProductImage $image)
    {
        if(request()->ajax())
        {
            if($image->delete())
            {
                if(file_exists($image->large_image)) 
                    unlink($image->large_image); 
                if(file_exists($image->thumb_image)) 
                    unlink($image->thumb_image); 
                return response()
                       ->json(['message' => "Successfully deleted"]);
            }
            return response()
                       ->json(['message' => "error"]);    
        }
    }
}
