<?php

namespace App\Http\Controllers;

use App\Order;

class OrderHistoriesController extends Controller
{
    public function orderHistory()
    {
        $orders = Order::where( 'user_id', auth()->user()->id )
            // ->with('purchases')
            ->select('id', 'created_at')
            ->get();
        return view('web.OrderHistories.order-history', compact('orders'));
    }

    public function orderDetails(Order $order)
    {
        // return $order->load('purchases', 'shipping');
        $order->load('purchases', 'shipping');
        return view('web.OrderHistories.details', compact('order'));
    }

    public function shippingInfo(Order $order)
    {
        # code...
    }
}
