<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use App\User;
use Auth;

class SocialAuthController extends Controller
{
    // protected $social;
    protected $socialUser;
    /**
     * Redirect the user to the Social authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($social)
    {
        return Socialite::driver($social)->redirect();
    }

    /**
     * Handle a user after authenticating from social media
     *
     * @return Response
     */
    public function handleProviderCallback($social)
    {
        $this->socialUser = Socialite::driver($social)->user(); //try-catch might be a good idea
        $user = User::where( "{$social}_id", $this->socialUser->id )->first();
        if( ! $user )
            $user = $this->create($social);
        return $this->loginUser($user->id);
        // return redirect()->route('login')->with('errorr', 'some');
    }

    /**
     * Create a new user instance with Social media information.
     *
     * @param  string  $social  facebook|google
     * @return \App\User
     */
    protected function create($social)
    {
        $social_id = $social . '_id';
        $user = new User;
        $user->$social_id = $this->socialUser->id;
        $user->name = $this->socialUser->name;
        if( $this->socialUser->email )
            $user->email = $this->socialUser->email;
        $user->save();
        return $user;
    }

    protected function loginUser($id)
    {        
        if( Auth::loginUsingId($id) )
            return redirect()->intended('/');
            // return "redirect('/')";
    }
}
