<?php

namespace App\Http\Controllers;

use Cart;
use DB;
use App\ManageProduct;
use App\Product;
use App\Http\Middleware\RedirectEmptyCart;
use Illuminate\Support\Facades\Validator;

class CheckoutsController extends Controller
{
     public function __construct()
    {
        $this->middleware(RedirectEmptyCart::class)
            ->except(['shipping', 'storeShipping']);
    }

    public function shipping()
    {
        $cart = Cart::content();
        return view('web.shipping', compact('cart') );
    }

    public function storeShipping()
    {
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email',
            'contact_no' => 'required',
            'address' => 'required',
            'zip_code' => 'required|numeric',
            'city' => 'required'
        ]);
        $shippingInfo = [
            'name' => request('name'),
            'email' => request('email'),
            'contact_no' => request('contact_no'),
            'address' => request('address'),
            'zip_code' => request('zip_code'),
            'city' => request('city'),
            'country' => 'Bangladesh'
        ];
        session(['shippingInfo' => $shippingInfo]);
        // dd( session('shippingInfo') );
        return redirect('payment');
    }

    public function payment()
    {
        if( ! session('shippingInfo') )
            return redirect('shipping');
        $cart = Cart::content();
        // dd( session('shippingInfo') ); 
        return view('web.payment', compact('cart') );
    }

    /**
     * Get a validator for storing Payment Info in Session.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function storePaymentValidator(array $data)
    {
        $v = Validator::make($data, [
            'payment_type' => 'required'
        ]);
        $v->sometimes(['trx_id', 'trx_phone'], 'required', function ($input) {
            return $input->payment_type == 'bkash';
        });
        return $v;
    }


    public function storePayment()
    {
        $this->storePaymentValidator(request()->all())->validate();
        $paymentInfo = [
            'payment_type' => request('payment_type'),
            'trx_id' => request('trx_id'),
            'trx_phone' => request('trx_phone')
        ];
        session(['paymentInfo' => $paymentInfo]);
        return redirect('review-order');
    }

    public function reviewOrder()
    {
        if( ! session('paymentInfo') )
            return redirect('payment');
        $cart = Cart::content();
        $total = 0;
        foreach($cart as $item):
            if( $item->options->discount )
                $total += ( ($item->price - $item->options->discount) * (int) $item->qty );
            else
                $total += ($item->price * (int) $item->qty);
        endforeach;
        return view('web.review-order', compact('cart', 'total') );
    }

    public function confirmOrder()
    {
        if( ! session('paymentInfo') )
            return redirect('payment');
        $cart = Cart::content();
        DB::beginTransaction();
        // try {
        $product_stocks_ids = $cart->pluck('id')->toArray();
        $collection = ( ManageProduct::find($product_stocks_ids) )->load('product');
        // start transaction
        $order = \App\Order::create( $this->sessionOrderInfo() );
        $order->purchases()
            ->createMany( $this->purchasedItems($collection, $cart) );
        $order->shipping()
            ->create( $this->sessionShippingInfo() );                
            \DB::commit();
            session()->forget(['paymentInfo', 'shippingInfo']);
            Cart::destroy();
            return redirect('/')->with( 'success', "Ordered successfully" );            
        // } catch (\Exception $e) {
            // dump($e->getMessage());
            \DB::rollBack();
            return redirect('my-cart')->with('error', "Order Failed. " . $e->getMessage() );
        // }
    }

    /**
     * Collection of array, ready to insert in purchase table 
     *      >>>with changing >> status in product_stock_table
     *                       >> stock_status in product table

     * @param  collection  $collection
     * @param  collection  $cart
     * @return Collection of array, ready to insert in purchase table
     */

    public function purchasedItems($collection, $cart)
    {
        return ( $collection->map( function ($item, $key) use ($cart) {
            // find the cart item quantity for this collection item
            $quantity = (int) ( $cart->where('id', $item->id)->first() )->qty;
            if(!$quantity)
                throw new Exception("Invalid cart Quantity");
            // checkage for original quantity of product
            if( $quantity > $item->quantity )
                throw new Exception("Only " . $item->quantity . " No of this product is available");
            // updating quantity in status table
            ManageProduct::where('id', $item->id)
                ->update(['quantity' => ( $item->quantity - $quantity ) ]);
            // updating Product table stock_status || more conditions will apply alter (i.e: almost gone, nearly finished...) 
            if( ( $item->quantity - $quantity ) < 1 )
                Product::where('id', $item->product_id)
                    ->update(['stock_status' => 0 ]);
            return [
                'product_id' => $item->product_id,
                'product_title' => $item->product->title,
                'size' => $item->size,
                'color' => $item->color,
                'quantity' => $quantity,
                'price' => $item->price,
                'discount' => $item->discount,
            ];
        }) )->all();
    }

    public function sessionShippingInfo()
    {
        return [
            'name' => session('shippingInfo.name'),
            'address' => session('shippingInfo.address'),
            'contact_no' => session('shippingInfo.contact_no'),
            'email' => session('shippingInfo.email'),
            'zip_code' => session('shippingInfo.zip_code'),
            'city' => session('shippingInfo.city'),
            'country' => session('shippingInfo.country')
        ];
    }

    public function sessionOrderInfo()
    {
        return [
            'user_id'       => auth()->user()->id,
            'payment_type'  => session('paymentInfo.payment_type'),
            'trx_id'        => ( session('paymentInfo.payment_type') == 'bkash' ) ? session('paymentInfo.trx_id') : null,
            'trx_phone'     => ( session('paymentInfo.payment_type') == 'bkash' ) ? session('paymentInfo.trx_phone') : null,
        ];
    }

}
    