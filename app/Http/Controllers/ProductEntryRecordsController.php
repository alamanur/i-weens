<?php

namespace App\Http\Controllers;

use App\ProductEntryRecord;
use Illuminate\Http\Request;

class ProductEntryRecordsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = ProductEntryRecord::with('product')->latest()->paginate(5);
        // return $products;
        $title = 'Product Entry Records';
        return view('backend.ProductEntryRecords.index', compact('title', 'products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductEntryRecord  $ProductEntryRecord
     * @return \Illuminate\Http\Response
     */
    public function show(ProductEntryRecord $ProductEntryRecord)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductEntryRecord  $ProductEntryRecord
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductEntryRecord $ProductEntryRecord)
    {
        $ProductEntryRecord->load('product');
        // dd($product->product->title);
        $title = "Update Product Entry : {$ProductEntryRecord->product->title}";
        return view('backend.ProductEntryRecords.edit', compact('ProductEntryRecord', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductEntryRecord  $ProductEntryRecord
     * @return \Illuminate\Http\Response
     */
    public function update(ProductEntryRecord $ProductEntryRecord)
    {
        $this->validate(
            request(), 
            [
                'quantity' => 'integer|min:0',
                "price" => "required|numeric",
                "discount" => "nullable|numeric|max:".request('price')
            ], 
            [
                'quantity.min' => 'Quantity Must be greater than zero'
            ]
        );
        $manage = \App\ManageProduct::where('product_id', $ProductEntryRecord->product_id)
                            ->where('size', $ProductEntryRecord->size)     
                            ->where('color', $ProductEntryRecord->color)
                            ->first();
        $decrease = $ProductEntryRecord->quantity - request('quantity');
        // dd($decrease);
        if($manage && ($manage->quantity >= $decrease))
        {
            \DB::beginTransaction();
            try {                    
                $ProductEntryRecord->quantity -= $decrease; 
                $ProductEntryRecord->price     = request('price'); 
                $ProductEntryRecord->discount  = request('discount'); 
                $ProductEntryRecord->save();

                $manage->quantity -= $decrease;
                $manage->price     = request('price'); 
                $manage->discount  = request('discount');                 
                $manage->save();

                \DB::commit();
                return back()->with('success', "Successfully Updated");
                
            } catch (\ception $e) {
                \DB::rollBack();
                return back()->with('warning', 'Something went wrong!!');
            }
        };
        // dd($decrease);
        return back()->with('warning', 'Something went wrong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductEntryRecord  $ProductEntryRecord
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductEntryRecord $ProductEntryRecord)
    {
        //
    }
}
