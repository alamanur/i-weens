<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class SettingsController extends Controller
{
    public function index()
    {
        $user = User::find( auth()->user()->id );
        return view('web.User.setting', compact('user'));
    }
    /**
     * Get a validator for an incoming Update request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => [
                'required',
                'string', 
                'max:255',
                Rule::unique('users')->ignore( auth()->user()->id ),
            ],
            'phone' => [
                'required',
                'string', 
                'max:15',
                Rule::unique('users')->ignore( auth()->user()->id ),
            ],
        ]);
    }

    /**
     * Update Authenticated User after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function update(array $data)
    {
        return User::where('id', auth()->user()->id )
            ->update([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
        ]);
    }

    public function updateUser()
    {
        $this->validator( request()->all() )->validate();
        if ($this->update( request()->all() ))
            return back()->with('success', "successfully updated");
        return back()->with('error', "something went wrong");
    }
}
