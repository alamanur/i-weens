<?php

namespace App\Http\Controllers;

use Cart;
use App\ManageProduct;

class CartsController extends Controller
{
    public function myCart(ManageProduct $manage)
    {
        $cart = Cart::content();
        $cartProductMaxQuantity = (
                ManageProduct::whereIn(
                        'id', 
                        ($cart->pluck('id'))->all() 
                    )
                    ->select('id', 'quantity')
                    ->pluck('quantity', 'id')
            )->toJson();
        return view('web.cart', compact('cart', 'cartProductMaxQuantity'));
    }

    public function cart()
    {
        // if(request()->ajax())
            return Cart::content();
    }

    public function add(ManageProduct $manage)
    {
        // if(request()->ajax())
        // {
           $manage->load('product');
            // dd($manage);
            Cart::add($manage->id, $manage->product->title, 1, $manage->price, [
                        'discount'  => (int) $manage->discount,
                        'image'     => $manage->product->front_image,
                        'size'     => $manage->size,
                        'color'     => $manage->color,
                        // 'rating'    => pending
                    ]);
            return Cart::content();            
        // }
    
    }

    public function update($rowId, $quantity)
    {
        // if(request()->ajax())
        // {
            Cart::update($rowId, $quantity);
            return Cart::content();            
        // }
    }

    public function delete($rowId)
    {
        // if(request()->ajax())
        // {   
            Cart::remove($rowId);
            return Cart::content();
        // }
    }

    public function destroy()
    {
        // if(request()->ajax())
        // {
            Cart::destroy();
            return Cart::content();
        // }
    }
}
