<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('backend.Categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.Categories.category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'name' => 'required|unique:categories,name'
            ], [
                // 'name.required' => 'here u go'
            ]);
        $category = Category::create(['name' => $request->name]);
        return back()->with('success', "Successfully added {$category->name}");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        // dd($category);
        return view('backend.Categories.edit', compact('category'))->withTitle('Edit Category');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Category $category)
    {
        $this->validate(request(), ['name' => 'unique:categories,name']);
        $category->update(['name' => request('name')]);
        return redirect()->route('categories.index')->with("success", "Successfully updated {$category->name}");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if($category->product()->count() > 0 )
            return back()->with('danger', 'Can\'t be deleted as this category have products');

        $category->delete();
        return back()->with('success', "successfully deleted");
    }
}
