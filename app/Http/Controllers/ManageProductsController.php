<?php

namespace App\Http\Controllers;

use App\ManageProduct;
use App\Product;
use App\ProductEntryRecord;
use App\ProductImage;
use Intervention\Image\ImageManagerStatic as Image;

class ManageProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     $title = 'Manage Product';
    //     // $products = \DB::table('product_stocks')
    //     //                  ->groupBy('product_id')
    //     //                 ->get();
    //     // dd($products);
    //     return view('backend.ManageProducts.index', compact('products', 'title'));
    // }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $manageProduct
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $product->load('images', 'manage');
        // $product = Product::with('images', 'manage')->find($product_id);
        // $manageProduct = $manageProduct::with('images', 'manage')->get();
        // dd($product->toArray());

        $title = "Manage Product: {$product->title}";
        return view('backend.ManageProducts.show', compact('product', 'title'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Product $product)
    {
        $title = "Manage Product : {$product->title}";
        return view('backend.ManageProducts.create', compact('product', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        // dd(request()->all());
        $this->ValidateStoreData();

        \DB::beginTransaction();

        try {
            $manageProduct = ManageProduct::firstOrNew(
                    ["product_id" => request('product_id'),
                    "size" => request('size'),
                    "color" => request('color')]
                );
            $manageProduct->quantity += request('quantity');
            $manageProduct->price = request('price');
            $manageProduct->discount = request('discount');
            $manageProduct->save();

            // dd($manageProduct->id);

            $productEntryRecord = ProductEntryRecord::create([
                    "product_id" => request('product_id'),
                    "size" => request('size'),
                    "color" => request('color'),
                    "quantity" => request('quantity'),
                    "price" => request('price'),
                    "discount" => request('discount'),
                ]);

            if(request('quantity') > 0) //||||||||||||need to modify
                Product::where('id', request('product_id'))->update(['stock_status' => 1]);

            if(request()->hasFile('image'))
                $this->insertOrUpdateImage($manageProduct->id);

            \DB::commit();
            return redirect()
                    ->route('manage.show', ['product' => request('product_id')])
                    ->with('success', 'Successfully inserted');

        } catch (\Exception $e) {
            \DB::rollBack();
            return back()->with('danger', 'Something went wrong, try again!!!');
        }
    }


    /**
     * Validating a newly created Product data.
     */

    public function ValidateStoreData()
    {
        $this->validate(request(), [
                "product_id" => "required|integer",
                "size" => "nullable",
                "color" => "nullable",
                "quantity" => "required|integer",
                "price" => "required|numeric",
                "discount" => "nullable|numeric|max:".request('price'),
                // "image" => "image|max:1500|dimensions:ratio=1/1",
                "image" => "image|max:1500",
            ], [
                "product_id.required" => "Product Is required"
            ]);
    }
    /**
    * Resize and Store Image
    * @param $file  request()->file('name')
    * @param $location  image location with forward slashes
    * @param $width  width of the resized image
    * @param $height  width of the resized image
    * @return path to the saved image
    *
    */
    public function storeImage($file, $location, $width, $height)
    {
        $ext= $file->guessClientExtension();
        $path = trim($location, '/').'/'.time().str_random(10). '.' . $ext;
        // ini_set('memory_limit','500M');
        if(Image::make($file)
                ->resize($width, $height)
                ->save($path)
            )
            return $path;
    }

    public function insertOrUpdateImage($id)
    {
            // dd("4");
        $image = \App\ProductImage::firstOrNew([
                    'product_id'        => request('product_id'),
                    // 'size'              => request('size'),
                    // 'color'             => request('color'),
                    'product_stock_id'  => $id
                ]);

        //assigning image locations to a variable to delete after insertion
        $largeImage = $image->large_image;
        $thumbImage = $image->thumb_image;


        $image->large_image = $this->storeImage(request('image'), '/images/products/large/', 800, 800);
        $image->thumb_image = $this->storeImage(request('image'), '/images/products/thumb/', 60, 60);
        $image->save();
        //checking for old image and deleting after insertion
        if($largeImage && file_exists($largeImage))
            unlink($largeImage);
        if($thumbImage && file_exists($thumbImage))
            unlink($thumbImage);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ManageProduct  $manageProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(ManageProduct $manage)
    {
        // dd($manage);
        $manageProduct = $manage->load('product');
        // dd($manageProduct);
        $image = \App\ProductImage::where('product_id', $manageProduct->product_id)
                                    ->where('product_stock_id', $manageProduct->id)
                                    ->first();
        // dd($image);
        $title = "Edit Product Options : {$manageProduct->product->title}";

        return view('backend.ManageProducts.edit', compact('manageProduct', 'image', 'title'));
    }

    public function increase(ManageProduct $manage)
    {
        $manageProduct = $manage->load('product');
        // dd($manageProduct);
        $image = \App\ProductImage::where('product_id', $manageProduct->product_id)
                                    ->where('product_stock_id', $manageProduct->id)
                                    // ->where('size', $manageProduct->size)
                                    // ->where('color', $manageProduct->color)
                                    ->first();
        // dd($image);
        $title = "Add Product quantity : {$manageProduct->product->title}";

        return view('backend.ManageProducts.increase', compact('manageProduct', 'image', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ManageProduct  $manageProduct
     * @return \Illuminate\Http\Response
     */
    public function update(ManageProduct $manage)
    {
        $this->validate(request(), [
                // 'size' => 'required',
                // 'color' => 'required',
                'product_id' => 'required',
                'image_id' => 'required',
                "price" => "required|numeric",
                'image' => 'image|max:1500'
            ]);
        // dd(request()->all());
        \DB::beginTransaction();
        try {
            $manage->size = request('size');
            $manage->color = request('color');
            $manage->price = request('price');
            $manage->discount = request('discount');

            $image = ProductImage::find(request('image_id'));
            $image->size = request('size');
            $image->color = request('color');

            //storing old images to variable to delete later
            $largeImage = $image->largeImage;
            $thumbImage = $image->thumbImage;

            if(request()->hasFile('image'))
            {
                $image->large_image = $this->storeImage(request('image'), '/images/products/large/', 800, 800);
                $image->thumb_image = $this->storeImage(request('image'), '/images/products/thumb/', 60, 60);
            }
            $manage->save();
            $image->save();
            \DB::commit();
            //checking for old image and deleting after insertion
            if($largeImage && file_exists($largeImage))
                unlink($largeImage);
            if($thumbImage && file_exists($thumbImage))
                unlink($thumbImage);

            return back()->with('success', 'Successfully Updated');
        } catch (\Exception $e) {
            \DB::rollBack();
            return back()->with('danger', 'Something went wrong, try again!!!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ManageProduct  $manageProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(ManageProduct $manageProduct)
    {
        //
    }
}
