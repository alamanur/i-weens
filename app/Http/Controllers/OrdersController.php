<?php

namespace App\Http\Controllers;

use App\Order;
use App\Purchase;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::with('user')->latest()->get();
        // dd($orders);
        $title = "Orders";
        return view('backend.Orders.index', compact('orders', 'title'));
    }

    public function show($order_id)
    {
        // dd($customer_order);
        $purchases = Purchase::where('order_id', $order_id)->get();
        $purchases->load('product_stock');
        // dd($purchases);     
        $title = "Detail of Order id: {$order_id}";
        return view('backend.Orders.show', compact('purchases', 'title'));
    }

    public function shipping(Order $customer_order)
    {
        $customer_order->load('shipping');
        $title = "Shipping Address for Order No : {$customer_order->id}";
        return view('backend.Orders.shipping', compact('customer_order', 'title'));
    }

}
