<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;

class HomeController extends Controller
{

    /**
     * Show the Home Page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::where('stock_status', 1)
            ->where('status', 1)
            ->latest()
            ->paginate(1)
            ->toJson();
        if(request()->ajax()) //|||||||||temp api
            return response($products);
        return view('_welcome', compact('products'));
        // return view('web.welcome', compact('products'));
    }

    /**
     * Show Related Products
     *
    */
    public function relatedProducts($categoryId)
    {
        if(request()->ajax()) //|||||||||temp api
        {
            return response()->json( 
                Product::where('category_id', $categoryId)
                    ->latest()
                    ->paginate(2)
            );
        }
    }
    /**
     * Show Product Detail
     *
    */
    public function detail(Product $product)
    {
        $relatedProducts = Product::where('category_id', $product->category_id)
            ->latest()
            ->paginate(1)
            ->toJson();
        if(request()->ajax()) //|||||||||temp api
            return response($relatedProducts);
        $product = ($product->load('manage', 'images'))->toJson();
        return view('web.detail', compact('product', 'relatedProducts'));
    }

    public function search()
    {
        if( request('search') )
        {
            $products = Product::where('title', 'like', '%' . request('search') . '%' )
                ->where('status', 1)
                ->latest()
                ->paginate(1)
                ->toJson();
            if(request()->ajax()) //|||||||||temp api
                return response($products);
            return view('_welcome', compact('products'));
        } else {
            return redirect('/');
        }
    }

    public function byCategory($name)
    {
        $category = Category::where('name', $name)->first();
        $products = Product::where('stock_status', 1)
                        ->where('status', 1)
                        ->where('category_id', $category->id)
                        ->latest()
                        ->paginate(1)
                        ->toJson();
        if(request()->ajax()) //|||||||||temp api
            return response($products);
        return view('_welcome', compact('products'));
    }

    public function contactUs()
    {
        return view('web.contact-us');
    }

    public function returnPolicy()
    {
        return view('web.return-policy');
    }

    public function help()
    {
        return view('web.help');
    }

    public function privacyPolicy()
    {
        return view('web.privacy-policy');
    }

    public function terms()
    {
        return view('web.terms');
    }

}
