<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function shipping()
    {
        return $this->hasOne(\App\Shipping::class);
    }

    public function purchases()
    {
        return $this->hasMany(\App\Purchase::class);
    }

    // public function products()
    // {
    //     return $this->belongsTo(\App\ManageProduct::class)
    //     		->join('products', 'product_stocks.product_id', '=', 'products.id')
    //     		->select('products.title');
    // }

}
