<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
	public $timestamps = false;
    protected $guarded = [];
    // protected $appends = ['productssss'];

    public function product_stock()
    {
        return $this->belongsTo(\App\ManageProduct::class)
        			->join('products', 'product_stocks.product_id', 'products.id')
        			->select('product_stocks.*', 'products.title');
    }

}
