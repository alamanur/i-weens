<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];

    public function product()
    {
        return $this->hasMany(\App\Product::class);
    }
}
