<?php

// |||||||||||||||||WebView|||||||||||||||||||||
Auth::routes();
Route::group(['middleware' => 'guest', 'namespace' => 'Auth'], function () {
	Route::get('/login/callback/{social}', 'SocialAuthController@handleProviderCallback');
	Route::get('/login/{social}', 'SocialAuthController@redirectToProvider');
});
Route::get('/', 'HomeController@index');
Route::get('/category/{name}', 'HomeController@byCategory');
Route::get('/related-products/{categoryId}', 'HomeController@relatedProducts');
Route::get('/detail/{product}', 'HomeController@detail')->name('product.detail');
Route::get('/search', 'HomeController@search')->name('search');
//static
Route::get('/help', 'HomeController@help')->name('help');
Route::get('/contact-us', 'HomeController@contactUs')->name('contact-us');
Route::get('/privacy-policy', 'HomeController@privacyPolicy')->name('privacy-policy');
Route::get('/terms', 'HomeController@terms')->name('terms');
Route::get('/return-policy', 'HomeController@returnPolicy')->name('return-policy');
//cart
Route::get('/cart', 'CartsController@cart');
Route::get('/cart/add/{manage}', 'CartsController@add');
Route::get('/cart/update/{rowId}/{quantity}', 'CartsController@update');
Route::get('/cart/delete/{rowId}', 'CartsController@delete');
Route::get('/cart/destroy', 'CartsController@destroy');

Route::get('/my-cart', 'CartsController@myCart')->name('my-cart');

Route::middleware(['auth'])->group(function () {
	Route::get('/shipping', 'CheckoutsController@shipping')->name('shipping');
	Route::post('/shipping', 'CheckoutsController@storeShipping')->name('shipping.store');
	Route::get('/payment', 'CheckoutsController@payment')->name('payment');
	Route::post('/payment', 'CheckoutsController@storePayment');
	Route::get('/review-order', 'CheckoutsController@reviewOrder')->name('review-order');
	Route::post('/confirm-order', 'CheckoutsController@confirmOrder')->name('confirm-order');
	Route::get('/order-history', 'OrderHistoriesController@orderHistory')->name('order-history');
	Route::get('/order-history/details/{order}', 'OrderHistoriesController@orderDetails')->name('order-history.details');
	Route::get('/user/settings', 'SettingsController@index')->name('settings');
	Route::post('/user/settings', 'SettingsController@updateUser')/*->name('update-user')*/;
 });






// |||||||||||||||||||||admin routes|||||||||||||||||||
Route::prefix('backend')->group(function () {
	Route::get('dashboard', 'BackendController@index')->name('backend');
	Route::get('home', 'BackendController@index');

	Route::resource('categories', 'CategoriesController');

	Route::delete('products/{image}/gallery/remove', 'ProductsController@remove')->name('products.gallery.remove');
	Route::match(['get', 'post'], 'products/{product}/gallery', 'ProductsController@gallery')->name('products.gallery');
	Route::resource('products', 'ProductsController');

	Route::get('product/manage/create/{product}', 'ManageProductsController@create')->name('manage.create');
	Route::get('product/manage/{product}', 'ManageProductsController@show')->name('manage.show');
	Route::get('product/manage/{manage}/increase', 'ManageProductsController@increase')->name('manage.increase');
	Route::resource('product/manage', 'ManageProductsController', ['except' => ['create', 'index', 'show']]);

	Route::resource('product-entry-records', 'ProductEntryRecordsController');

	Route::get('customer-orders/{customer_order}/shipping', 'OrdersController@shipping')->name('customer-orders.shipping');
	Route::resource('customer-orders', 'OrdersController');

	// Route::get('add-product', 'ProductsController@create');

});

Route::get('/home', 'HomeController@index')->name('home');
