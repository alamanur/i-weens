<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
    return response()->json( 
    		\App\Product::where('stock_status', 1)
                ->where('status', 1)
                ->latest()
                ->paginate(12) 
    	);
});

Route::get('/detail/{product}', function (\App\Product $product) {
    return response()->json( $product->load('manage', 'images') );
});


Route::get('/order/confirmation', function() {return view('test');});

Route::post('/order/confirmation', function() {

	// if(
	// 	( ! request('cart') ) 
	// 	|| ( count( request('cart') ) == 0 ) 
	// 	|| ( ! is_array( request('cart') ) ) 
	// )
	// 	throw new Exception("Cart is Empty");
	
	if( count( request('cart') ) == 0 ) 
		throw new Exception("Cart is Empty");

	if( !is_array( request('cart') ) )
		throw new Exception("Invalid Cart");

	if(  ! request('cart') )
		throw new Exception("Cart is not Found");

	dd(request()->all());
	// dd(request('cart'));

    \DB::beginTransaction();

    try {

	$product_stocks_ids = collect( request('cart') )->map( function ($item, $key) {
								return (int) $item["product_stock_id"];	
							});
// dd($product_stocks_ids);
	$collection = ( \App\ManageProduct::find($product_stocks_ids) )->load('product');
// dd($collection);
	$purchases = ( $collection->map(function ($item, $key) {
                // find the cart item quantity for this collection item
			$quantity = (int) request('cart')[
							array_search(
								$item->id, array_column(
									request('cart'), 'product_stock_id'
								)
							)
						]['quantity'];
			if(!$quantity)
				throw new Exception("Invalid cart Quantity");
			// checkage for original quantity of product
			if( $quantity > $item->quantity )
				throw new Exception("Only " . $item->quantity . " No of this product is available");

			// dump($quantity . '||||'. $item->quantity);
			// updating quantity in status table
			\App\ManageProduct::where('id', $item->id)
	            ->update(['quantity' => ( $item->quantity - $quantity ) ]);

			// updating Product table stock_status || more conditions will apply alter (i.e: almost gone, nearly finished...) 
			if( ( $item->quantity - $quantity ) < 1 )
				\App\Product::where('id', $item->product_id)
		            ->update(['stock_status' => 0 ]);



			// dump($quantity);
			// dd('===');
			return [
				'product_id' => $item->product_id,
				'product_title' => $item->product->title,
				'size' => $item->size,
				'color' => $item->color,
				'quantity' => $quantity,
				'price' => $item->price,
				'discount' => $item->discount,
			];
		}) )->all();


	// dd($purchases);
    	$order = \App\Order::create([
    			'user_id' => request('user_id'),
    			'trx_id' => request('trx_id'),
    			'trx_phone' => request('trx_phone')
    		]);
		$order->purchases()
			->createMany($purchases);
		$order->shipping()
			->create([
				'name' => request('name'),
				'address' => request('address'),
				'contact_no' => request('contact_no'),
				'email' => request('email'),
				'zip_code' => request('zip_code'),
				'city' => request('city'),
				'country' => request('country')
			]);
			
		// dd($order->load('purchases'));


    	\DB::commit();

        return response()->json( ['success' => "Ordered successfully"] );
        // return $order;
     	
    } catch (\Exception $e) {
    	// dump($e->getMessage());
     	\DB::rollBack();

        return response()->json( [
        		'error' => "Order Failed",
        		'message' => $e->getMessage()
        	], 400 );
    }
});
