
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));
Vue.component('product-list', require('./components/product/ProductList.vue'));
Vue.component('product-detail', require('./components/product/ProductDetail.vue'));
Vue.component('mini-cart', require('./components/MiniCart.vue'));
Vue.component('cart', require('./components/cart/Cart.vue'));

const eventHub = new Vue()
Vue.mixin({
    data: function () {
        return {
            eventHub: eventHub,
        }
    }
})

const app = new Vue({
    el: '#app',
    data: {
        itemsInCart : 0,
        baseUrl
    },
    created: function () {
        this.eventHub.$on('itemsInCart', function(updateItemsInCart){
            this.updateItemsInCart = updateItemsInCart;
        });
        this.eventHub.$on('itemsInCart', (itemsInCart) => {this.itemsInCart = itemsInCart;});
    },
    methods: {
        // updateItemsInCart
    }
});
