@extends('layouts.master')
{{-- @section('title', 'Page Title') --}}
@section('content')
<section class="page" id="settings">
    <div class="container">
        <div class="row">
            <div class="pageContentSection">
                <div class="col s10  m12 offset-s1">
                    <h4 class="onPageTitle">Sign Up</h4>
                    <section class="facebookSettings">
                        <div class="col s12">

                                <div class="col s12">
                                    <a href="{{ url('/login/facebook') }}">
                                        <img src="{{ asset('/images/facebook-sign-up.png') }}" class="responsive-img"  >                                        
                                    </a>
                                </div>
                                <div class="col s12">
                                    <a href="{{ url('/login/google') }}">
                                        <img src="{{ asset('/images/google-sign-up.png') }}" class="responsive-img"  >                                        
                                    </a>
                                </div>
                                <div class="col s12">
                                    <a href="{{ route('register') }}">
                                        <img src="{{ asset('/images/email-sign-up.png') }}" class="responsive-img"  >                                        
                                    </a>
                                </div>
                            <form class="col s12" action="{{ route('login') }}" method="post">
                                {{ csrf_field() }}

                                <div class="input-field col s10">
                                    <input 
                                        name="email" 
                                        id="email" 
                                        type="email" 
                                        class="validate {{ $errors->has('email') ? 'invalid' : '' }}" 
                                        value="{{ old('email') }}" 
                                        required>
                                    <label for="email"> 
                                        Email 
                                        <span class="required red-text" >*</span>
                                        @if ($errors->has('email'))
                                            <span class="red-text">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </label>
                                </div>

                                <div class="input-field col s10">
                                    <input 
                                        name="password" 
                                        id="password" 
                                        type="password" 
                                        class="validate {{ $errors->has('password') ? 'invalid' : '' }}" 
                                        required>
                                    <label for="password"> 
                                        Password 
                                        <span class="required red-text" >*</span>
                                        @if ($errors->has('password'))
                                            <span class="red-text">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </label>
                                </div>


                                <div class="input-field col s10"></div>

                                <div class="col s12">
                                    <p class="">
                                        <input type="checkbox" id="test5" />
                                        <label for="test5">Remember Me</label>
                                    </p>
                                    <button type="submit" class="waves-effect waves-light btn light-blue lighten-2">Login</button>
                                </div>

                            </form>

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

</section>

@endsection