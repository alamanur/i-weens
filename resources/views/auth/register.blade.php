@extends('layouts.master')
{{-- @section('title', 'Page Title') --}}
@section('content')
<section class="page" id="settings">
    <div class="container">
        <div class="row">
            <div class="pageContentSection">
                <div class="col s10  m12 offset-s1">
                    <h4 class="onPageTitle">Register</h4>
                    <section class="facebookSettings">
                        <div class="col s12">
                            <form class="col s12" action="{{ route('register') }}" method="post">
                                {{ csrf_field() }}

                                <div class="input-field col s10">
                                    <input 
                                        name="name" 
                                        id="first_name" 
                                        type="text" 
                                        class="validate {{ $errors->has('name') ? 'invalid' : '' }}" 
                                        value="{{ old('name') }}" 
                                        required>
                                    <label for="name"> 
                                        Name 
                                        <span class="required red-text" >*</span>
                                        @if ($errors->has('name'))
                                            <span class="red-text">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </label>
                                </div>

                                <div class="input-field col s10">
                                    <input 
                                        name="email" 
                                        id="email" 
                                        type="email" 
                                        class="validate {{ $errors->has('email') ? 'invalid' : '' }}" 
                                        value="{{ old('email') }}" 
                                        required>
                                    <label for="email"> 
                                        Email 
                                        <span class="required red-text" >*</span>
                                        @if ($errors->has('email'))
                                            <span class="red-text">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </label>
                                </div>

                                <div class="input-field col s10">
                                    <input 
                                        name="phone" 
                                        id="phone" 
                                        type="text" 
                                        class="validate {{ $errors->has('phone') ? 'invalid' : '' }}" 
                                        value="{{ old('phone') }}" 
                                        required>
                                    <label for="phone"> 
                                        Phone Number 
                                        <span class="required red-text" >*</span>
                                        @if ($errors->has('phone'))
                                            <span class="red-text">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </label>
                                </div>

                                <div class="input-field col s10">
                                    <input 
                                        name="password" 
                                        id="password" 
                                        type="password" 
                                        class="validate {{ $errors->has('password') ? 'invalid' : '' }}" 
                                        required>
                                    <label for="password"> 
                                        Password 
                                        <span class="required red-text" >*</span>
                                        @if ($errors->has('password'))
                                            <span class="red-text">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </label>
                                </div>

                                <div class="input-field col s10">
                                    <input 
                                        name="password_confirmation" 
                                        id="password_confirmation" 
                                        type="password" 
                                        class="validate {{ $errors->has('password_confirmation') ? 'invalid' : '' }}"
                                        required>
                                    <label for="password_confirmation"> 
                                        Password Confirmation 
                                        <span class="required red-text" >*</span>
                                    </label>
                                </div>

                                <div class="col s12">
                                    <button type="submit" class="waves-effect waves-light btn light-blue lighten-2">Sign Up</button>
                                </div>

                            </form>

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

</section>

@endsection