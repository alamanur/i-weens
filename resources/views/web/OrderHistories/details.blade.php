@extends('layouts.master')
{{-- @section('title', 'Page Title') --}}
@section('content')

<section class="page" id="transection">
    <div class="container">
        <div class="row">
            <div class="pageContentSection">
                <div class="col s10 offset-s1 m8 offset-m2">
                    <h4 class="onPageTitle center-align">Order Detail</h4>
                    <p class="center-align"> OrderID: {{ $order->id }} </p>
               
                    <div class="row">
                        <h5 class="center-align">Product Info</h5>
                         <table class="bordered striped highlight responsive-table">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Size</th>
                                    <th>Color</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Discount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $total = 0; ?>
                                @foreach( $order->purchases as $purchase)
                                <tr>
                                    <td>{{ $purchase->product_title }}</td>
                                    <td>{{ $purchase->size }}</td>
                                    <td>{{ $purchase->color }}</td>
                                    <td>{{ $purchase->quantity }}</td>
                                    <td>{{ $purchase->price }}</td>
                                    <td>{{ $purchase->discount }}</td>
                                 </tr>
                                 <?php $total += ($purchase->price - $purchase->discount) * $purchase->quantity; ?>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="4"></th>
                                    <th>Total: {{ $total }}</th>
                                    <th></th>
                                    <?php unset($total); ?>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div class="row">
                        <h5 class="center-align">Shipping Info</h5>
                         <table class="bordered striped highlight responsive-table valign">
                            <thead>
                                <tr>
                                    <th>Person</th>
                                    <th>Contact No</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>City</th>
                                    <th>Country</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $order->shipping->name }}</td>
                                    <td>{{ $order->shipping->contact_no }}</td>
                                    <td>{{ $order->shipping->email }}</td>
                                    <td>{{ $order->shipping->address }}</td>
                                    <td>{{ $order->shipping->city }}</td>
                                    <td>{{ $order->shipping->country }}</td>
                                 </tr>
                            </tbody>
                        </table>
                    </div>
               

                </div> <!-- /.col -->

            </div> <!-- /.pageContentSection -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</section> <!-- /.page -->

@endsection