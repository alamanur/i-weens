@extends('layouts.master')
{{-- @section('title', 'Page Title') --}}
@section('content')

<section class="page" id="transection">
    <div class="container">
        <div class="row">
            <div class="pageContentSection">
                <div class="col s10 offset-s1 m8 offset-m2">
                    <h4 class="onPageTitle center-align">Order History</h4>
                    <p class="center-align"><i class="fa fa-meh-o"></i> We have your back! You never have to worry when you shop on Wish.</p>
                @if( $orders->count() == 0 )
                    <div class="row">
                        <div class="col s3 offset-s1 m2 offset-m2">
                        <p class="center-align"><i class="fa fa-info-circle"></i> You haven't made any orders.</p>
                        </div>
                        <a href="/" class="grey center-align waves-effect waves-light btn-large"><i class="fa fa-shopping-bag"></i> Shop Now</a>
                    </div>
                @else
                    <div class="row">
                         <table>
                            <thead>
                                <tr>
                                    <th>Order Id</th>
                                    {{-- <th>Payment Total</th> --}}
                                    {{-- <th>Status</th> --}}
                                    <th>Date</th>
                                    <th>Details</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $orders as $order)
                                <tr>
                                    <td>{{ $order->id }}</td>
                                    {{-- <td>Eclair</td> --}}
                                    {{-- <td></td> --}}
                                    <td>{{ $order->created_at->format('jS F Y') }}</td>
                                    <td>
                                        <a href="{{ route('order-history.details', ['order' => $order->id]) }}">Details</a>
                                    </td>
                                 </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif


                </div> <!-- /.col -->

            </div> <!-- /.pageContentSection -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</section> <!-- /.page -->

@endsection