@extends('layouts.master')
{{-- @section('title', 'Page Title') --}}
@section('content')
<section class="page" id="settings">
    <div class="container">
        <div class="row">
            <div class="pageContentSection">
                <div class="col s10  m8 offset-s1">
                    <h4 class="onPageTitle">Settings</h4>
                    <section class="facebookSettings">
                        <h5>Accont Setting</h5>
                        <div class="col s12">
                            <form class="col s12" action="{{ route('settings') }}" method="post">
                                {{ csrf_field() }}

                                <div class="input-field col s10">
                                    <input 
                                        name="name" 
                                        id="first_name" 
                                        type="text" 
                                        class="validate {{ $errors->has('name') ? 'invalid' : '' }}" 
                                        value="{{ old('name') ?? $user->name }}" 
                                        required>
                                    <label for="name"> 
                                        Name 
                                        <span class="required red-text" >*</span>
                                        @if ($errors->has('name'))
                                            <span class="red-text">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </label>
                                </div>

                                <div class="input-field col s10">
                                    <input 
                                        name="email" 
                                        id="email" 
                                        type="email" 
                                        class="validate {{ $errors->has('email') ? 'invalid' : '' }}" 
                                        value="{{ old('email') ?? $user->email }}" 
                                        required>
                                    <label for="name"> 
                                        Email 
                                        <span class="required red-text" >*</span>
                                        @if ($errors->has('email'))
                                            <span class="red-text">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </label>
                                </div>

                                <div class="input-field col s10">
                                    <input 
                                        name="phone" 
                                        id="phone" 
                                        type="text" 
                                        class="validate {{ $errors->has('phone') ? 'invalid' : '' }}" 
                                        value="{{ old('phone') ?? $user->phone }}" 
                                        required>
                                    <label for="name"> 
                                        Phone Number 
                                        <span class="required red-text" >*</span>
                                        @if ($errors->has('phone'))
                                            <span class="red-text">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </label>
                                </div>

                                <div class="col s12">
                                    <button type="submit" class="waves-effect waves-light btn orange lighten-2">Update</button>
                                </div>
                            </form>
                        </div>
                    </section>
                    {{-- 
                    <section class="localSettings">
                        <h5>Locale Settings</h5>
                        <div class="col s12">
                            <p>Language of preference for your Wish.</p>
                            <form action="" method="post">
                                <p>
                                    <input class="with-gap" name="localSet" type="radio" id="bangla" checked="checked"  />
                                    <label for="bangla">Bangla</label>
                                </p>
                                <p>
                                    <input class="with-gap" name="localSet" type="radio" id="english"  />
                                    <label for="english">English</label>
                                </p>
                                <p>
                                    <input class="with-gap" name="localSet" type="radio" id="espanol"  />
                                    <label for="espanol">Español</label>
                                </p>
                                <p>
                                    <input class="with-gap" name="localSet" type="radio" id="porgugues"  />
                                    <label for="porgugues">Português</label>
                                </p>
                                <p>
                                    <input class="with-gap" name="localSet" type="radio" id="italiano"  />
                                    <label for="italiano">Italiano</label>
                                </p>
                                <p>
                                    <input class="with-gap" name="localSet" type="radio" id="deutsch"  />
                                    <label for="deutsch">Deutsch</label>
                                </p>
                                <p>
                                    <input class="with-gap" name="localSet" type="radio" id="dansk"  />
                                    <label for="dansk">Dansk</label>
                                </p>
                            </form>
                        </div>
                    </section>
                     --}}
                @if( ! auth()->user()->facebook_id )
                    <h4>&nbsp;</h4>
                    <h5>Facebook Communication Settings</h5>
                    <section class="facebookSettings">                      
                        <div class="col s12">
                                <div class="input-field col s10">
                                    <p>Connect with Facebook to help personalize your experience and share products with friends</p>
                                </div>
                                <div class="input-field col s10">
                                    <a href="{{ url('/login/facebook') }}" class="waves-effect waves-light btn white " style="padding: 0px;"><img src="/images/connect-with-facebook.png" width="300"></a>
                                </div>
                        </div>
                    </section>
                @endif
                @if( ! auth()->user()->google_id )
                    <h4>&nbsp;</h4>
                    <h5>Google Communication Settings</h5>
                    <section class="facebookSettings">                      
                        <div class="col s12">
                                <div class="input-field col s10">
                                    <p>Connect with Google to help personalize your experience and share products with friends</p>
                                </div>
                                <div class="input-field col s10">
                                    <a href="{{ url('/login/google') }}" class="waves-effect waves-light btn white " style="padding: 0px;"><img src="/images/connect-with-google.png" width="300"></a>
                                </div>
                        </div>
                    </section>
                @endif
                    <h4>&nbsp;</h4>
                    <h4>&nbsp;</h4>

                </div>
                <div class="col s10 m4 offset-s1">

                    @include('web.partials.settings-nav')

                </div>
            </div>
        </div>
    </div>

</section>

@endsection