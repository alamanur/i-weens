@extends('layouts.master')
{{-- @section('title', 'Page Title') --}}
@section('content')
<section class="page" id="latestProduct">
 

<div class="cartPage">
     <div class="container">

        <div class="row">
            <div class="col s10 offset-s1 l12">
                <div class="row">
                    <aside id="shipTo">
                        <div class="col m12 l5">
                            <div class="row">
                                <div class="col s12 ">

                                    @include( 'web.partials.checkout-nav' )

                                    <h2 class="orderSummary cartPageHeading">PAYMENT</h2>
                                    <div class="row">
                                    <form id="payment-form" action="{{ route('payment') }}" method="post">
                                    {{ csrf_field() }}
                                        {{-- 
                                        <div class="col s12">
                                            <ul class="tabs shippingOptions">
                                                <li class="tab col s6">
                                                    <a href="#test1">
                                                        <img src="images/cards/master.png" width="25%">
                                                        <img src="images/cards/visa.png" width="25%">
                                                        <img src="images/cards/express.png" width="25%">
                                                        <img src="images/cards/discover.png" width="25%">
                                                    </a>
                                                </li>
                                                <li class="tab col s6">
                                                    <a href="#test2"  class="active">
                                                        <img src="images/cards/others2.png" width="30%">
                                                    </a>
                                                </li>

                                            </ul>
                                        </div>
                                        <div id="test1" class="col s12">
                                            <div class="row">
                                                <!--SHIP TO form -->

                                                <form class="col s12" action="review.php" method="post">
                                                    <div class="row">
                                                        <div class="input-field col s10">
                                                            <input  id="first_name" type="text" class="validate">
                                                            <label for="first_name">Card Number <span class="required red-text" >*</span></label>
                                                        </div>
                                                        <div class="input-field col s10">
                                                            <input id="cvv" type="number" class="validate">
                                                            <label for="cvv">Security Code(CVV) <span class="required red-text" >*</span></label>
                                                        </div>


                                                        <div class="input-field col s10">
                                                            <input id="expire" type="text" class="validate">
                                                            <label for="expire">Expiration Date <span class="required red-text" >*</span></label>
                                                        </div>


                                                        <div class="input-field col s10">
                                                            <input id="zipCode" type="text" class="validate">
                                                            <label for="zipCode">Zip Code <span class="required red-text" >*</span></label>
                                                        </div>

                                                        <div class="col s12">
                                                            <button type="submit" class="waves-effect waves-light btn orange lighten-2">Next</button>
                                                        </div>
                                                    </div>
                                                </form> 


                                            </div> 
                                        </div>
                                         --}}
                                        <div id="test2" class="col s12">

                                            <div class="row">
                                                <div class="col s12">
                                                    {{-- 
                                                    <div class="row">
                                                        <div class="col s12">
                                                            <div class="card horizontal">
                                                                <div class="card-image">
                                                                    <img src="images/cards/paypal.png">
                                                                </div>
                                                                <div class="card-stacked">
                                                                    <div class="card-content">
                                                                        <p>PayPal is the faster, safer way to send money, make an online payment, receive money or set up a merchant account.</p>
                                                                    </div>
                                                                    <div class="card-action">
                                                                        <a href="#">Pay On Paypal</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <h4 class="center-align">OR</h4>
                                                     --}}
                                                     {{-- bKash Payment --}}
                                                    <div class="row">
                                                        <div class="col s12">
                                                            <div class="card horizontal">
                                                                <div class="card-image">
                                                                    <a class="modal-trigger" href="#bkashPaymentModal" onclick="event.preventDefault()">
                                                                        <img width="30%" src="images/cards/bkash.png">
                                                                    </a>
                                                                </div>
                                                                <div class="card-stacked">
                                                                    <div class="card-content">
                                                                        <p>bKash is a mobile financial service in Bangladesh operating under the authority of Bangladesh Bank as a subsidiary of BRAC Bank Limited.</p>
                                                                    </div>
                                                                    <div class="card-action activator">
                                                                        <button class="btn white black-text activator" href="#" for="bkash_payment" onclick="event.preventDefault();">
                                                                             Pay on bKash 
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                <div class="card-reveal">
                                                                    <span class="card-title grey-text text-darken-4">
                                                                        <i class="material-icons right">close</i>
                                                                    </span>

                                                                        <input 
                                                                            class="with-gap" 
                                                                            name="payment_type" 
                                                                            type="radio" 
                                                                            id="bkash_payment" 
                                                                            value="bkash" 
                                                                            {{ (( session('paymentInfo.payment_type') == "bkash" ) || ( old('trx_id') == 'bkash' )) 
                                                                            ? 'checked' 
                                                                            : '' }}
                                                                            checked
                                                                            required/>
                                                                        <label for="bkash_payment">bKash Payment</label>

                                                                    <div class="input-field col s10 text-red">
                                                                        <input 
                                                                            form="payment-form"
                                                                            name="trx_id" 
                                                                            id="trx_id" 
                                                                            type="text" 
                                                                            class="validate {{ $errors->has('trx_id') ? 'invalid' : '' }}" 
                                                                            value="{{ session('paymentInfo.trx_id') ?? old('trx_id') }}" 
                                                                            >
                                                                        <label for="trx_id"> 
                                                                            Transaction ID 
                                                                            <span class="required red-text" >*</span>
                                                                            {{-- @if ($errors->has('trx_id'))
                                                                                <span class="red-text">
                                                                                    <strong>{{ $errors->first('trx_id') }}</strong>
                                                                                </span>
                                                                            @endif --}}
                                                                        </label>
                                                                    </div>
                                                                    <div class="input-field col s10 text-red">
                                                                        <input 
                                                                            form="payment-form"
                                                                            name="trx_phone" 
                                                                            id="trx_phone" 
                                                                            type="text" 
                                                                            class="validate {{ $errors->has('trx_phone') ? 'invalid' : '' }}" 
                                                                            value="{{ session('paymentInfo.trx_phone') ?? old('trx_phone') }}" 
                                                                            >
                                                                        <label for="trx_phone"> 
                                                                            Transaction Number 
                                                                            <span class="required red-text" >*</span>
                                                                            {{-- @if ($errors->has('trx_phone'))
                                                                                <span class="red-text">
                                                                                    <strong>{{ $errors->first('trx_phone') }}</strong>
                                                                                </span>
                                                                            @endif --}}
                                                                        </label>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                     {{-- ./bKash Payment --}}
                                                    
                                                     {{-- COD Payment --}}
                                                    <div class="row">
                                                        <div class="col s12">
                                                            <h4 class="center-align">OR</h4>
                                                            <div class="card horizontal">
                                                                <div class="card-image">
                                                                    <img width="30%" src="images/cards/cashondelivery.jpg">
                                                                </div>
                                                                <div class="card-stacked">
                                                                    <div class="card-content">
                                                                        <p>Cash on delivery (COD) is a type of transaction in which the recipient makes payment for a good at the time of delivery. If the purchaser does not make payment when the good is delivered, then the good is returned to the seller.</p>
                                                                    </div>
                                                                    <div class="card-action">
                                                                        <a href="#">Cash On Delivery</a>
                                                                        <p>
                                                                            <input 
                                                                                name="payment_type" 
                                                                                type="radio" 
                                                                                class="filled-in" 
                                                                                id="cod_payment" 
                                                                                form="payment-form" 
                                                                                value="cod" 
                                                                                {{ (( session('paymentInfo.payment_type') == "cod" ) || ( old('trx_id') == 'cod' )) 
                                                                                ? 'checked' 
                                                                                : '' }}
                                                                                required />
                                                                            <label for="cod_payment">Check in</label>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                     {{-- ./COD Payment --}}
                                                    
                                                    <div class="row">
                                                        
                                                            <button type="submit" class="waves-effect waves-light btn orange lighten-2" form="payment-form">Next</button>
                                                            
                                                       
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>  <!-- /.col -->
                    </aside>

                    @include( 'web.partials.checkout-cart', ['cart' => $cart] )

                </div> <!-- /.row -->

            </div> <!-- /.col -->
        </div> <!-- /.row -->

    </div>
</div>

</section>



<!-- Modal Include --> 

<!-- Modal Structure -->
<div id="bkashPaymentModal" class="modal modal-fixed-footer" >
    <div class="modal-content">
        <h4 class="center-align">bKash Payment Process</h4>
        <hr />
        <img src="images/cards/bkash-payment.png" alt="payment process on bkash" class="responsive-img" />
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" title="Click outside of the modal to close popup window" onclick="event.preventDefault()">Close</a>
    </div>
    

</div>
<!-- Modal Include end --> 

@endsection