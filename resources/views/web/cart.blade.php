@extends('layouts.master')
{{-- @section('title', 'Page Title') --}}
@section('content')
<section class="page" id="latestProduct">

    
    @push('vueVariable')
        <script>
            window.cartContent = {!! $cart !!} ;
            
            window.cartProductMaxQuantity = {!! $cartProductMaxQuantity !!}
        </script>
    @endpush

<div class="cartPage">
    <cart></cart>
</div>

</section>
@endsection