                                        <nav class="">
                                            <div class="nav-wrapper">
                                               <div class="col s12 black lighten-5">
                                                    <a href="{{ route('shipping') }}" class="breadcrumb orange-text ">Ship To</a>
                                                    <a 
                                                        href="{{ route('payment') }}" 
                                                        class="breadcrumb 
                                                            {{ (request()->is('payment') || request()->is('review-order') )
                                                            ? 'orange-text' :
                                                            'white-text' }}">
                                                        Payment</a>
                                                    <a href="{{ route('review-order') }}" class="breadcrumb  {{ request()->is('review-order') ? 'orange-text' : 'white-text' }}">Review Order</a>
                                                </div>
                                            </div>
                                        </nav>