
                    <!--Side menu bar-->
                    <div class="collection with-header">
                        <a href="{{ route('settings') }}" class="collection-item">Settings</a>
                        {{-- <a href="" class="collection-item">Rewards</a> --}}
                        <a href="https://play.google.com/store/apps/details?id=eshopping.ecommerce.e_shopping&hl=en" target="_blank" class="collection-item">Mobile Apps</a>
                        <a href="{{ route('help') }}" class="collection-item">Help</a>
                        <a href="{{ route('contact-us') }}" class="collection-item">Contact Us</a>
                        {{-- <a href="" class="collection-item">Company</a> --}}
                        <a href="{{ route('privacy-policy') }}" class="collection-item">Privacy Policy</a>
                        <a href="{{ route('terms') }}" class="collection-item">Terms of Service</a>
                        <a href="{{ route('return-policy') }}" class="collection-item">Return Policy</a>
                        {{-- <a href="supplier.php" class="collection-item">Suppliers</a> --}}
                    </div>
                    <!--Side menu bar end -->