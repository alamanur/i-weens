
                    <aside id="shoppingCart">
                        <div class="col m12 l7 offset-l0" style="background: none;">
                            <h2 class="shipPageHeading white lighten-3">SHOPPING CART</h2>
                            
                        @foreach( $cart as $content )    
                            <article>
                                <div class="row">
                                    <div class="col s12">
                                        <div class="col s12 m5 l3">

                                            <div class="shipItemThumb" style="background-image: url({{ asset($content->options->image) }}); "></div>
                                            {{-- <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons tiny">star</i> --}}

                                        </div>
                                        <div class="col s12 m7 l9 cartItemInfo ">
                                            <h5 class="cartItemTitle" >
                                               {{ $content->name }}
                                            </h5>
                                            <p class="cartItemSize">

                                            @if( $content->options->size ) 
                                                Size: {{ $content->options->size }} 
                                            @endif 
                                                
                                            @if( $content->options->color )
                                                Color: {{ $content->options->color }}</span>
                                            @endif
                                            </p>                                

                                            <p class="shipQuantity">Quantity: {{  $content->qty }} </p>      
                                            {{-- <p class="cartItemShipping">Shipping : $8.00</p> --}}
                                            {{-- <p class="cartItemArrival">Estimated Arrival: Sep 2 - Sep 5</p> --}}
                                            <p class="cartItemPrice">
                                            @if( $content->options->discount )
                                                <span class="cartItemPriceOld">${{ $content->price }}</span> 
                                                ${{ ($content->price - $content->options->discount) }}
                                            @else
                                                ${{ $content->price }}
                                            @endif
                                            </p>

                                        </div>
                                    </div>
                                </div>  <!-- /.row -->
                            <hr class="hide-on-med-and-up" />
                            </article>
                        @endforeach

                        </div>  <!-- /.col -->
                    </aside>