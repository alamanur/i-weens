@extends('layouts.master')
{{-- @section('title', 'Page Title') --}}
@section('content')
	{{-- @push('css')
		@if(config('app.env') == "local")
			<script type="text/javascript" src="{{ asset('js/vue.js') }}"></script>
		@else
			<script type="text/javascript" src="{{ asset('js/vue.min.js') }}"></script>
			<script type="text/javascript" src="https://unpkg.com/vue@2.4.2/dist/vue.min.js"></script>
		@endif
	@endpush --}}

<section class="page" id="latestProduct">
    <div class="row">
        <div class="col s11 m10 l10 xl12 offset-l1 offset-m1 offset-s1">

            @push('vueVariable')
                <script>
                    window.productDetail = {!! $product !!};
                    window.products = {!! $relatedProducts !!}
                </script>
            @endpush

            <product-detail></product-detail>
          <!-- /.singleProduct -->
        </div>  <!-- /.col -->
    </div>   <!-- /.row -->
</section>   <!-- /.page -->

@endsection