@extends('layouts.master')
{{-- @section('title', 'Page Title') --}}
@section('content')
<section class="page" id="latestProduct">
 
<div class="cartPage">
     <div class="container">

        <div class="row">
            <div class="col s10 offset-s1 l12">
                <div class="row">
                    <aside id="shipTo">
                        <div class="col m12 l5">
                            <div class="row">
                                <div class="col s12 ">

                                    @include( 'web.partials.checkout-nav' )

                                        <h2 class="orderSummary cartPageHeading">SHIP TO </h2>
                                        <!--SHIP TO form -->                                        

                                        <div class="row">
                                            <form class="col s12" action="{{ route('shipping.store') }}" method="post">
                                                {{ csrf_field() }}
                                                <div class="row">

                                                    <div class="input-field col s10">
                                                        <input 
                                                            name="name" 
                                                            id="name" 
                                                            type="text" 
                                                            class="validate {{ $errors->has('name') ? 'invalid' : '' }}" 
                                                            value="{{ session('shippingInfo.name') ?? old('name') }}" 
                                                            required>
                                                        <label for="name"> 
                                                            Name 
                                                            <span class="required red-text" >*</span>
                                                            @if ($errors->has('name'))
                                                                <span class="red-text">
                                                                    <strong>{{ $errors->first('name') }}</strong>
                                                                </span>
                                                            @endif
                                                        </label>
                                                    </div>

                                                    <div class="input-field col s10">
                                                        <input 
                                                            name="email" 
                                                            id="email" 
                                                            type="email" 
                                                            class="validate {{ $errors->has('email') ? 'invalid' : '' }}"
                                                            value="{{ session('shippingInfo.email') ?? old('email') }}"
                                                            required>
                                                        <label for="email">
                                                            Email 
                                                            <span class="required red-text" >*</span>
                                                            @if ($errors->has('email'))
                                                                <span class="red-text">
                                                                    <strong>{{ $errors->first('email') }}</strong>
                                                                </span>
                                                            @endif
                                                        </label>
                                                    </div>


                                                    <div class="input-field col s10">
                                                        <input 
                                                            name="contact_no" 
                                                            id="contact_no" 
                                                            type="text" 
                                                            class="validate {{ $errors->has('contact_no') ? 'invalid' : '' }}"
                                                            value="{{ session('shippingInfo.contact_no') ?? old('contact_no') }}"
                                                            required>
                                                        <label for="contact_no">
                                                            Phone 
                                                            <span class="required red-text" >*</span>                   
                                                            @if ($errors->has('contact_no'))
                                                                <span class="red-text">
                                                                    <strong>{{ $errors->first('contact_no') }}</strong>
                                                                </span>
                                                            @endif
                                                        </label>
                                                    </div>

                                                    <div class="input-field col s10">
                                                        <textarea 
                                                            id="address" 
                                                            class="materialize-textarea validate {{ $errors->has('address') ? 'invalid' : '' }}" 
                                                            name="address"
                                                            required>{!! session('shippingInfo.address') ?? old('address') !!}</textarea>
                                                        <label for="address">
                                                            Address 
                                                            <span class="required red-text" >*</span>
                                                            @if ($errors->has('address'))
                                                                <span class="red-text">
                                                                    <strong>{{ $errors->first('address') }}</strong>
                                                                </span>
                                                            @endif
                                                        </label>
                                                    </div>

                                                    <div class="input-field col s10">
                                                        <input 
                                                            name="zip_code" 
                                                            id="zip_code" 
                                                            type="text" 
                                                            class="validate {{ $errors->has('zip_code') ? 'invalid' : '' }}"
                                                            value="{{ session('shippingInfo.zip_code') ?? old('zip_code') }}"
                                                            required>
                                                        <label for="zip_code">
                                                            Zip Code 
                                                            <span class="required red-text" >*</span>
                                                            @if ($errors->has('zip_code'))
                                                                <span class="red-text">
                                                                    <strong>{{ $errors->first('zip_code') }}</strong>
                                                                </span>
                                                            @endif
                                                        </label>
                                                    </div>

                                                    <div class="input-field col s10">
                                                        <input 
                                                            name="city" 
                                                            id="city" 
                                                            type="text" 
                                                            class="validate {{ $errors->has('city') ? 'invalid' : '' }}"
                                                            value="{{ session('shippingInfo.city') ?? old('city') }}"
                                                            required>
                                                        <label for="city">
                                                            City
                                                            <span class="required red-text" >*</span>
                                                            @if ($errors->has('city'))
                                                                <span class="red-text">
                                                                    <strong>{{ $errors->first('city') }}</strong>
                                                                </span>
                                                            @endif
                                                        </label>
                                                    </div>

                                                    <div class="col s12">
                                                        <button type="submit" class="waves-effect waves-light btn orange lighten-2">Next</button>
                                                    </div>
                                                </div>
                                            </form> 
                                        </div>
                                </div>

                            </div>


                        </div>  <!-- /.col -->
                    </aside>

                    @include( 'web.partials.checkout-cart', ['cart' => $cart] )

                </div> <!-- /.row -->

            </div> <!-- /.col -->
        </div> <!-- /.row -->

    </div>
</div>

</section>
@endsection