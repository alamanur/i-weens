@extends('layouts.master')
{{-- @section('title', 'Page Title') --}}
@section('content')
<section class="page" id="latestProduct">

    <div class="row">
        <div class="col s11 m10 l10 xl12 offset-l1 offset-m1 offset-s1">

            <div class="row" id="product-list">
            {{-- @for($i = 1; $i < 20; $i++) --}}
            <product-list></product-list>
            {{-- @endfor --}}

            </div>

        </div>
    </div>
</section>

@push('script')
	<script>
	// var data;
	var vmProductList = new Vue({
	    el: '#product-list',
		data: function () {
                return {
                    items: {!! $products !!},
                }
		    }
	});

	function append_data()
	{
		if(vmProductList.$data.items.next_page_url)
		{
			$.ajax({
		      type: "get",
		      url: vmProductList.$data.items.next_page_url,
		      data: { "_token" : "{{ csrf_token() }}" },
		      success: function( response ){
		         let data = JSON.parse(response);
		         // console.log(data);
		         // console.log(JSON.parse(response));
		         vmProductList.$data.items.current_page = data.current_page;
		         vmProductList.$data.items.last_page = data.last_page;
		         vmProductList.$data.items.next_page_url = data.next_page_url;
		         vmProductList.$data.items.path = data.path;
		         vmProductList.$data.items.per_page = data.per_page;
		         vmProductList.$data.items.prev_page_url = data.prev_page_url;
		         vmProductList.$data.items.to = data.to;
		         vmProductList.$data.items.total = data.total;
		         $.each(data.data, function(val)
		         	{
		         		// alert(val)
		         		vmProductList.$data.items.data.push(data.data[val]);
		         	});
		         // vmProductList.$data.items.data.push(data.data[0]);
		      },
		    });
		}
	};

	$(window).scroll(function()
	{
		if ($(window).scrollTop() == $(document).height() - $(window).height())
		{
			append_data();
		}
	});

	</script>
@endpush
@endsection