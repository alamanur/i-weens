@extends('layouts.master')
{{-- @section('title', 'Page Title') --}}
@section('content')
<section class="page" id="latestProduct">
 

<div class="cartPage">
     <div class="container">

        <div class="row">
            <div class="col s10 offset-s1 l12">
                <div class="row">
                    <aside id="shipTo">
                        <div class="col m12 l5">
                            <div class="row">
                                <div class="col s12 ">

                                    @include( 'web.partials.checkout-nav' )

                                    <h2 class="orderSummary cartPageHeading">ORDER SUMMARY </h2>
                                    <aside id="orderSummary">
                                        <div class="col m12 l12">
                                            <div class="row">
                                                <div class="col s12  grey lighten-4">
                                                    <h2 class="orderSummary cartPageHeading"></h2>
                                                    <h6 class="orderSummary itemTotal">Item Total: <span class="pull-right">${{ $total }}</span></h6>
                                                    {{-- <h6 class="orderSummary estimatedShipping">Estimated Shipping: <span class="pull-right">$49</span></h6> --}}
                                                    <h5 class="orderSummary orderTotal">ORDER TOTAL: <span class="pull-right">${{ $total }}</span></h5>
                                                </div>
                                                <div class="col s12"></div>
                                            </div>
                                            <div class="row">
                                                <a href="{{ route('confirm-order') }}" class="waves-effect waves-light btn btn-block" onclick="
                                                    event.preventDefault(); 
                                                    document.getElementById('confirm-order-form').submit();">
                                                        Confirm Order
                                                </a>
                                            <form action="{{ route('confirm-order') }}" method="POST" id="confirm-order-form">{{ csrf_field() }}</form>
                                            </div>

                                        </div> <!--   /.col  -->
                                    </aside>

                                </div>
                            </div>
                        </div>  <!-- /.col -->
                    </aside>

                    @include( 'web.partials.checkout-cart', ['cart' => $cart] )

                </div> <!-- /.row -->

            </div> <!-- /.col -->
        </div> <!-- /.row -->

    </div>
</div>

</section>



<!-- Modal Include --> 

<!-- Modal Structure -->
<div id="bkashPaymentModal" class="modal modal-fixed-footer" >
    <div class="modal-content">
        <h4 class="center-align">bKash Payment Process</h4>
        <hr />
        <img src="images/cards/bkash-payment.png" alt="payment process on bkash" class="responsive-img" />
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" title="Click outside of the modal to close popup window" onclick="event.preventDefault()">Close</a>
    </div>
    

</div>
<!-- Modal Include end --> 

@push('script')
<script type="text/javascript">
</script>
@endpush

@endsection