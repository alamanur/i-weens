<!DOCTYPE html>
<html>
    @include('layouts.partials.htmlHeader')

    <body>

        @stack('vueVariable')
        
        <div id="app">

        @include('layouts.partials.mainHeader')        

        @include('layouts.partials.mainNav')

        {{-- @include('layouts.partials.customarSupport') --}}

        @include('layouts.partials.flashMessages')

            <!-- Product Card Start  -->
            <div class="productArea">
{{-- ||||||||||||||||||||||||||||||HEADER||||||||||||||||||||||||||||||||||||||| --}}
            @yield('content')
{{-- |||||||||||||||||||||||||||||content|||||||||||||||||||||||||||||||||||||\\ --}}
            </div>
        </div> {{-- id= #app --}}

    <!-- Product Section End -->
    <!--Main Footer Secton-->
    @include('layouts.partials.footer')
    <!--Main Footer Secton End -->
@include('layouts.partials.scripts')
</body>
</html>