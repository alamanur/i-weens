
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Bixop @yield('title')</title>
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <base href="{{ asset('') }}">


        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link  rel="stylesheet" href="{{ asset('css/materialize.min.css') }}"/>
        <link  rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}"/>
        <link  rel="stylesheet" href="{{ asset('css/pgwslideshow.min.css') }}"/>
        <link  rel="stylesheet" href="{{ asset('css/custom.css') }}"/>
        <link  rel="stylesheet" href="{{ asset('css/responsive.css') }}"/>
        <script type="text/javascript">
            window.baseUrl = '{{ config('app.url') }}';
        </script>

        @stack('css')

    </head>