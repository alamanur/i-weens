
        <section id="navTabs" class="hide-on-med-and-down">
            <div class="container">
                <div class="row">
                    <div class="col s12 m10 offset-m1">
                        <ul class="mainMenu">
                            {{-- <li class="tab col "><a href="index.php">Outlet</a></li> --}}
                            <li class="tab col {{ request()->is('/') ? 'active' : '' }} "><a href="{{ url('') }}">Latest</a></li>{{-- 
                            <li class="tab col "><a href="index.php">Recently Viewed</a></li>
                            <li class="tab col "><a href="index.php">Fashion</a></li>
                            <li class="tab col "><a href="index.php">Gadgets</a></li>
                            <li class="tab col "><a href="index.php">Shoes</a></li>
                            <li class="tab col "><a href="index.php">Home Decor</a></li>
                            <li class="tab col "><a href="index.php">Hobbies</a></li>
                            <li class="tab col "><a href="index.php">Tops</a></li>
                            <li class="tab col "><a href="index.php">Wallets & Bags</a></li>
                            <li class="tab col "><a href="index.php">Accessories</a></li> --}}
                            <?php $categoriesCounter = 0; ?>
                            @foreach($categories as $category)
                            <li class="tab col {{ request()->is('category/*') ? 'active' : '' }}"><a href="{{ url('/category/' . $category->name ) }}">{{ $category->name }}</a></li>
                            <?php $categoriesCounter++; ?>
                            <?php if($categoriesCounter == 4) break;?>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </section>