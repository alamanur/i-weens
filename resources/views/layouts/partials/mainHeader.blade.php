
        <header id="mainHeader">
            <div class="container">
                <div class="row">
                    <div class="col m12 l12 s12 main-header-shrink">
                        <a href="/"><img class="shirnk-logo" src="/images/logo.png" alt="siteLogo" /></a>
                        <a :href="baseUrl + '/my-cart'" class="shirnk-cart">
                            <i class="material-icons">shopping_cart</i>
                            <sup 
                                v-if="itemsInCart"
                                v-text="itemsInCart">
                            </sup>
                        </a>                        
                        <a href="#" data-activates="mobile-demo" class="button-collapse shirnk-search"><i class="material-icons">search</i></a>

                    </div>
                    <div class="col m12 l12 main-header-wide">
                        <nav class="nav-extended light-blue lighten-2">
                            <div class="nav-wrapper nav-wrapper-height">
                                <div class="col s6 m6 l4">
                                    <ul id="" class="bannerHeader">
                                        <li><a href="{{ url('') }}"><img class="responsive-img logoMain" src="{{ asset('images/logo.png') }}" alt="siteLogo" /></a></li>
                                        <li><a href="https://play.google.com/store/apps/details?id=eshopping.ecommerce.e_shopping&hl=en" target="_blank">
                                            <img class="responsive-img google  hide-on-small-and-down" src="{{ asset('images/banner_google.png') }}" alt="Bixop" style="    margin-left: 30px;" />
                                        </a></li>
                                    </ul>
                                </div>
                                <div class="col s6 m6 l4">

                                    <div class="searchButton  hide-on-med-and-down">
                                        <i class="fa fa-search hide-on-small-and-down"></i>
                                        <form action="{{ route('search') }}" method="get">
                                            <input name="search" class="" type="text"  placeholder="Search...">    
                                        </form>

                                    </div>
                                </div>

                                <div class="col m6 l4 hide-on-med-and-down">

                                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                                        <li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>

                                    @if ( ! auth()->user() )

                                        <li class="dropdownMenu" >
                                            <a href="#profile"> <i class="fa fa-caret-down"></i> Guest</a>
                                            <ul>
                                                <li><a href="{{ route('login') }}"><i class="fa fa-sign-in"></i> Login</a></li>
                                                <li><a href="{{ route('register') }}"><i class="fa fa-plus"></i> Register</a></li>
                                            </ul>
                                        </li>
                                        
                                    @else
                                        
                                        <li class="dropdownMenu" >
                                            <a href="#profile"> <i class="fa fa-caret-down"></i> {{ auth()->user()->name }} </a>
                                            <ul>
                                                {{-- <li><a href="profile.php"><i class="fa fa-user"></i> Profile</a></li> --}}
                                                <li><a :href="baseUrl + '/my-cart'"><i class="fa fa-shopping-bag"></i> Shopping Cart</a></li>
                                                {{-- <li><a href="rewards.php"><i class="fa fa-gift"></i> Rewards</a></li> --}}
                                                <li><a href="https://play.google.com/store/apps/details?id=eshopping.ecommerce.e_shopping&hl=en" target="_blank"><i class="fa fa-mobile-phone"></i> Mobile Apps</a></li>
                                                <li><a :href="baseUrl + '/order-history'"><i class="fa fa-history"></i> Order History</a></li>
                                                <li><a href="{{ route('settings') }}"><i class="fa fa-wrench"></i> Settings</a></li>
                                                {{-- <li><a href="help.php"><i class="fa fa-question"></i> Help</a></li> --}}
                                                <li>
                                                    <a  href="{{ route('logout') }}"
                                                        onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                                    <i class="fa fa-sign-out"></i> Logout
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>

                                                 </li>
                                            </ul>
                                        </li>
                                        
                                    @endif

                                        <li><a href="#" id="cartCount" @click.prevent=""><img class="responsive-img shoppingCartImg" src="images/shopping-cart.png" alt="shopping cart" />

                                            <sup 
                                                v-if="itemsInCart"
                                                v-text="itemsInCart">
                                            </sup>
                                            
                                        </a></li>
                                        {{-- <li><a href="notifications.php"><i class="fa  fa-bell fa-2x"></i><sup>5</sup></a></li> --}}
                                    </ul>
                                </div>
                            </div>

                        </nav>
                    </div>
                    <a href="#" data-activates="mobile-demo" class="button-collapse" id="mobile-menu-icon"><i class="material-icons">menu</i></a>
                    <ul class="side-nav" id="mobile-demo">
                        <li>
                            <form accept="" method="get">
                                <input type="text" name="search" placeholder="Search" style="padding: 0 30px;" autofocus="">
                            </form>
                        </li>
                        <li><a href="https://play.google.com/store/apps/details?id=eshopping.ecommerce.e_shopping&hl=en"><i class="material-icons">system_update_alt</i>Download App</a></li>
                        <li><a href="/"><i class="material-icons">home</i>Home</a></li>
                        <li><a :href="baseUrl + '/my-cart'"><i class="material-icons">shopping_cart</i>Shopping Cart</a></li>
                        <li><a :href="baseUrl + '/order-history'"><i class="material-icons">subject</i>Order History</a></li>
                        <li><a href="{{ route('help') }}"><i class="material-icons">help</i>Help</a></li>
                        <li><a href="{{ route('settings') }}">Settings</a></li>
                        <li><a href="{{ route('terms') }}">Terms of Service</a></li>
                        <li><a href="{{ route('privacy-policy') }}">Privacy Policy</a></li>
                        <li><a href="{{ route('return-policy') }}">Return Policy</a></li>
                        @if( auth()->user() )
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">Logout</a></li>
                        @else
                        <li><a href="{{ route('login') }}">LogIn</a></li>
                        @endif
                    </ul>
                    @include('layouts.partials.miniCart') 
                </div>
            </div>
        </header>