
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

<!--Import jQuery before materialize.js-->
{{-- <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script> --}}
<script type="text/javascript" src="{{ asset('js/pgwslideshow.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/materialize.min.js') }}"></script>


<script>
    $(document).ready(function () {
        $(".button-collapse").sideNav();
        $('ul.tabs').tabs();
        $('.scrollspy').scrollSpy();
        $('.modal').modal();
        $('.pgwSlideshow').pgwSlideshow();

        $("#cartCount").click(function () {
            $("#popupCart").css("display", "block");
        });
        $("#cartCloseBtn").click(function () {
            $("#popupCart").css("display", "none");
        });
    });
</script>

 @stack('script')