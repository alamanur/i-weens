
    @if(session('warning'))
       <div class="" style="padding-top: 105px; text-align: center">
            <div class="red white-text"><strong>some message</strong></div>
        </div>
    @endif

    @if(session('success'))
    @push('script') 
     <script type="text/javascript">Materialize.toast('{{ session('success') }}', 2000,'light-blue z-depth-5')</script>
    @endpush 
    @endif

    @if(session('error'))
    @push('script') 
     <script type="text/javascript">Materialize.toast('{{ session('error') }}', 2000,'red z-depth-5')</script>
    @endpush 
    @endif
