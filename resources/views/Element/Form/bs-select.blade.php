
        <div class="form-group {{ $errors->has($name) ? ' has-error' : '' }}">

          <label class="control-label" for="{{ $name }}">{{ $label or ucwords(str_replace('_', ' ', $name)) }} {!! isset($required) ? '<span style="color:red">*</span>' : '' !!}</label>

          <select class="form-control" id="{{ $name }}" name="{{ $name }}">
            <option value="">Select {{ $label or ucwords(str_replace('_', ' ', $name)) }}</option>

            @foreach($options as $option)
              <option value="{{ $option->$val }}">{{ isset($show) ? $option->$show : $option->$val }}</option>
            @endforeach

          </select>

            @if(isset($help))
                <span class="help-block">
                    <strong> {{ $help }} </strong>
                </span>
            @endif

            @if ($errors->has($name))
                <span class="help-block">
                    <strong>{{ $errors->first($name) }}</strong>
                </span>
            @endif

        </div>

        @if ( old($name) )
          <script type="text/javascript">
              document.getElementById("{{ $name }}").value = {{ old($name) }};
          </script>
        @elseif(isset($value))
          <script type="text/javascript">
              document.getElementById("{{ $name }}").value = {{ $value }};
          </script>
        @endif