
    <div class="form-group {{ $errors->has($name) ? ' has-error' : '' }}">

        <label class="control-label" for="{{ $name }}">
            {{ $label or ucwords(str_replace('_', ' ', $name)) }} 
            {!! isset($required) ? '<span style="color:red">*</span>' : '' !!}
        </label>

        <input 
            id="{{ $name }}" 
            type="{{ $type or 'text'}}" 
            {{ $attr or ''}} 
            class="form-control" 
            name="{{ $name }}" 
            value="{{   !empty(old($name)) 
                        ?   old($name)
                        :   (isset($value) 
                            ? $value
                            : '' ) 
                            }}"

            {{ $required or '' }}
        />

        @if(isset($help))
            <span class="help-block">
                <strong> {{ $help }} </strong>
            </span>
        @endif

        @if ($errors->has($name))
            <span class="help-block">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
        @endif

    </div>