
    <div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">

        <label class="control-label" for="{{ $name }}">
            {{ ucwords(str_replace('_', ' ', $name)) }} {!! $required ? '<span style="color:red">*</span>' : '' !!}
        </label>

        <textarea 
            id="{{ $name }}" 
            class="form-control" 
            name="{{ $name }}" 
            {{ $required or '' }}
            >
                {!! !empty(old($name)) 
                        ?   old($name)
                        :   (isset($value) 
                            ? $value
                            : '' ) 
                 !!}
        </textarea>

        @if(isset($help))
            <span class="help-block">
                <strong> {{ $help }} </strong>
            </span>
        @endif

        @if ($errors->has($name))
            <span class="help-block">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
        @endif

    </div>