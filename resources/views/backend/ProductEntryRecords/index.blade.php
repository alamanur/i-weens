@extends('backend.layout.master')

@section('title', $title)

@section('main-content') 

@push('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('admin_assets')}}/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
    th { vertical-align: top; }
</style>
@endpush

<div class="box">
    <div class="box-header">
        <h3 class="box-title">{{ $title }}</h3>

    </div>
    <!-- /.box-header -->
    <div class="box-body">

    

        <table id="example1" class="table table-bordered table-striped text-center">
            <thead>
                <tr class="">
                    <th>ID</th>
                    <th>Product</th>
                    <th>Image</th>
                    <th>size</th>
                    <th>color</th>
                    <th>quantity</th>
                    <th>Price (Front)</th>
                    <th>Discount (Front)</th>
                    <th>Added in</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @if($products)
                @foreach($products as $product)

                <tr>
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->product->title }}</td>
                    <td><img src="{{ asset($product->product->front_image) }}" width="40" /></td>
                    <td>{{ $product->size }}</td>
                    <td>{{ $product->color }}</td>
                    <td>{{ $product->quantity }}</td>
                    <td>{{ $product->price }}</td>
                    <td>{{ $product->discount }}</td>
                    <td>{{ $product->created_at }}</td>
                    <td>
                      <div class="col-sm-8">
                       {{-- <form class="form-inline" action="{{ route('products.destroy', ['product' => $product->id]) }}" method="post">
                          {{ csrf_field() }}
                          {{ method_field('DELETE') }}
                          <button type="submit" class="btn-sm btn-danger pull-right edit" onclick="return confirm('Are you sure')">
                              <i class="glyphicon glyphicon-edit"></i> Delete
                          </button>
                       </form> --}}
                     </div>
                      {{-- <a href="" class="btn-sm btn-danger pull-right edit">
                          <i class="glyphicon glyphicon-trash"></i> Delete
                      </a> --}}
                      <a href="{{ route('product-entry-records.edit', ['product_entry_record' => $product->id ] ) }}" class="btn-sm btn-warning pull-right edit">
                          <i class="glyphicon glyphicon-edit"></i> Edit
                      </a>
                      {{-- <a href="http://localhost:8001/admin/posts/1" class="btn-sm btn-warning pull-right">
                          <i class="glyphicon glyphicon-eye-open"></i> View
                      </a> --}}
                    </td>
                    
                </tr>
                @endforeach
                @endif


            </tbody>

        </table>
     
        <div class="text-center">{{ $products->links() }}</div>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->


@push('scripts')
<!-- DataTables -->
<script src="{{asset('admin_assets')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('admin_assets')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
        $(function () {
        // $("#example1").DataTable();
          $('#example1').DataTable({
                  "paging": false,
                  "lengthChange": false,
                  "searching": true,
                  "ordering": true,
                  "info": true,
                  "autoWidth": false,
                  "columns": [
                  { "searchable": true },
                  { "searchable": true },
                  { "searchable": false },
                  { "searchable": false },
                  { "searchable": false },
                  { "searchable": false },
                  { "searchable": false },
                  { "searchable": false },
                  { "searchable": false },
                  { "searchable": false },
                  ]
          });

        });
</script>
@endpush
@endsection