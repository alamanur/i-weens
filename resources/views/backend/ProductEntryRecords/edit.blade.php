@extends('backend.layout.master')

@section('title', $title)


@section('main-content') 
@push('css')
@endpush



<div class="box box-success">

  <div class="box-header ui-sortable-handle" style="cursor: move;">
    <i class="fa fa-th"></i>

    <h3 class="box-title">{{ $title }}</h3>


  </div>
         
  <div class="box-body">
     
  <div class="box-footer">
    <form method="post" action="{{ route('product-entry-records.update', ['product_entry_record' => $ProductEntryRecord->id]) }}" enctype="multipart/form-data">
      
      {{ csrf_field() }}
      {{ method_field("PUT") }}

        <div class="col-md-6 pull-right">
          <img src="{{ asset($ProductEntryRecord->product->front_image) }}" alt="{{ $ProductEntryRecord->product->title }}" />
        </div>
        <div class="col-md-6">  
        <table class="table table-stripped table-responsive">
          <tr>
            <th>Size</th>
            <td>: {{ $ProductEntryRecord->size }}</td>
          </tr>
          <tr>
            <th>Color</th>
            <td>: {{ $ProductEntryRecord->color }}</td>
          </tr>

          @if ($errors->has('price'))
              <tr>
              <td></td>
                <td>
                  <strong style="color: red;">{{ $errors->first('price') }}</strong>
                </td>
              </tr>
          @endif

          <tr>
            <th>Price</th>
            <td>: 
              <input type="number" style="background:{{ $errors->has('price') ? 'red' : 'lightgreen'}};font-weight:bolder;" name="price" value="{{ $ProductEntryRecord->price }}">
              {{-- $ProductEntryRecord->price --}}
            </td>
          </tr>


          @if ($errors->has('discount'))
              <tr>
              <td></td>
                <td>
                  <strong style="color: red;">{{ $errors->first('discount') }}</strong>
                </td>
              </tr>
          @endif

          <tr>
            <th>Discount</th>
            <td>: 
              <input type="number" style="background:{{ $errors->has('discount') ? 'red' : 'lightgreen'}};font-weight:bolder;" name="discount" value="{{ $ProductEntryRecord->discount }}">
              {{-- $ProductEntryRecord->discount --}}
            </td>
          </tr>

          <tr>
            <th>Added</th>
            <td>: {{ $ProductEntryRecord->created_at }}</td>
          </tr>
          <tr>
            <th>Updated</th>
            <td>: {{ $ProductEntryRecord->updated }}</td>
          </tr>
          @if ($errors->has('quantity'))
              <tr>
              <td></td>
                <td>
                  <strong style="color: red;">{{ $errors->first('quantity') }}</strong>
                </td>
              </tr>
          @endif
          <tr>
            <th>Quantity</th>
            <th>: <input type="number"  autofocus style="background:{{ $errors->has('quantity') ? 'red' : 'lightgreen'}};font-weight:bolder;" name="quantity" value="{{ $ProductEntryRecord->quantity }}"></th>
          </tr>

          <tr>
            <td></td>
            <td><button type="submit" class="btn btn-warning "><i class="fa fa-edit fa-lg"></i>Change</button></td>
          </tr>
        </table>
        </div>
      </form>
  </div>

  </div>
</div>



@push('scripts')
@endpush
@endsection
