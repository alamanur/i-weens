<!DOCTYPE html>
<html lang="en">

@section('htmlheader')
    @include('backend.layout.partials.htmlheader')
@show
<body class="skin-blue sidebar-mini">
<div id="app">
    <div class="wrapper">

    @include('backend.layout.partials.mainheader')

    @include('backend.layout.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('backend.layout.partials.contentheader')

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @include('backend.layout.partials.flash-messages')
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('backend.layout.partials.mainfooter')


</div><!-- ./wrapper -->
</div>
@section('scripts')
    @include('backend.layout.partials.scripts')
@show
	
</body>
</html>
