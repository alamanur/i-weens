<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <!-- <img src="{{asset('public/student_image/user.png')}}" class="img-circle" alt="User Image"> -->
            </div>
            <div class="pull-left info">
            </div>
        </div>

        <!-- search form (Optional) -->
        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form> -->
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            
            <li class="header"></li>
            
            <li class="{{ request()->is('/') ? 'active' : '' }}"><a href="{{url('/')}}"><i class="fa fa-lg fa-home"></i> <span>Dashboard</span></a></li>
            
            <li class="treeview">
                <a href="#"> <span>Operations</span></a>
            </li>

            <li class="{{ request()->is('*/categories*') ? 'active' : '' }}"><a href="{{ route('categories.index') }}"><i class="ion fa-lg ion-bag "></i><span>Categories</span></a></li>

            <li class="treeview {{ request()->is('*/products*') ? 'active' : '' }}">
                <a href="{{ route('products.index') }}">
                    <i class="ion fa-lg ion-bag"></i><span>Products</span>
                    <i class="fa fa-lg fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ request()->fullUrlIs(route('products.index')) ? 'active' : '' }}"><a href="{{ route('products.index') }}"><i class="fa fa-lg fa-list-ul text-orange "></i> All Products</a></li>
                    <li class="{{ request()->fullUrlIs(route('products.create')) ? 'active' : '' }}"><a href="{{ route('products.create') }}"><i class="fa fa-lg fa-plus text-green"></i> Add Product</a></li>
               </ul>
            </li>

            <li class="">
                <a href="{{ route('product-entry-records.index') }}">
                    <i class="ion fa-lg ion-bag"></i><span>Products Entry Records</span>
                </a>
            </li>
            <li class="">
                <a href="{{ route('customer-orders.index') }}">
                    <i class="ion fa-lg ion-bag"></i><span>Bills</span>
                </a>
            </li>

            {{-- <li class="treeview ">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Forms</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
                <li><a href="advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
                <li class="active"><a href="editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
              </ul>
            </li> --}}
     

            
            <!-- <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li> -->
            
        </ul>
        
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
