
@if(session('warning'))
    <div class="alert alert-xs alert-warning"><strong>{{ session('warning') }}</strong></div>
@endif

@if(session('success'))
    <div class="alert alert-xs alert-success"><strong>{{ session('success') }}</strong></div>
@endif

@if(session('danger'))
    <div class="alert alert-xs alert-danger"><strong>{{ session('danger') }}</strong></div>
@endif