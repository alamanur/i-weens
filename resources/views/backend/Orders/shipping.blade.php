@extends('backend.layout.master')

@section('title', $title)

@section('main-content')
@push('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('admin_assets')}}/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
    th { vertical-align: top; }
</style>
@endpush


<div class="box box-primary">
            <div class="box-header ui-sortable-handle" style="cursor: move;">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">{{ $title}}</h3>
              
            </div>
            <!-- /.box-header -->
    <div class="box-body">

    

        <table class="table table-bordered table-striped">
                <tr>
                    <th>Name</th>
                    <td>:{{ $customer_order->shipping->name }}</td>
                </tr>
                <tr>
                    <th>Address</th>
                    <td>:{{ $customer_order->shipping->address }}</td>
                </tr>
                <tr>
                    <th>Contact no</th>
                    <td>:{{ $customer_order->shipping->contact_no }}</td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>:{{ $customer_order->shipping->email }}</td>
                </tr>
                <tr>
                    <th>Zip Code</th>
                    <td>:{{ $customer_order->shipping->zip_code }}</td>
                </tr>
                <tr>
                    <th>City</th>
                    <td>:{{ $customer_order->shipping->city }}</td>
                </tr>
                <tr>
                    <th>Country</th>
                    <td>:{{ $customer_order->shipping->country }}</td>
                </tr>

        </table>
     
        {{-- <div class="text-center">{{ $products->links() }}</div> --}}
    </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
              {{-- <button type="submit" class="btn btn-warning pull-right"><i class="fa fa-trash"></i> Delete Selected</button> --}}
            </div>
          </div>




@push('scripts')
<!-- DataTables -->
<script src="{{asset('admin_assets')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('admin_assets')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
        $(function () {
        // $("#example1").DataTable();
          $('#example1').DataTable({
          "paging": false,
                  "lengthChange": false,
                  "searching": true,
                  "ordering": true,
                  "info": true,
                  "autoWidth": false,
                  "columns": [
                  { "searchable": true },
                  { "searchable": true },
                  { "searchable": false },
                  { "searchable": true },
                  ]
          });

        });
</script>
@endpush
@endsection
