@extends('backend.layout.master')

@section('title', $title)

@section('main-content')
@push('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('admin_assets')}}/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
    th { vertical-align: top; }
</style>
@endpush




<div class="box box-success">

  <div class="box-header ui-sortable-handle" style="cursor: move;">
    <i class="fa fa-th"></i>

    <h3 class="box-title">{{ $title }}</h3>

  </div>
     
</div>


<div class="box box-primary">
            <div class="box-header ui-sortable-handle" style="cursor: move;">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">{{ $title}}</h3>
              
            </div>
            <!-- /.box-header -->
    <div class="box-body">

    

        <table id="example1" class="table table-bordered table-striped text-center">
            <thead>
                <tr class="">
                    <th>ID</th>
                    <th>Product</th>
                    <th>Size</th>
                    <th>Color</th>
                    <th>Quantity</th>
                    <th>Price (/pcs)</th>
                    <th>Discount (/pcs)</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 0; ?>
                @if($purchases)
                @foreach($purchases as $purchase)

                <tr>
                    <td>{{ $purchase->id }}</td>
                    <td>{{ $purchase->product_stock->title }}</td>
                    <td>{{ $purchase->product_stock->size }}</td>
                    <td>{{ $purchase->product_stock->color }}</td>
                    <td>{{ $purchase->quantity }}</td>
                    <td>{{ $purchase->price }}</td>
                    <td>{{ $purchase->discount }}</td>
                    <td>{{ $total = $purchase->quantity * ($purchase->price - $purchase->discount) }}</td>
                    <?php $i += $total; unset($total); ?>                    
                </tr>
                @endforeach
                @endif
            </tbody>
            <tfoot>
                <tr class="">
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Total:</th>
                    <th>{{ $i }}</th>
                </tr>
            </tfoot>

        </table>
     
        {{-- <div class="text-center">{{ $products->links() }}</div> --}}
    </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
              {{-- <button type="submit" class="btn btn-warning pull-right"><i class="fa fa-trash"></i> Delete Selected</button> --}}
            </div>
          </div>




@push('scripts')
<!-- DataTables -->
<script src="{{asset('admin_assets')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('admin_assets')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
        $(function () {
        // $("#example1").DataTable();
          $('#example1').DataTable({
          "paging": false,
                  "lengthChange": false,
                  "searching": true,
                  "ordering": true,
                  "info": true,
                  "autoWidth": false,
                  "columns": [
                  { "searchable": true },
                  { "searchable": true },
                  { "searchable": false },
                  { "searchable": true },
                  ]
          });

        });
</script>
@endpush
@endsection
