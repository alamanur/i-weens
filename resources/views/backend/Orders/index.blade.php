@extends('backend.layout.master')

@section('title', $title)

@section('main-content')
@push('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('admin_assets')}}/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
    th { vertical-align: top; }
</style>
@endpush


<div class="box box-primary">
            <div class="box-header ui-sortable-handle" style="cursor: move;">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">{{ $title}}</h3>
              
            </div>
            <!-- /.box-header -->
    <div class="box-body">

    

        <table id="example1" class="table table-bordered table-striped text-center">
            <thead>
                <tr class="">
                    <th>ID</th>
                    <th>User</th>
                    <th>TrxID</th>
                    <th>Trx Phone</th>
                    <th>Date</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @if($orders)
                @foreach($orders as $order)

                <tr>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->user->name }}</td>
                    <td>{{ $order->trx_id }}</td>
                    <td>{{ $order->trx_phone }}</td>
                    <td>{{ $order->created_at }}</td>
                    <td>
                      <a href="{{ route('customer-orders.shipping', ['customer_order' => $order->id]) }}" class="btn-sm btn-info pull-right edit">
                          <i class="fa fa-truck fa-lg"></i>&nbsp; Shipping
                      </a>
                      <a href="{{ route('customer-orders.show', ['col3' => $order->id ] ) }}" class="btn-sm btn-warning pull-right edit">
                          <i class="fa fa-list fa-lg"></i>&nbsp;  Details
                      </a>
                      {{-- <a href="http://localhost:8001/admin/posts/1" class="btn-sm btn-warning pull-right">
                          <i class="glyphicon glyphicon-eye-open"></i> View
                      </a> --}}
                    </td>
                    
                </tr>
                @endforeach
                @endif


            </tbody>

        </table>
     
        {{-- <div class="text-center">{{ $products->links() }}</div> --}}
    </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
              {{-- <button type="submit" class="btn btn-warning pull-right"><i class="fa fa-trash"></i> Delete Selected</button> --}}
            </div>
          </div>




@push('scripts')
<!-- DataTables -->
<script src="{{asset('admin_assets')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('admin_assets')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
        $(function () {
        // $("#example1").DataTable();
          $('#example1').DataTable({
          "paging": false,
                  "lengthChange": false,
                  "searching": true,
                  "ordering": true,
                  "info": true,
                  "autoWidth": false,
                  "columns": [
                  { "searchable": true },
                  { "searchable": true },
                  { "searchable": false },
                  { "searchable": true },
                  ]
          });

        });
</script>
@endpush
@endsection
