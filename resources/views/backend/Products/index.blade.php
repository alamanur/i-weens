@extends('backend.layout.master')

@section('title', $title)

@section('main-content') 

@push('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('admin_assets')}}/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
    th { vertical-align: top; }
</style>
@endpush


<div class="box">
    <div class="box-header">
        <h3 class="box-title">{{ $title }}</h3>

        <a href="{{ route('products.create') }}" class="btn btn-success pull-right"><span class="fa fa-plus"></span>&nbsp;&nbsp; Add Product</a>

    </div>
    <!-- /.box-header -->
    <div class="box-body">

    

        <table id="example1" class="table table-bordered table-striped text-center">
            <thead>
                <tr class="">
                    <th>ID</th>
                    <th>Product Title</th>
                    <th>Image</th>
                    <th>Category</th>
                    <th>Price (Front)</th>
                    <th>Discount (Front)</th>
                    <th>Status</th>
                    <th>Stock Status</th>
                    <th width="200">Actions</th>
                </tr>
            </thead>
            <tbody>
                @if($products)
                @foreach($products as $product)

                <tr>
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->title }}</td>
                    <td><img src="{{ asset($product->front_image) }}" width="40" /></td>
                    <td>{{ $product->category->name }}</td>
                    <td>{{ $product->front_price }}</td>
                    <td>{{ $product->front_discount }}</td>
                    <td>{!! ($product->status == 1) ? "<label class='label bg-green'>Active</label>" : "<label class='label bg-red'>Inactive</label>" !!}</td>
                    <td>{!! ($product->stock_status == 0) ? "<label class='label bg-red'>Out of Stock</label>" : "<label class='label bg-green'>In Stock</label>" !!}</td>
                    <td>
                    <div class="col-sm-8">
                     {{-- <form class="form-inline" action="{{ route('products.destroy', ['product' => $product->id]) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" class="btn-sm btn-danger pull-right edit" onclick="return confirm('Are you sure')">
                            <i class="glyphicon glyphicon-edit"></i> Delete
                        </button>
                     </form> --}}
                   </div>
                      <a href="{{ route('products.edit', ['product' => $product->id]) }}" class="btn-sm btn-primary pull-right edit">
                          <i class="glyphicon glyphicon-edit"></i> Edit
                      </a>
                      <a href="{{ route('manage.show', ['product' => $product->id]) }}" class="btn-sm btn-warning pull-right edit">
                          <i class="glyphicon glyphicon-edit"></i> Manage
                      </a>
                      <a href="{{ route('products.gallery', ['product' => $product->id]) }}" class="btn-sm btn-success pull-right edit">
                          <i class="glyphicon glyphicon-edit"></i> Gallery
                      </a>
                      {{-- <a href="http://localhost:8001/admin/posts/1" class="btn-sm btn-warning pull-right">
                          <i class="glyphicon glyphicon-eye-open"></i> View
                      </a> --}}
                    </td>
                    
                </tr>
                @endforeach
                @endif


            </tbody>

        </table>
     
        <div class="text-center">{{ $products->links() }}</div>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->


@push('scripts')
<!-- DataTables -->
<script src="{{asset('admin_assets')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('admin_assets')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
        $(function () {
        // $("#example1").DataTable();
          $('#example1').DataTable({
          "paging": false,
                  "lengthChange": false,
                  "searching": true,
                  "ordering": true,
                  "info": true,
                  "autoWidth": false,
                  "columns": [
                  { "searchable": true },
                  { "searchable": true },
                  { "searchable": false },
                  { "searchable": true },
                  { "searchable": false },
                  { "searchable": false },
                  { "searchable": false },
                  { "searchable": false },
                  ]
          });

        });
</script>
@endpush
@endsection