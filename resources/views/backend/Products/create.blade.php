@extends('backend.layout.master')

@section('title', 'Products')


@section('main-content')
@push('css')
@endpush


<div class="box box-success">

  <div class="box-header ui-sortable-handle" style="cursor: move;">
    <i class="fa fa-th"></i>

    <h3 class="box-title">Add Product</h3>

  </div>
         
  <div class="box-footer">
    <form method="post" action="{{ route('products.store') }}" enctype="multipart/form-data">
      
      {{ csrf_field() }}


        @include('Element.Form.bs-select', [
            'name' => 'category_id', 
            'label' => 'Category', 
            'required' => 'required', 
            'help' => '', 
            'options' => $categories, 
            'val' => 'id', 
            'show' => 'name'
          ])

        @include('Element.Form.bs-input', [
            'name' => 'title', 
            'required' => 'required', 
            'help' => ''
          ])

        @include('Element.Form.bs-input', [
            'name' => 'front_price',
            'label' => 'Price',
            'help'  => 'Price to be displayed in Front',
            'type' => 'number',
            'required' => 'required',
            'attr' => 'min=1 step=0.01'
          ])

        @include('Element.Form.bs-input', [
            'name' => 'front_discount',
            'label' => 'Discount',
            'help' => 'Discount to be displayed in Front',
            'type' => 'number',
            'attr' => 'min=1 step=0.01'
          ])

        @include('Element.Form.bs-input', [
            'name' => 'front_image', 
            'type' => 'file', 
            'required' => 'required', 
            'attr' => 'accept=image/*'
          ])
        
        @include('Element.Form.bs-textarea', [
            'name' => 'description', 
            'required' => 'required', 
            'help' => ''
          ])

        <div class="form-group">
          <button type="submit" class="btn btn-success"><i class="fa fa-plus"> Add Product</i></button>
        </div>

      </form>
  </div>
</div>


@push('scripts')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/full/ckeditor.js"></script>

<script>
  $(function () {
    // Replace the <textarea id="description"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('description');
  });
</script>
@endpush
@endsection
