@extends('backend.layout.master')

@section('title', $title)

@section('main-content')

@push('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('admin_assets')}}/plugins/dropzone/dropzone.css">
<style type="text/css">
  .relative {position: relative;}
  .abs-btn {position: absolute; top: 5px; right: 5px; background: transparent; cursor: pointer;}
</style>

@endpush


<div class="box">
    <div class="box-header">
        <h3 class="box-title">{{ $title }}</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="col-md-12">
        <form id="productGallery" class="dropzone" action="{{ url()->current() }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
        </form>
      </div>

      <div class="col-md-12">
         <div class="row" id="product_gallery">

         @foreach($product->images as $image)
          <div class="col-md-2" id="image_{{ $image->id }}">
            <div class="thumbnail relative">
                <img src="{{ asset($image->large_image) }}" alt="Fjords" style="width:100%">
                <div class="caption text-center">
                  <span href="#" class="abs-btn" onclick="remove_image('{{ route('products.gallery.remove', ['image' => $image->id ]) }}', '#image_{{ $image->id }}')"><i class="fa fa-trash fa-2x text-red"></i></span>
                </div>
            </div>
          </div>
          @endforeach

        </div>
      </div>

    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->


@push('scripts')
<!-- DataTables -->
<script src="{{asset('admin_assets')}}/plugins/dropzone/dropzone.js"></script>
<script type="text/javascript">
  Dropzone.options.productGallery = {
  paramName: "image",
  uploadMultiple: false,
  // thumbnailWidth: 200,
  // thumbnailHeight: 200,
  maxFiles: 5,
  acceptedFiles: 'image/*',
  maxFilesize: 2, // MB
  // addRemoveLinks: true, //||||||||||||||||||
  init: function() {
   //  this.on("removedfile", function(file) {
   //    x = confirm('Do you want to delete?');
   //    if(!x)  return false;
   // });
    this.on("success", function(file, response){

            // obj = JSON.parse(response);
            alert(response.message);
            $("#product_gallery").append(response.src);
    });
    this.on("error", function(file, response){

            // obj = JSON.parse(response);
            alert(response.image);
            $(file.previewElement).find('.dz-error-message').text(response.image);

    })
  },
};
</script>
<script type="text/javascript">
  function remove_image(link, id){
    $.ajax({
      type: "POST",
      url: link,
      data: { "_method" : "DELETE", "_token" : "{{ csrf_token() }}" },
      success: function( response ){
         alert(response.message);
         if (response.message != "error") {
          $(id).remove();
         }
      },
    });
  }
</script>
@endpush
@endsection