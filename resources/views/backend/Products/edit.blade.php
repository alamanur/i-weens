@extends('backend.layout.master')

@section('title', 'Products')


@section('main-content')
@push('css')
@endpush



<div class="box box-success">

  <div class="box-header ui-sortable-handle" style="cursor: move;">
    <i class="fa fa-th"></i>

    <h3 class="box-title">Update Product</h3>

  </div>
         
  <div class="box-body">
    <div class="col-md-3 pull-right">
      <img class="img-responsive pull-right" src="{{ asset($product->front_image) }}">
    </div>
    <form method="post" action="{{ route('products.update', [ 'products' => $product->id ]) }}" enctype="multipart/form-data">
      
      {{ csrf_field() }}

      {{ method_field('PATCH') }}

      <div class="col-md-9">
        @include('Element.Form.bs-select', [
            'name' => 'category_id', 
            'label' => 'Category', 
            'value' => $product->category_id, 
            'required' => 'required', 
            'help' => '', 
            'options' => $categories, 
            'val' => 'id', 
            'show' => 'name'
          ])
        @include('Element.Form.bs-input', [
            'name' => 'title', 
            'required' => 'required', 
            'value' => $product->title, 
            'help' => ''
          ])
        @include('Element.Form.bs-input', [
            'name' => 'front_price',
            'label' => 'Price',
            'value' => $product->front_price, 
            'help'  => 'Price to be displayed in Front',
            'type' => 'number',
            'required' => 'required',
            'attr' => 'min=1 step=0.01'
          ])
        @include('Element.Form.bs-input', [
            'name' => 'front_discount',
            'label' => 'Discount',
            'value' => $product->front_discount,
            'help' => 'Discount to be displayed in Front',
            'type' => 'number',
            'attr' => 'min=1 step=0.01'
          ])
        @include('Element.Form.bs-input', [
            'name' => 'front_image', 
            'type' => 'file',
            'attr' => 'accept=image/*',
            'help' => 'Image should be square in dimention and Max 1.5 MB'
          ])
      </div>

      <div class="col-md-12">      
        @include('Element.Form.bs-textarea', [
            'name' => 'description', 
            'value' => $product->description,
            'required' => 'required', 
            'help' => ''
          ])
        <div class="form-group">
          <button type="submit" class="btn btn-success"><i class="fa fa-edit"> Update Product</i></button>
        </div>
      </div>

      </form>
  </div>
</div>


@push('scripts')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/full/ckeditor.js"></script>

<script>
  $(function () {
    // Replace the <textarea id="description"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('description');
  });
</script>
@endpush
@endsection
