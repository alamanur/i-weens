@extends('backend.layout.master')

@section('title', 'Products')


@section('main-content') 
@push('css')
@endpush




<div class="box box-success">

  <div class="box-header ui-sortable-handle" style="cursor: move;">
    <i class="fa fa-th"></i>

    <h3 class="box-title">Products</h3>


  </div>
         
  <div class="box-body">
    <img src="{{ asset('/images/products/front/1501568436LZtcTYMEXZ.jpeg')}}">
    <img src="/images/products/front/1501568436LZtcTYMEXZ.jpeg">
    {{ asset('/images/products/front/1501568436LZtcTYMEXZ.jpeg')}}
    <br>
    /images/products/front/1501568436LZtcTYMEXZ.jpeg

  </div>
</div>



@push('scripts')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/full/ckeditor.js"></script>

<script>
  $(function () {
    // Replace the <textarea id="description"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('description');
  });
</script>
@endpush
@endsection
