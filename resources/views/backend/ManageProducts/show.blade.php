@extends('backend.layout.master')

@section('title', $title)

@section('main-content')

@push('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('admin_assets')}}/plugins/datatables/dataTables.bootstrap.css">
<style type="text/css">
    th { vertical-align: top; }
</style>
@endpush


<div class="box">
    <div class="box-header">
        <h3 class="box-title">{{ $title }}</h3>
        <a href="{{ route('manage.create', ['product' => $product->id]) }}" class="btn btn-success pull-right">
          <i class="fa fa-plus fa-sm"></i>
          &nbsp; Add New
        </a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

        <table id="example1" class="table table-bordered table-striped text-center">
            <thead>
                <tr class="">
                    <th>ID</th>
                    <th>Image</th>
                    <th>Size</th>
                    <th>Color</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Discount</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @if(isset($product->manage))
                @foreach($product->manage as $manage)
                <tr>
                    <td>{{ $manage->id }}</td>
                    <td onclick="showImage('{{ $manage->product_id }}', '{{ $manage->id }}', this)"><button class="btn btn-sm">show</button></td>
                    <td>{{ $manage->size }}</td>
                    <td>{{ $manage->color }}</td>
                    <td>{{ $manage->quantity }} <a href="{{ route('manage.increase', ['manage' => $manage->id]) }}" class="fa fa-lg text-green fa-plus pull-right"></a></td>
                    <td>{{ $manage->price }}</td>
                    <td>{{ $manage->discount }}</td>
                    <td>
                      <a href="{{ route('manage.edit', ['manage' => $manage->id ]) }}" class="btn-sm btn-warning pull-right">
                          <i class="glyphicon glyphicon-eye-open"></i> Edit
                      </a>
                    </td>

                </tr>
                @endforeach
                @endif


            </tbody>

        </table>

    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

@push('scripts')
<!-- DataTables -->
<script src="{{asset('admin_assets')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('admin_assets')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
        $(function () {
        // $("#example1").DataTable();
          $('#example1').DataTable({
          "paging": false,
                  "lengthChange": false,
                  "searching": true,
                  "ordering": true,
                  "info": true,
                  "autoWidth": false,
                  "columns": [
                  { "searchable": true },
                  { "searchable": true },
                  { "searchable": false },
                  { "searchable": true },
                  { "searchable": false },
                  { "searchable": false },
                  { "searchable": false },
                  { "searchable": false },
                  ]
          });


        });
</script>
<script type="text/javascript">
  var productImage =  {!! $product->images->toJson() !!};
function showImage(product_id, id,ob) {
  for(var x in  productImage) {
      if((productImage[x].product_id == product_id) && (productImage[x].product_stock_id == id) )
      {
          // $(ob).html('<img src="/'+ productImage[x].thumb_image + '" width="40" />');
          ob.innerHTML = '<img src="{{ asset('') }}'+ productImage[x].thumb_image + '" width="40" />';
          break;
      }
      else { ob.innerHTML = 'No Image'}
  }
}

</script>

@endpush
@endsection