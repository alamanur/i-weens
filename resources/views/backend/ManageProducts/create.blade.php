@extends('backend.layout.master')

@section('title', $title)


@section('main-content')
@push('css')
@endpush



<div class="box box-success">

  <div class="box-header ui-sortable-handle" style="cursor: move;">
    <i class="fa fa-th"></i>

    <h3 class="box-title">{{ $title }}</h3>


  </div>

  <div class="box-body">

  <div class="box-footer">
    <form method="post" action="{{ route('manage.store') }}" enctype="multipart/form-data">

      {{ csrf_field() }}


        <input type="hidden" name="product_id" value="{{ $product->id }}"/>

        <div class="col-md-6">
          @include('Element.Form.bs-input', [
              'name' => 'size',
              'help' => ''
            ])
        </div>

        <div class="col-md-6">
          @include('Element.Form.bs-input', [
              'name' => 'color',
              'help' => ''
            ])
        </div>

        <div class="col-md-4">
          @include('Element.Form.bs-input', [
              'name' => 'quantity',
              'type' => 'number',
              'required' => 'required',
              'attr' => 'min=1'
            ])
        </div>

        <div class="col-md-4">
          @include('Element.Form.bs-input', [
              'name' => 'price',
              'label' => 'Price',
              'value' => $product->front_price,
              'type' => 'number',
              'required' => 'required',
              'attr' => 'min=1 step=0.01'
            ])
        </div>

        <div class="col-md-4">
          @include('Element.Form.bs-input', [
              'name' => 'discount',
              'type' => 'number',
              'value' => $product->front_discount,
              'attr' => 'min=1 step=0.01'
            ])
        </div>

       {{--  @include('Element.Form.bs-input', [
            'name' => 'image',
            'type' => 'file',
            'attr' => 'accept=image/* ',
            'help' => 'Image must be sqare size'
          ]) --}}

        <div class="form-group">
          <button type="submit" class="btn btn-success"><i class="fa fa-plus"> Add</i></button>
        </div>

      </form>
  </div>

  </div>
</div>



@push('scripts')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/full/ckeditor.js"></script>

<script>
  $(function () {
    // Replace the <textarea id="description"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('description');
  });
</script>
@endpush
@endsection
