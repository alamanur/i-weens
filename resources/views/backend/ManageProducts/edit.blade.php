@extends('backend.layout.master')

@section('title', $title)


@section('main-content') 
@push('css')
@endpush


<div class="box box-success">

  <div class="box-header ui-sortable-handle" style="cursor: move;">
    <i class="fa fa-th"></i>

    <h3 class="box-title">{{ $title }}</h3>


  </div>
         
  <div class="box-body">
     
  <div class="box-footer">
    <form method="post" action="{{ route('manage.update', ['manage' => $manageProduct->id]) }}" enctype="multipart/form-data">
      
      {{ csrf_field() }}
      {{ method_field('PUT') }}


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

        <div class="col-md-6 pull-right">
          @if($image)
            <img src="{{ asset($image->large_image) }}" class="img-responsive" />
          @else
          <h3>No Images Available For This Option</h3>
          @endif
        </div>          
        <div class="col-md-6">    
          <input type="hidden" name="product_id" value="{{ $manageProduct->product_id }}">      
          <input type="hidden" name="image_id" value="{{ $image->id }}">      
          @include('Element.Form.bs-input', [
              'name' => 'size', 
              'value' => $manageProduct->size,
              'help' => ''
            ])

          @include('Element.Form.bs-input', [
              'name' => 'color', 
              'value' => $manageProduct->color, 
              'help' => ''
            ])

          @include('Element.Form.bs-input', [
              'name' => 'price',
              'value' => $manageProduct->price,
              'label' => 'Price',
              'type' => 'number',
              'required' => 'required',
              'attr' => 'min=1 step=0.01'
            ])

          @include('Element.Form.bs-input', [
              'name' => 'discount',
              'value' => $manageProduct->discount,
              'type' => 'number',
              'attr' => 'min=1 step=0.01'
            ])

        @include('Element.Form.bs-input', [
            'name' => 'image', 
            'label' => 'Update Image',
            'type' => 'file', 
            'attr' => 'accept=image/*', 
            'help' => 'Image must be sqare size'
          ])


          <div class="form-group">
            <button type="submit" class="btn btn-success"><i class="fa fa-plus"> Update </i></button>
          </div>
        </div>

      </form>
  </div>

  </div>
</div>



@push('scripts')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/full/ckeditor.js"></script>

<script>
  $(function () {
    // Replace the <textarea id="description"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('description');
  });
</script>
@endpush
@endsection
