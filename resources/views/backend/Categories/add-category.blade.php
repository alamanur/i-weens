@extends('backend.layout.master')

@section('title', 'Sale Records')


@section('main-content')
@push('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('admin_assets')}}/plugins/datatables/dataTables.bootstrap.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{asset('admin_assets')}}/plugins/datepicker/datepicker3.css">
  <!-- Select2 -->
  <!-- <link rel="stylesheet" href="{{asset('admin_assets')}}/plugins/select2/select2.min.css"> -->
  <style type="text/css">
    th { vertical-align: top; }
  </style>
@endpush




===============
{{ Request::current() }}




@push('scripts')
<!-- DataTables -->
<script src="{{asset('admin_assets')}}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('admin_assets')}}/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="{{asset('admin_assets')}}/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Select2 -->
<!-- <script src="{{asset('admin_assets')}}/plugins/select2/select2.full.min.js"></script> -->
<!-- page script -->
<script>
  $(function () {
    // $("#example1").DataTable();
    $('#example1').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "columns": [
        { "searchable": true },
        { "searchable": true },
        { "searchable": false },
        { "searchable": false },
        { "searchable": false },
        { "searchable": false },
        { "searchable": false },
        { "searchable": false },
        { "searchable": false },
        { "searchable": false },
        { "searchable": false },
      ] 
    });


    //Initialize Select2 Elements
    // $(".select2").select2();

    //Date picker
    $('#datepicker1').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    $('#datepicker2').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });


    @if(isset($product_id))
      $('#product_id').val('{{ $product_id }}');
    @endif


  });
  </script>
@endpush
@endsection
