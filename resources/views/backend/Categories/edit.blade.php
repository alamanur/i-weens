@extends('backend.layout.master')

@section('title', $title)


@section('main-content')
@push('css')

@endpush




<div class="box box-success">

  <div class="box-header ui-sortable-handle" style="cursor: move;">
    <i class="fa fa-th"></i>

    <h3 class="box-title">{{ $title }}</h3>

  </div>

  <div class="box-footer{{ $errors->has('name') ? ' has-error' : '' }}">
    <form method="post" action="{{ route('categories.update', ['category' => $category->id ]) }}">
      {{ csrf_field() }}
       {{ method_field('PUT') }}
      <div class="input-group input-group-lg">
        <input name="name" type="text" value="{{ $category->name }}" class="form-control" required>
        <span class="input-group-btn">
          <button type="submit" class="btn btn-warning btn-flat"><i class="fa fa-wrench"></i> Update</button>
        </span>
      </div>

       @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
      </form>
  </div>
</div>

@push('scripts')
@endpush
@endsection
