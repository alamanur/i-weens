@extends('backend.layout.master')

@section('title', 'Categories')


@section('main-content')
@push('css')

@endpush




<div class="box box-success">

  <div class="box-header ui-sortable-handle" style="cursor: move;">
    <i class="fa fa-th"></i>

    <h3 class="box-title">Add Category</h3>

  </div>

  <div class="box-footer{{ $errors->has('name') ? ' has-error' : '' }}">
    <form method="post" action="{{ route('categories.store') }}">
      {{ csrf_field() }}
      <div class="input-group input-group-lg">
        <input name="name" type="text" value="{{ old('name') }}" class="form-control" placeholder="Type a category..." required>
        <span class="input-group-btn">
          <button type="submit" class="btn btn-success btn-flat"><i class="glyphicon glyphicon-plus"></i> Add</button>
        </span>
      </div>

       @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
      </form>
  </div>
</div>


<div class="box box-primary">
            <div class="box-header ui-sortable-handle" style="cursor: move;">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">Categoies</h3>

            </div>
            <!-- /.box-header -->
            <form method="post">
            {{ csrf_field() }}
            <div class="box-body">
              <ul class="todo-list ui-sortable">
                @foreach($categories as $category)
                <li class="">
                      <span class="handle ui-sortable-handle">
                        {{ $loop->iteration }} &nbsp;
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                  <!-- checkbox -->
                  {{-- <input name="id[]" type="checkbox" value="{{ $category->id }}"> --}}
                  <!-- todo text -->
                  <span class="text">{{ $category->name }}</span>
                  {{-- <input class=" edit-{{ $category->id }}" type="text" name="name" value="{{ $category->name }}"> --}}
                  <!-- General tools such as edit or delete-->
                  <div class="tools">
                    <a href="{{ route('categories.edit', [ 'category' => $category->id]) }}"><span class="fa fa-2x fa-edit btn-edit-{{ $category->id }}" data-toggle="tooltip" title="Edit"></span></a>
                    <a href="#" onclick="delete_category('{{ route('categories.destroy', [ 'category' => $category->id]) }}')"><sapn class="fa fa-2x fa-trash-o bg-red" data-toggle="tooltip" title="delete"></sapn></a>
                  </div>
                </li>
                @endforeach
              </ul>
            </div>
            <!-- /.box-body -->
            </form>
          </div>


        <form id="delete-form" method="post">
          {{ csrf_field() }}
          {{ method_field('DELETE') }}
        </form>

@push('scripts')
<script>
$(".todo-list > li .tools button").click(function(event){
    event.preventDefault();
});
function delete_category(url) {
  if ( confirm('are you sure'))
  {
    $('#delete-form').attr('action', url);
    $('#delete-form').submit();
  }
}
</script>
@endpush
@endsection
