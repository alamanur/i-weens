@extends('layouts.master')
{{-- @section('title', 'Page Title') --}}
@section('content')
<section class="page" id="latestProduct">

    <div class="row">
        <div class="col s11 m10 l10 xl12 offset-l1 offset-m1 offset-s1">

@push('vueVariable')
<script type="text/javascript">
    window.products = {!! $products !!} ;            
</script>
@endpush
            <div class="row" id="product-list">
            {{-- @for($i = 1; $i < 20; $i++) --}}
            <product-list></product-list>

            {{-- @endfor --}}

            </div>

        </div>
    </div>
</section>

@endsection