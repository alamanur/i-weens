<?php include_once('inc/header.php'); ?>
<?php $cart = [1]; ?>
<div class="cartPage">
    <div class="container">

        <div class="row">
            <div class="col s10 offset-s1 l12">
                <div class="row">
                    <aside id="shipTo">
                        <div class="col m12 l5">
                            <div class="row">
                                <div class="col s12 ">
                                    <nav class="">
                                        <div class="nav-wrapper">
                                            <?php include_once 'inc/ship-menu.php'; ?>
                                        </div>
                                    </nav>

                                    <h2 class="orderSummary cartPageHeading">PAYMENT</h2>
                                    <div class="row">
                                        <div class="col s12">
                                            <ul class="tabs shippingOptions">
                                                <li class="tab col s6">
                                                    <a href="#test1">
                                                        <img src="images/cards/master.png" width="25%">
                                                        <img src="images/cards/visa.png" width="25%">
                                                        <img src="images/cards/express.png" width="25%">
                                                        <img src="images/cards/discover.png" width="25%">
                                                    </a>
                                                </li>
                                                <li class="tab col s6">
                                                    <a href="#test2"  class="active">
                                                        <img src="images/cards/others2.png" width="30%">
                                                    </a>
                                                </li>

                                            </ul>
                                        </div>
                                        <div id="test1" class="col s12">
                                            <div class="row">
                                                <!--SHIP TO form -->

                                                <form class="col s12" action="review.php" method="post">
                                                    <div class="row">
                                                        <div class="input-field col s10">
                                                            <input  id="first_name" type="text" class="validate">
                                                            <label for="first_name">Card Number <span class="required red-text" >*</span></label>
                                                        </div>
                                                        <div class="input-field col s10">
                                                            <input id="cvv" type="number" class="validate">
                                                            <label for="cvv">Security Code(CVV) <span class="required red-text" >*</span></label>
                                                        </div>


                                                        <div class="input-field col s10">
                                                            <input id="expire" type="text" class="validate">
                                                            <label for="expire">Expiration Date <span class="required red-text" >*</span></label>
                                                        </div>


                                                        <div class="input-field col s10">
                                                            <input id="zipCode" type="text" class="validate">
                                                            <label for="zipCode">Zip Code <span class="required red-text" >*</span></label>
                                                        </div>

                                                        <div class="col s12">
                                                            <button type="submit" class="waves-effect waves-light btn orange lighten-2">Next</button>
                                                        </div>
                                                    </div>
                                                </form> 


                                            </div> 
                                        </div>
                                        <div id="test2" class="col s12">

                                            <div class="row">
                                                <div class="col s12">
                                                    <div class="row">
                                                        <div class="col s12">
                                                            <div class="card horizontal">
                                                                <div class="card-image">
                                                                    <img src="images/cards/paypal.png">
                                                                </div>
                                                                <div class="card-stacked">
                                                                    <div class="card-content">
                                                                        <p>PayPal is the faster, safer way to send money, make an online payment, receive money or set up a merchant account.</p>
                                                                    </div>
                                                                    <div class="card-action">
                                                                        <a href="#">Pay On Paypal</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4 class="center-align">OR</h4>
                                                    <div class="row">
                                                        <div class="col s12">
                                                            <div class="card horizontal">
                                                                <div class="card-image">
                                                                    <img width="30%" src="images/cards/bkash.png">
                                                                </div>
                                                                <div class="card-stacked">
                                                                    <div class="card-content">
                                                                        <p>bKash is a mobile financial service in Bangladesh operating under the authority of Bangladesh Bank as a subsidiary of BRAC Bank Limited.</p>
                                                                    </div>
                                                                    <div class="card-action">
                                                                        <a  class="modal-trigger" href="#bkashPaymentModal">Pay on bKash</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4 class="center-align">OR</h4>
                                                    <div class="row">
                                                        <div class="col s12">
                                                            <div class="card horizontal">
                                                                <div class="card-image">
                                                                    <img width="30%" src="images/cards/cashondelivery.jpg">
                                                                </div>
                                                                <div class="card-stacked">
                                                                    <div class="card-content">
                                                                        <p>Cash on delivery (COD) is a type of transaction in which the recipient makes payment for a good at the time of delivery. If the purchaser does not make payment when the good is delivered, then the good is returned to the seller.</p>
                                                                    </div>
                                                                    <div class="card-action">
                                                                        <a href="#">Cash On Delivery</a>
                                                                        <form action="#" class="pull-right">
                                                                            <p>
                                                                                <input type="checkbox" class="filled-in" id="filled-in-box"  />
                                                                                <label for="filled-in-box">Check in</label>
                                                                            </p>
                                                                        </form>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <form class="col s12" action="review.php" method="post">
                                                            <button type="submit" class="waves-effect waves-light btn orange lighten-2">Next</button>
                                                        </form> 
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>  <!-- /.col -->
                    </aside>

                    <aside id="shoppingCart">
                        <div class="col m12 l7 offset-l0" style="background: none;">
                            <h2 class="shipPageHeading white lighten-3">SHOPPING CART</h2>
                            <article>

                                <div class="row">
                                    <div class="col s12">
                                        <div class="col s12 m5 l3">

                                            <div class="shipItemThumb" style="background-image: url(images/products/13.jpg); "></div>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons tiny">star</i>

                                        </div>
                                        <div class="col s12 m7 l9 cartItemInfo ">
                                            <h5 class="cartItemTitle" >
                                                Casual Genuine Leather Trainers Comfortable Men Sport Shoes Breathable Sneakers
                                            </h5>
                                            <p class="cartItemSize">Size: 8 <span class="cartItemColor">Color: red</span></p>                                

                                            <p class="shipQuantity">Quantity: 8 </p>      
                                            <p class="cartItemShipping">Shipping : $8.00</p>
                                            <p class="cartItemArrival">Estimated Arrival: Sep 2 - Sep 5</p>
                                            <p class="cartItemPrice"><span class="cartItemPriceOld">$93.00</span> $26.00</p>

                                        </div>
                                    </div>
                                </div>  <!-- /.row -->

                            </article>
                            <hr class="hide-on-med-and-up" />

                            <article>

                                <div class="row">
                                    <div class="col s12">
                                        <div class="col s12 m5 l3">

                                            <div class="shipItemThumb" style="background-image: url(images/products/11.jpg); "></div>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons tiny">star</i>


                                        </div>
                                        <div class="col s12 m7 l9 cartItemInfo ">
                                            <h5 class="cartItemTitle" >
                                                Casual Genuine Leather Trainers Comfortable Men Sport Shoes Breathable Sneakers
                                            </h5>
                                            <p class="cartItemSize">Size: 8 <span class="cartItemColor">Color: red</span></p>                                
                                            <p class="shipQuantity">Quantity: 8 </p>      
                                            <p class="cartItemShipping">Shipping : $8.00</p>
                                            <p class="cartItemArrival">Estimated Arrival: Sep 2 - Sep 5</p>
                                            <p class="cartItemPrice"><span class="cartItemPriceOld">$93.00</span> $26.00</p>

                                        </div>
                                    </div>
                                </div>  <!-- /.row -->

                            </article>
                            <hr class="hide-on-med-and-up" />

                            <article>

                                <div class="row">
                                    <div class="col s12">
                                        <div class="col s12 m5 l3">

                                            <div class="shipItemThumb" style="background-image: url(images/products/12.jpg); "></div>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons tiny">star</i>


                                        </div>
                                        <div class="col s12 m7 l9 cartItemInfo ">
                                            <h5 class="cartItemTitle" >
                                                Casual Genuine Leather Trainers Comfortable Men Sport Shoes Breathable Sneakers
                                            </h5>
                                            <p class="cartItemSize">Size: 8 <span class="cartItemColor">Color: red</span></p>                                
                                            <p class="shipQuantity">Quantity: 8 </p>                                
                                            <p class="cartItemShipping">Shipping : $8.00</p>
                                            <p class="cartItemArrival">Estimated Arrival: Sep 2 - Sep 5</p>
                                            <p class="cartItemPrice"><span class="cartItemPriceOld">$93.00</span> $26.00</p>

                                        </div>
                                    </div>
                                </div>  <!-- /.row -->

                            </article>
                            <hr class="hide-on-med-and-up" />

                        </div>  <!-- /.col -->
                    </aside>

                </div> <!-- /.row -->

            </div> <!-- /.col -->
        </div> <!-- /.row -->

    </div>
</div>

<?php include_once('inc/footer.php'); ?>
