<?php include_once('inc/header.php'); ?>
<div class="container">
    <div class="row">
        <div class="pageContentSection">
            <div class="col s10  m8 offset-s1 ">
                <h4 class="onPageTitle">Frequently Asked Questions</h4>
                
                <ul class="collapsible" data-collapsible="accordion">
                    <li id="shipAndDelivery">
                        <div class="collapsible-header active "><i class="material-icons">local_shipping</i>
                            <span class="faqTitle">Shipping and Delivery</span>
                        </div>
                        <div class="collapsible-body">
                            <h6 class="contentTopic">How long does shipping take?</h6>
                            <blockquote class="topicDetails">
                                Orders typically ship within 1-2 days. Shipping times vary based on the individual store and destination. To view estimated shipping times for your orders, please visit the Order History page: transaction. Choose "Where's My Package?" for more information. You can track your package's delivery progress at any time from this area.
                            </blockquote>
                            <h6 class="contentTopic">How can I track my order?</h6>
                            <blockquote class="topicDetails">If you want to check the delivery status of your order, you can find the most updated tracking information by visiting the Order History page: transaction.
                                Tap on "Where's my package?" to view your order’s estimated date of arrival and tracking history. Please note, depending on how the merchant ships your order, there may not be tracking information available.
                                If you have any other questions about the status of your order, you can contact our Customer Service team from your Order History section: transaction.
                            </blockquote>
                            <h6 class="contentTopic"> How much does shipping cost?</h6>
                            <blockquote class="topicDetails">
                                We strive to maintain low shipping costs. The shipping price associated with each item is displayed at checkout. The cost varies depending on the size, weight and destination of your order. Since items may ship from multiple merchants, shipping costs are applied separately by item ordered.
                            </blockquote>
                            <h6 class="contentTopic">Where does my order ship from?</h6>
                            <blockquote class="topicDetails">
                                Bixop is a global platform, connecting customers with merchants from across the world. Your order could ship from a variety of locations depending on the merchant.
                            </blockquote>
                            <h6 class="contentTopic">What do I do if my item was shipped to the wrong address?</h6>
                            <blockquote class="topicDetails">
                                You're only able to change the shipping address within the first 8 hours of completing your purchase. Once your order has shipped, it's no longer possible to change the address. We recommend contacting your local postal office with your tracking numbers to see if they can reroute your order to another address.
                            </blockquote>

                        </div>
                    </li>

                    <li id="returnAndRefund">
                        <div class="collapsible-header"><i class="material-icons">keyboard_return</i>
                            <span class="faqTitle">
                                Returns and Refunds
                            </span>
                        </div>
                        <div class="collapsible-body">
                            <h6 class="contentTopic">
                                How do I return a product?
                            </h6>
                            <blockquote class="topicDetails">
                                We want you to be fully satisfied with your purchase. All items may be returned within 30 days of delivery.
                                To return an item, please contact our Customer Service team through the app menu and tap Customer Support > My order > Reason for the return. You may also contact us on the web and select Contact Support at transaction. Please note that we don't cover return shipping costs at this time. For more information on our return policy, please visit return_policy.
                            </blockquote>
                            <h6 class="contentTopic">
                                Can I exchange an item?
                            </h6>
                            <blockquote class="topicDetails">
                                Unfortunately, we currently do not offer item exchanges. We may be able to help replace an order if it does not meet your satisfaction. Please contact our Customer Service team through the app menu and tap Customer Support > My order > Reason.
                            </blockquote>
                            <h6 class="contentTopic">
                                How do I cancel my order?
                            </h6>
                            <blockquote class="topicDetails">
                                If you need to cancel any items, please visit the Order History section. You have up to 8 hours from the time of purchase to cancel any items you no longer wish to receive. For any items that haven't been shipped or processed, an option that says "Cancel Order" will appear. After clicking this option, your order will be canceled and no charges will occur. If your order has already been processed, you'll be covered under our standard return policy: return_policy.
                            </blockquote>

                            <h6 class="contentTopic">
                                Where's my refund?
                            </h6>
                            <blockquote class="topicDetails">
                                You can check the status of your refund by visiting your Order History and checking the specific order: transaction. If you are still having trouble finding your processed refund in your account, we recommend contacting your bank or payment provider for more information. 
                            </blockquote>

                            <h6 class="contentTopic">
                                Can you issue my refund to a different card if my card is canceled, lost, expired, or stolen?
                            </h6>
                            <blockquote class="topicDetails">
                                We can only refund your money to your original payment method and we are unable to issue refunds to a different card, different account, or different payment method. For more information please consult our Return Policy: return_policy.
                                We recommend that you contact your card issuer for help with receiving a refund if your card was replaced.
                            </blockquote>

                            <h6 class="contentTopic">
                                I cancelled my order. How will I receive my refund?
                            </h6>
                            <blockquote class="topicDetails">
                                Depending on your payment type, your refund will be automatically applied back to your account. Please allow 5-10 business days for the refund to appear in your account. In some cases, especially with credit cards, your original charge may disappear entirely from your statement and you won't see a refund in your account.
                            </blockquote>

                        </div>
                    </li>   

                    <li id="payment">
                        <div class="collapsible-header"><i class="material-icons">payment</i>
                            <span class="faqTitle">
                                Payment, Pricing & Promotions
                            </span>
                        </div>
                        <div class="collapsible-body">
                            <h6 class="contentTopic">
                                What payment methods does Bixop accept?
                            </h6>
                            <blockquote class="topicDetails">
                                We currently accept payments through the following payment providers:
                                Visa, Mastercard, American Express, Discover, Cirrus, and Maestro
                                Google Wallet (Android Pay) play.google.com/store/apps/details?id= com.google.android.apps.walletnfcrel&hl=en
                                Apple Pay www.apple.com/apple-pay/
                                PayPal www.paypal.com/home
                                Klarna www.klarna.com
                                EBANX (Credit Card, Boleto, OXXO) www.ebanx.com
                                iDeal www.ideal.nl/en/
                                We cannot accept cash, bank transfer, or payment in installments. For certain countries, payment on delivery is available. Please note that the list of available payment methods may change depending on which country you are in, what platform you are using, and all payments are made in U.S. dollars.
                            </blockquote>


                            <h6 class="contentTopic">
                                What currency does Bixop sell in?
                            </h6>
                            <blockquote class="topicDetails">
                                For international users, we automatically display prices in your local currency. However, the order will be charged in U.S. dollars and will be shown during checkout.
                                If your credit card is used for a payment in foreign currency, most banks will allow you to pay, though they may charge a fee for this service. Please check with your bank to see if you can make international payments.
                            </blockquote>



                            <h6 class="contentTopic">
                                Does Bixop charge for customs, taxes, VAT, or any other extra fees?
                            </h6>
                            <blockquote class="topicDetails">
                                Bixop only charges for purchase price and shipping rate. We do not collect any payment for customs, taxes, VAT, currency conversion rates, or any other fees, and cannot reimburse any extra charges.
                                Customs: We recommend that you contact your local customs office if you have any doubts or questions about whether or not your item will be permitted into your country.
                                VAT: We do not charge for Value-Added Tax and cannot cover any charges which your country may add.
                                Currency Conversion: Although all items are displayed in your country's currency, each transaction is charged under US dollars.
                                Post Office: We cannot say for certain whether your local post will charge you for the products you order. If you have any questions, please contact your local post for more information.
                            </blockquote>


                            <h6 class="contentTopic">
                                How do I delete my credit card or payment information?
                            </h6>
                            <blockquote class="topicDetails">
                                Your billing information is completely secure on Bixop. If you decide you'd like to delete your billing information, please follow the steps below.
                                On the mobile app: 
                                Open the menu by tapping the top, left-hand side of the screen.
                                Tap Settings, then tap Payment Settings.
                                Tap the red "Delete" button to remove it.
                            </blockquote>


                            <h6 class="contentTopic">
                                How do I get a free gift?
                            </h6>
                            <blockquote class="topicDetails">
                                The free gift offer is currently only available for new customers in select countries. To receive the promotion, new customers must register on a device that's never received a free gift. Please also note that you must check out immediately to receive it.
                                Unfortunately, if you exit out of the app, click back or don’t complete it fast enough, the free gift offer may expire. Keep in mind that we're unable to cover shipping fees and you may need to provide a payment method to cover the order total.
                            </blockquote>


                            <h6 class="contentTopic">
                                What are rewards?
                            </h6>
                            <blockquote class="topicDetails">
                                Rewards and gift cards can be applied toward eligible items sold on Bixop. We will notify you when you receive a reward by sending an email and you’ll also get a notification on your notifications page, which you’ll see when you log in. Each reward is different and may vary in amount, and cannot be exchanged for cash or transferred outside of Bixop. You can always check on your rewards in your Rewards section: gift-cards.
                                Most rewards expire within a few days. You can see the rewards you have earned and their expiration dates in your Rewards section: gift-cards.
                            </blockquote>


                            <h6 class="contentTopic">
                                How can I use my rewards?
                            </h6>
                            <blockquote class="topicDetails">
                                Rewards will be automatically applied toward the purchase price of your products. Please note that Bixop rewards cannot cover shipping fees, as shipping is fulfilled by merchants across the world.
                            </blockquote>


                            <h6 class="contentTopic">
                                What is promo code? How does it work?
                            </h6>
                            <blockquote class="topicDetails">
                                Promo codes allow you to save even more on your order. To redeem a promo code, visit your cart in the mobile app when you are ready to check out, click on the promo code field and type in your code. When you click on 'Apply', your cart will be updated to reflect the added discounts. Please note that some promo codes may not be used together.
                            </blockquote>

                        </div>
                    </li>  

                    <li id="orders">
                        <div class="collapsible-header"><i class="material-icons">queue</i>
                            <span class="faqTitle">
                                Orders
                            </span>
                        </div>
                        <div class="collapsible-body">
                            <h6 class="contentTopic">
                                How do I get help with my order?
                            </h6>
                            <blockquote class="topicDetails">
                                For assistance, please contact our Customer Support team in the mobile app by navigating to the top, left-hand menu and tapping Customer Support. You may also contact us on the web at transaction. Then click Contact Support on the left side of the screen. If you'd like to listen to audio instructions for contacting us, please call 1-800-266-0172.
                            </blockquote>
                            <h6 class="contentTopic">
                                Why is my order history empty?
                            </h6>
                            <blockquote class="topicDetails">
                                If your order history is empty, it probably means your charges were declined when you tried to place your order. This means that we never actually accepted payment for your order. If you are seeing a charge on your bank statement, your bank should be able to process the amount back to your account within 5-7 days.
                                If you do not see your purchases in your order history, please make sure you are logged into the correct account. Every Bixop, Geek, Mama, and Home account is separate and may be under a different Facebook, Google+ or email address.
                            </blockquote>
                            <h6 class="contentTopic">
                                Why didn't my order go through?

                            </h6>
                            <blockquote class="topicDetails">
                                If your order did not go through or does not appear in your order history, it probably means your card was declined. Here are a few reasons why that may have happened.
                                The card is expired.
                                The account has insufficient funds.
                                The card was reported as stolen or frozen.
                                If you are using a debit card, be sure it has a CVV and can be billed as a credit card. Your order will also not go through if the product is unable to ship to your country.
                            </blockquote>
                            <h6 class="contentTopic">
                                Where can I find an invoice for my order?
                            </h6>
                            <blockquote class="topicDetails">
                                You can find your order invoice on your Order History page by clicking "Details" next to your order. This page as well as the confirmation email you received can be used as an invoice for your records. Please note, merchants are unable to attach an invoice to the outside of your package for customs purposes.
                            </blockquote>

                        </div>
                    </li>   

                    <li id="manageYourAccount">
                        <div class="collapsible-header"><i class="material-icons">account_balance</i>
                            <span class="faqTitle scrollspy" id="manageYourAccount">
                                Managing Your Account
                            </span>
                        </div>
                        <div class="collapsible-body">
                            <h6 class="contentTopic scrollspy" id="createAccount">
                                How can I create an account?
                            </h6>
                            <blockquote class="topicDetails">
                                You can create an account on Bixop by using your email address, Facebook or Google+ account to make logging into Bixop easier. We recommend using one of these methods for a more personalized shopping experience. Please visit www.wish.com or download our mobile apps to create an account and start shopping.
                            </blockquote>
                            <h6 class="contentTopic">
                                How do I unsubscribe from email communications?
                            </h6>
                            <blockquote class="topicDetails">
                                Please login to your Bixop account, select "Settings" under your profile photo at the top and check the appropriate boxes to change your email subscriptions.
                            </blockquote>
                            <h6 class="contentTopic">
                                How do I change the email address associated with my account?
                            </h6>
                            <blockquote class="topicDetails">
                                To change your email address, please follow these steps:
                                Visit settings.
                                Scroll down to find the section called Change Email.
                                Update your email address and click Change. This will send a confirmation email to your new address.
                                Click the confirmation link in the email you receive. Please allow up to 24 hours to receive your confirmation email and check your spam folder if you're not seeing it.
                            </blockquote>
                            <h6 class="contentTopic">
                                How do I reset my password?
                            </h6>
                            <blockquote class="topicDetails">
                                To change your password, please follow these steps:
                                Visit settings.
                                Scroll down to find the section called Change Password.
                                Enter your old and new password in the boxes, then tap Change Password.
                            </blockquote>
                            <h6 class="contentTopic">
                                What if I forgot my password?
                            </h6>
                            <blockquote class="topicDetails">
                                To reset your password, please follow these steps:
                                Click Login and the "Forgot Password" link. This will send a confirmation email to reset your password.
                            </blockquote>
                            <h6 class="contentTopic">
                                What should I do if I think my account was hacked?
                            </h6>
                            <blockquote class="topicDetails">
                                If you think your account was hacked, please contact our Customer Support team in the app by navigating to the top, left-hand menu and tapping Customer Support. You may also contact us on the web at transaction. Then click Contact Support and we will be happy to assist.
                            </blockquote>
                            <h6 class="contentTopic">
                                Why is my account restricted?
                            </h6>
                            <blockquote class="topicDetails">
                                There may be certain instances when we restrict your account as a safety precaution. This is usually for your own financial safety and protection. If you have any questions, you can contact our Customer Service team in the app by navigating to the left-hand menu and tapping Customer Support. You may also contact us on the web at transaction. Then click Contact Support.
                            </blockquote>
                            <h6 class="contentTopic">
                                How do I delete my Bixop account?
                            </h6>
                            <blockquote class="topicDetails">
                                If your account is registered through Facebook, you can disable Bixop's Facebook privileges.
                                Go to www.facebook.com/settings/?tab=applications.
                                Hit the "x" on the far right of the row with "Bixop" on the left.
                                If you login with email or Google+:
                                You can delete your account from your Settings page. On the mobile app, tap on Settings > Delete Account. On the website, scroll to the bottom of the Settings page and click on Delete Account.
                            </blockquote>

                        </div>
                    </li>   

                </ul>


            </div>
            <div class="col s10 m4 offset-s1">        
                <div class="collection with-header">
                    <li class="collection-header"><h5>Help Topics</h5></li>
                    <a href="#shipAndDelivery" class="collection-item">Shipping and Delivery</a>
                    <a href="#returnAndRefund" class="collection-item">Returns and Refunds</a>
                    <a href="#payment" class="collection-item ">Payment, Pricing & Promotions</a>
                    <a href="#orders" class="collection-item">Orders</a>
                    <a href="#manageYourAccount" class="collection-item">Managing Your Account</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('inc/footer.php'); ?>
