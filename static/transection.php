<?php include_once('inc/header.php'); ?>

<section class="page" id="transection">
    <div class="container">
        <div class="row">
            <div class="pageContentSection">
                <div class="col s10 offset-s1 m8 offset-m2">
                    <h4 class="onPageTitle center-align">Order History</h4>
                    <p class="center-align"><i class="fa fa-meh-o"></i> We have your back! You never have to worry when you shop on Wish.</p>

                    <div class="row">
                        <p class="center-align"><i class="fa fa-info-circle"></i> You haven't made any orders.</p>
                        <div class="col s3 offset-s1 m2 offset-m2"></div>
                        <a href="/" class="grey center-align waves-effect waves-light btn-large"><i class="fa fa-shopping-bag"></i> Shop Now</a>
                    </div>

                    <div class="row">




                    </div>

                </div> <!-- /.col -->

            </div> <!-- /.pageContentSection -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</section> <!-- /.page -->
<?php include_once('inc/footer.php'); ?>
