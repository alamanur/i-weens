
<?php include_once('inc/header.php'); ?>

<section class="page" id="">

    <div class="row">
        <div class="col s11 m10 l10 xl12 offset-l1 offset-m1 offset-s1">
            <article class="singleProduct">
                <div class="row">


                    <div class="col s12 m7 l7 ">



                        <ul class="pgwSlideshow">
                            <li><img src="images/products/11.jpg" alt="San Francisco, USA" data-description="Golden Gate Bridge"></li>
                            <li><img src="images/products/11.jpg" alt="San Francisco, USA" data-description="Golden Gate Bridge"></li>
                            <li><img src="images/products/12.jpg" alt="San Francisco, USA" data-description="Golden Gate Bridge"></li>
                            <li><img src="images/products/13.jpg" alt="San Francisco, USA" data-description="Golden Gate Bridge"></li>
                            <li><img src="images/products/14.jpg" alt="San Francisco, USA" data-description="Golden Gate Bridge"></li>
                            <li><img src="images/products/15.jpg" alt="San Francisco, USA" data-description="Golden Gate Bridge"></li>
                        </ul>


                    </div>

                    <div class="col s12 m4 l4 offset-l1">

                        <h3 class="singleTitle">Folding Solar Charger Solar Power Bank 300000mAh Rechargeable External Solar Battery Support Solar Charging for Smart Phones</h3>
                        <div class="row">
                            <i class="material-icons light-blue-text tiny">star</i>
                            <i class="material-icons light-blue-text tiny">star</i>
                            <i class="material-icons light-blue-text tiny">star</i>
                            <i class="material-icons light-blue-text tiny">star</i>
                            <i class="material-icons light-blue-text tiny">star</i>
                            <span class="totalRatings" style="text-decoration: underline; ">(1010)</span>
                        </div>

                        <h4 class="singlePrice"><small class="singlePriceOld"><i class="fa fa-dollar"></i>300</small>  <i class="fa fa-dollar"></i>150  USD</h4>

                        <div class="row">
                            <select>
                                <option value="" disabled selected>Choose a Size</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                        </div>
                        <div class="row">
                            <select>
                                <option value="" disabled selected>Choose a Color</option>
                                <option value="1">Reg</option>
                                <option value="1">Blue</option>
                                <option value="1">Orange</option>
                                <option value="1">Green</option>
                                <option value="1">Black</option>
                            </select>
                        </div>
                        <div class="row">
                            <a class="waves-effect waves-light btn btn-block btn-buy orange lighten-1 white-text"><i class="fa fa-cart-arrow-down"></i> Buy</a>
                        </div>
                        <div class="row">
                            <a class="waves-effect waves-light btn btn-block btn-save grey lighten-3 black-text"><i class="fa fa-heart-o"></i> Save</a>
                        </div>

                    </div>  <!-- /.col -->

                </div>   <!-- /.row -->

                <div class="row">
                    <div class="col s12">

                        <ul id="tabs-swipe" class="tabs">
                            <li class="tab col s2 "><a class="active"  href="#description">Description</a></li>
                            <li class="tab col s2 hide-on-small-and-down"><a href="#overview">Overview</a></li>
                            <li class="tab col s2"><a  href="#related">Related Products</a></li>
                            <li class="tab col s2 hide-on-small-and-down"><a href="#ratings">Ratings</a></li>
                            <li class="tab col s2 hide-on-med-and-down"><a href="#storeRating">Store Ratings</a></li>
                            <li class="tab col s2"><a href="#shipping">Shipping</a></li>
                        </ul>

                        <!--Description of the product tab-->

                        <div id="description" class="col s12  ">
                            <strong>Description</strong>
                            <p>Style: Tank Top</p>
                            <p>Type: GYM Sport</p>
                            <p>Material: 80% Cotton 20%Spandex</p>
                            <p>Sleeve Length: Sleeveless</p>
                            <p>Hem: Irregular Hem</p>

                            <p> Asian Size: 1~2size smaller than Europe/US size</p>

                            <p> Size CM/Inch</p>
                            <p>S: FrontLength 80/31.5" BehindLength 68/26.9" Chest 92/36.2"</p>
                            <p>M: FrontLength 82/32.3" BehindLength 70/27.6" Chest 94/37"</p>
                            <p>L : FrontLength 86/33.9" BehindLength 74/29.2" Chest 98/38.6"</p>
                            <p>XL: FrontLength 88/34.6" BehindLength 76/29.9" Chest 104/40.9"</p>
                            <p>XXL: FrontLength 90/35.4" BehindLength 78/30.7" Chest 108/42.5"</p>
                        </div>

                        <!--Overview of the Product tab -->

                        <div id="overview" class="col s12 hide-on-small-and-down">
                            <div class="row">
                                <div class="productFeatures">

                                    <div class="col s12 m8 l8">
                                        <div class="row">
                                            <div class="col s12">
                                                <h6><strong>Recent Reviews</strong><a href="" class="pull-right">View All</a></h6>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s12 m4">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                            </div>
                                            <div class="col s12 m8">
                                                <strong>Ishtiak Rahman APK</strong>
                                                <p>Item fit just right...nice</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s12 m4">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons tiny">star</i>
                                            </div>
                                            <div class="col s12 m8">
                                                <strong>Nur a alam LB</strong>
                                                <p>Feels just perfect. Will order more again in diff color.</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s12 m4">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons  tiny">star</i>
                                                <i class="material-icons  tiny">star</i>
                                            </div>
                                            <div class="col s12 m8">
                                                <strong>Asaduzzaman Asad</strong>
                                                <p>top quality ..speedy service... and looks ausum on..</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col s12">
                                                <h6><strong>Recent Reviews</strong><a href="" class="pull-right">View All</a></h6>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s12 m4">
                                                Too Small
                                            </div>
                                            <div class="col s12 m8">
                                                <div class="progress">
                                                    <div class="determinate light-blue lighten-3" style="width: 40%"></div>
                                                </div>
                                            </div>
                                            <div class="col s12 m4">
                                                Just Right
                                            </div>
                                            <div class="col s12 m8">
                                                <div class="progress">
                                                    <div class="determinate light-blue lighten-3" style="width: 60%"></div>
                                                </div>
                                            </div>
                                            <div class="col s12 m4">
                                                Too Large
                                            </div>
                                            <div class="col s12 m8">
                                                <div class="progress">
                                                    <div class="determinate light-blue lighten-3" style="width: 10%"></div>
                                                </div>
                                            </div>


                                        </div>



                                    </div> <!-- /.col -->
                                    <div class="col s12 m4 l3 offset-l1">
                                        <div class="row">
                                            <div class="col s12">
                                                <h6><strong>Estimated Arrival</strong></h6>
                                                <p>Aug 31 - Sep 10</p>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s12">
                                                <h6><strong>Estimated Shipping</strong></h6>
                                                <p><i class="fa fa-doller"></i>$1 USD</p>

                                            </div>
                                        </div>

                                    </div> <!-- /.col -->

                                </div> <!-- /.recentReviews -->

                            </div> <!-- /.row -->

                        </div>

                        <!--Related Products Tab-->

                        <div id="related" class="col s12  ">

                            <div class="col s11 m10 l10 xl12 offset-l1 offset-m1 offset-s1">

                                <div class="row">

                                    <div class="col s6 m4 l3">
                                        <div class="card">
                                            <a href="single.php">
                                                <div class="card-image  " style="background: url(images/products/3.jpg) left top no-repeat; background-size: contain;   ">
                                                    <span class="discout">-84%</span>
                                                    <span class="almostGone">Almost Gone!</span>
                                                </div>
                                            </a>
                                            <div class="card-content">
                                                <p class="price">
                                                    <i class="fa fa-dollar"></i>20
                                                    <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                                    <small class="sold">10k+ bought this</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col s6 m4 l3">
                                        <div class="card">
                                            <div class="card-image  " style="background: url(images/products/12.jpg) left top no-repeat; background-size: contain;   ">
                                                <span class="discout">-84%</span>
                                            </div>
                                            <div class="card-content">
                                                <p class="price">
                                                    <i class="fa fa-dollar"></i>20
                                                    <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                                    <small class="sold">10k+ bought this</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col s6 m4 l3">
                                        <div class="card">
                                            <div class="card-image  " style="background: url(images/products/7.jpg) left top no-repeat; background-size: contain;   ">
                                                <span class="discout">-84%</span>
                                            </div>
                                            <div class="card-content">
                                                <p class="price">
                                                    <i class="fa fa-dollar"></i>20
                                                    <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                                    <small class="sold">10k+ bought this</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col s6 m4 l3">
                                        <div class="card">
                                            <div class="card-image  " style="background: url(images/products/9.jpg) left top no-repeat; background-size: contain;   ">
                                                <span class="discout">-84%</span>
                                            </div>
                                            <div class="card-content">
                                                <p class="price">
                                                    <i class="fa fa-dollar"></i>20
                                                    <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                                    <small class="sold">10k+ bought this</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col s6 m4 l3">
                                        <div class="card">
                                            <div class="card-image  " style="background: url(images/products/10.jpg) left top no-repeat; background-size: contain;   ">
                                                <span class="discout">-84%</span>
                                            </div>
                                            <div class="card-content">
                                                <p class="price">
                                                    <i class="fa fa-dollar"></i>20
                                                    <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                                    <small class="sold">10k+ bought this</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col s6 m4 l3">
                                        <div class="card">
                                            <div class="card-image  " style="background: url(images/products/11.jpg) left top no-repeat; background-size: contain;   ">
                                                <span class="discout">-84%</span>
                                                <span class="almostGone">Almost Gone!</span>
                                            </div>
                                            <div class="card-content">
                                                <p class="price">
                                                    <i class="fa fa-dollar"></i>20
                                                    <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                                    <small class="sold">10k+ bought this</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col s6 m4 l3">
                                        <div class="card">
                                            <div class="card-image  " style="background: url(images/products/5.jpg) left top no-repeat; background-size: contain;   ">
                                                <span class="discout">-84%</span>
                                            </div>
                                            <div class="card-content">
                                                <p class="price">
                                                    <i class="fa fa-dollar"></i>20
                                                    <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                                    <small class="sold">10k+ bought this</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col s6 m4 l3">
                                        <div class="card">
                                            <div class="card-image  " style="background: url(images/products/13.jpg) left top no-repeat; background-size: contain;   ">
                                                <span class="discout">-84%</span>
                                                <span class="almostGone">Almost Gone!</span>
                                            </div>
                                            <div class="card-content">
                                                <p class="price">
                                                    <i class="fa fa-dollar"></i>20
                                                    <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                                    <small class="sold">10k+ bought this</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col s6 m4 l3">
                                        <div class="card">
                                            <div class="card-image  " style="background: url(images/products/1.jpg) left top no-repeat; background-size: contain;   ">
                                                <span class="discout">-84%</span>
                                                <span class="almostGone">Almost Gone!</span>
                                            </div>
                                            <div class="card-content">
                                                <p class="price">
                                                    <i class="fa fa-dollar"></i>20
                                                    <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                                    <small class="sold">10k+ bought this</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col s6 m4 l3">
                                        <div class="card">
                                            <div class="card-image  " style="background: url(images/products/2.jpg) left top no-repeat; background-size: contain;   ">
                                                <span class="discout">-84%</span>
                                            </div>
                                            <div class="card-content">
                                                <p class="price">
                                                    <i class="fa fa-dollar"></i>20
                                                    <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                                    <small class="sold">10k+ bought this</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col s6 m4 l3">
                                        <div class="card">
                                            <div class="card-image  " style="background: url(images/products/4.jpg) left top no-repeat; background-size: contain;   ">
                                                <span class="discout">-84%</span>
                                            </div>
                                            <div class="card-content">
                                                <p class="price">
                                                    <i class="fa fa-dollar"></i>20
                                                    <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                                    <small class="sold">10k+ bought this</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col s6 m4 l3">
                                        <div class="card">
                                            <div class="card-image  " style="background: url(images/products/5.jpg) left top no-repeat; background-size: contain;   ">
                                                <span class="discout">-84%</span>
                                            </div>
                                            <div class="card-content">
                                                <p class="price">
                                                    <i class="fa fa-dollar"></i>20
                                                    <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                                    <small class="sold">10k+ bought this</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col s6 m4 l3">
                                        <div class="card">
                                            <div class="card-image  " style="background: url(images/products/14.jpg) left top no-repeat; background-size: contain;   ">
                                                <span class="discout">-84%</span>
                                            </div>
                                            <div class="card-content">
                                                <p class="price">
                                                    <i class="fa fa-dollar"></i>20
                                                    <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                                    <small class="sold">10k+ bought this</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col s6 m4 l3">
                                        <div class="card">
                                            <div class="card-image  " style="background: url(images/products/15.jpg) left top no-repeat; background-size: contain;   ">
                                                <span class="discout">-84%</span>
                                                <span class="almostGone">Almost Gone!</span>
                                            </div>
                                            <div class="card-content">
                                                <p class="price">
                                                    <i class="fa fa-dollar"></i>20
                                                    <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                                    <small class="sold">10k+ bought this</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col s6 m4 l3">
                                        <div class="card">
                                            <div class="card-image  " style="background: url(images/products/13.jpg) left top no-repeat; background-size: contain;   ">
                                                <span class="discout">-84%</span>
                                            </div>
                                            <div class="card-content">
                                                <p class="price">
                                                    <i class="fa fa-dollar"></i>20
                                                    <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                                    <small class="sold">10k+ bought this</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col s6 m4 l3">
                                        <div class="card">
                                            <div class="card-image  " style="background: url(images/products/10.jpg) left top no-repeat; background-size: contain;   ">
                                                <span class="discout">-84%</span>
                                                <span class="almostGone">Almost Gone!</span>
                                            </div>
                                            <div class="card-content">
                                                <p class="price">
                                                    <i class="fa fa-dollar"></i>20
                                                    <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                                    <small class="sold">10k+ bought this</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s10 offset-s1 m4 offset-m5 l3 offset-l5" style="padding-bottom: 30px; overflow: hidden;">
                                            <strong class="loadingMore">Loading More...</strong> <img width="25" height="25" src="images/loader.gif" alt="Loading more products..." />
                                        </div>
                                    </div>


                                </div>


                            </div>
                        </div>

                        <!--Ratings Tab-->

                        <div id="ratings" class="col s12 hide-on-small-and-down ">
                            <div class="row">
                                <div class="col s12 m10 112 offset-l0">
                                    <i class="material-icons light-blue-text small">star</i>
                                    <i class="material-icons light-blue-text small">star</i>
                                    <i class="material-icons light-blue-text small">star</i>
                                    <i class="material-icons light-blue-text small">star</i>
                                    <i class="material-icons grey-text small">star</i>
                                    ( <span class="totalRatings">1804</span> )
                                </div>
                            </div> <!-- /.row -->

                            <div class="row">
                                <div class="col s12 m6 l4">
                                    <div class="row">
                                        <div class="col s7">
                                            <div class="ratingStars fiveStar pull-right">
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>

                                            </div>  <!-- /#ratingStars -->
                                        </div>  <!-- /.col -->

                                        <div class="col s5">
                                            <div class="progress">
                                                <div class="determinate light-blue lighten-3" style="width: 70%"></div>
                                            </div>

                                        </div>  <!-- /.col -->
                                        <div class="col s7">
                                            <div class="ratingStars fiveStar pull-right">
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>

                                            </div>  <!-- /#ratingStars -->
                                        </div>  <!-- /.col -->

                                        <div class="col s5">
                                            <div class="progress">
                                                <div class="determinate light-blue lighten-3" style="width: 50%"></div>
                                            </div>

                                        </div>  <!-- /.col -->
                                        <div class="col s7">
                                            <div class="ratingStars fiveStar pull-right">
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>

                                            </div>  <!-- /#ratingStars -->
                                        </div>  <!-- /.col -->

                                        <div class="col s5">
                                            <div class="progress">
                                                <div class="determinate light-blue lighten-3" style="width: 7%"></div>
                                            </div>

                                        </div>  <!-- /.col -->
                                        <div class="col s7">
                                            <div class="ratingStars fiveStar pull-right">
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>

                                            </div>  <!-- /#ratingStars -->
                                        </div>  <!-- /.col -->

                                        <div class="col s5">
                                            <div class="progress">
                                                <div class="determinate light-blue lighten-3" style="width: 3%"></div>
                                            </div>

                                        </div>  <!-- /.col -->
                                        <div class="col s7">
                                            <div class="ratingStars fiveStar pull-right">
                                                <i class="material-icons grey-text tiny">star</i>

                                            </div>  <!-- /#ratingStars -->
                                        </div>  <!-- /.col -->

                                        <div class="col s5">
                                            <div class="progress">
                                                <div class="determinate light-blue lighten-3" style="width: 10%"></div>
                                            </div>

                                        </div>  <!-- /.col -->

                                    </div>  <!-- /.row -->

                                </div>  <!-- /.col -->
                            </div> <!-- /.row -->

                            <div class="row">
                                <div class="col s12">
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/asad.jpg)">
                                            </div>

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Asaduzzaman Asad</strong>
                                            </h6>
                                            <p class="ratingText">
                                                It's good- seems to work pretty well. Hard to read the digits on it but works.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/hasan.jpg)">

                                            </div>
                                            <!--<img src="images/users/asad.jpg" alt="" class="responsive-img" />-->

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Mr Hasan Mollah</strong>
                                            </h6>
                                            <p class="ratingText">
                                                Awesome! Works GREAT.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/badruddoza_sir.jpg)">
                                            </div>

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Asaduzzaman Asad</strong>
                                            </h6>
                                            <p class="ratingText">
                                                It's good- seems to work pretty well. Hard to read the digits on it but works.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/ishtiaque.jpg)">

                                            </div>
                                            <!--<img src="images/users/asad.jpg" alt="" class="responsive-img" />-->

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Mr Hasan Mollah</strong>
                                            </h6>
                                            <p class="ratingText">
                                                Awesome! Works GREAT.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/manik.jpg)">
                                            </div>

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Asaduzzaman Asad</strong>
                                            </h6>
                                            <p class="ratingText">
                                                It's good- seems to work pretty well. Hard to read the digits on it but works.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/nazrul.jpg)">

                                            </div>
                                            <!--<img src="images/users/asad.jpg" alt="" class="responsive-img" />-->

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Mr Hasan Mollah</strong>
                                            </h6>
                                            <p class="ratingText">
                                                Awesome! Works GREAT.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/sohan.jpg)">
                                            </div>

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Asaduzzaman Asad</strong>
                                            </h6>
                                            <p class="ratingText">
                                                It's good- seems to work pretty well. Hard to read the digits on it but works.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/hasan.jpg)">

                                            </div>
                                            <!--<img src="images/users/asad.jpg" alt="" class="responsive-img" />-->

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Mr Hasan Mollah</strong>
                                            </h6>
                                            <p class="ratingText">
                                                Awesome! Works GREAT.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/sakil.jpg)">
                                            </div>

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Asaduzzaman Asad</strong>
                                            </h6>
                                            <p class="ratingText">
                                                It's good- seems to work pretty well. Hard to read the digits on it but works.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/default.jpg)">

                                            </div>
                                            <!--<img src="images/users/asad.jpg" alt="" class="responsive-img" />-->

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Mr Hasan Mollah</strong>
                                            </h6>
                                            <p class="ratingText">
                                                Awesome! Works GREAT.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/asad.jpg)">
                                            </div>

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Asaduzzaman Asad</strong>
                                            </h6>
                                            <p class="ratingText">
                                                It's good- seems to work pretty well. Hard to read the digits on it but works.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/hasan.jpg)">

                                            </div>
                                            <!--<img src="images/users/asad.jpg" alt="" class="responsive-img" />-->

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Mr Hasan Mollah</strong>
                                            </h6>
                                            <p class="ratingText">
                                                Awesome! Works GREAT.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->

                                </div><!-- /.col -->
                            </div><!-- /.row -->

                        </div> <!-- /#ratings -->

                        <!--Store Ratings Tab-->

                        <div id="storeRating" class="col s12 hide-on-med-and-down ">
                            <div class="row">
                                <div class="col s12 m10 112 offset-l0">
                                    <i class="material-icons light-blue-text small">star</i>
                                    <i class="material-icons light-blue-text small">star</i>
                                    <i class="material-icons light-blue-text small">star</i>
                                    <i class="material-icons light-blue-text small">star</i>
                                    <i class="material-icons grey-text small">star</i>
                                    ( <span class="totalRatings">1804</span> )
                                </div>
                            </div> <!-- /.row -->

                            <div class="row">
                                <div class="col s12 m6 l4">
                                    <div class="row">
                                        <div class="col s7">
                                            <div class="ratingStars fiveStar pull-right">
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>

                                            </div>  <!-- /#ratingStars -->
                                        </div>  <!-- /.col -->

                                        <div class="col s5">
                                            <div class="progress">
                                                <div class="determinate light-blue lighten-3" style="width: 70%"></div>
                                            </div>

                                        </div>  <!-- /.col -->
                                        <div class="col s7">
                                            <div class="ratingStars fiveStar pull-right">
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>

                                            </div>  <!-- /#ratingStars -->
                                        </div>  <!-- /.col -->

                                        <div class="col s5">
                                            <div class="progress">
                                                <div class="determinate light-blue lighten-3" style="width: 50%"></div>
                                            </div>

                                        </div>  <!-- /.col -->
                                        <div class="col s7">
                                            <div class="ratingStars fiveStar pull-right">
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>

                                            </div>  <!-- /#ratingStars -->
                                        </div>  <!-- /.col -->

                                        <div class="col s5">
                                            <div class="progress">
                                                <div class="determinate light-blue lighten-3" style="width: 7%"></div>
                                            </div>

                                        </div>  <!-- /.col -->
                                        <div class="col s7">
                                            <div class="ratingStars fiveStar pull-right">
                                                <i class="material-icons grey-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>

                                            </div>  <!-- /#ratingStars -->
                                        </div>  <!-- /.col -->

                                        <div class="col s5">
                                            <div class="progress">
                                                <div class="determinate light-blue lighten-3" style="width: 3%"></div>
                                            </div>

                                        </div>  <!-- /.col -->
                                        <div class="col s7">
                                            <div class="ratingStars fiveStar pull-right">
                                                <i class="material-icons grey-text tiny">star</i>

                                            </div>  <!-- /#ratingStars -->
                                        </div>  <!-- /.col -->

                                        <div class="col s5">
                                            <div class="progress">
                                                <div class="determinate light-blue lighten-3" style="width: 10%"></div>
                                            </div>

                                        </div>  <!-- /.col -->

                                    </div>  <!-- /.row -->

                                </div>  <!-- /.col -->
                            </div> <!-- /.row -->

                            <div class="row">
                                <div class="col s12">
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/asad.jpg)">
                                            </div>

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Asaduzzaman Asad</strong>
                                            </h6>
                                            <p class="ratingText">
                                                It's good- seems to work pretty well. Hard to read the digits on it but works.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/hasan.jpg)">

                                            </div>
                                            <!--<img src="images/users/asad.jpg" alt="" class="responsive-img" />-->

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Mr Hasan Mollah</strong>
                                            </h6>
                                            <p class="ratingText">
                                                Awesome! Works GREAT.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/badruddoza_sir.jpg)">
                                            </div>

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Asaduzzaman Asad</strong>
                                            </h6>
                                            <p class="ratingText">
                                                It's good- seems to work pretty well. Hard to read the digits on it but works.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/ishtiaque.jpg)">

                                            </div>
                                            <!--<img src="images/users/asad.jpg" alt="" class="responsive-img" />-->

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Mr Hasan Mollah</strong>
                                            </h6>
                                            <p class="ratingText">
                                                Awesome! Works GREAT.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/manik.jpg)">
                                            </div>

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Asaduzzaman Asad</strong>
                                            </h6>
                                            <p class="ratingText">
                                                It's good- seems to work pretty well. Hard to read the digits on it but works.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/nazrul.jpg)">

                                            </div>
                                            <!--<img src="images/users/asad.jpg" alt="" class="responsive-img" />-->

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Mr Hasan Mollah</strong>
                                            </h6>
                                            <p class="ratingText">
                                                Awesome! Works GREAT.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/sohan.jpg)">
                                            </div>

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Asaduzzaman Asad</strong>
                                            </h6>
                                            <p class="ratingText">
                                                It's good- seems to work pretty well. Hard to read the digits on it but works.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/hasan.jpg)">

                                            </div>
                                            <!--<img src="images/users/asad.jpg" alt="" class="responsive-img" />-->

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Mr Hasan Mollah</strong>
                                            </h6>
                                            <p class="ratingText">
                                                Awesome! Works GREAT.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/sakil.jpg)">
                                            </div>

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Asaduzzaman Asad</strong>
                                            </h6>
                                            <p class="ratingText">
                                                It's good- seems to work pretty well. Hard to read the digits on it but works.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/default.jpg)">

                                            </div>
                                            <!--<img src="images/users/asad.jpg" alt="" class="responsive-img" />-->

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Mr Hasan Mollah</strong>
                                            </h6>
                                            <p class="ratingText">
                                                Awesome! Works GREAT.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/asad.jpg)">
                                            </div>

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Asaduzzaman Asad</strong>
                                            </h6>
                                            <p class="ratingText">
                                                It's good- seems to work pretty well. Hard to read the digits on it but works.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->
                                    <div class="row ratingSection">
                                        <div class="col  m2 l1">
                                            <div class="ratingByImg" style="background-image: url(images/users/hasan.jpg)">

                                            </div>
                                            <!--<img src="images/users/asad.jpg" alt="" class="responsive-img" />-->

                                        </div> <!-- /.col -->
                                        <div class="col m10 l10">

                                            <h6 class="ratingBy">
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons grey-text tiny">star</i>
                                                <strong>Mr Hasan Mollah</strong>
                                            </h6>
                                            <p class="ratingText">
                                                Awesome! Works GREAT.
                                            </p>
                                            <small class="ratingTime">about a month ago</small>

                                        </div> <!-- /.col -->
                                    </div> <!-- /.row -->

                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div id="shipping" class="col s12 ">
                            <h6 class="shippingTabTitle"><strong>Shipping Details</strong></h6>
                            <table class="bordered responsive-table">

                                <tbody>
                                    <tr>
                                        <td>Availability</td>
                                        <td>$3 USD</td>
                                    </tr>
                                    <tr>
                                        <td>Estimated Arrival</td>
                                        <td>Ships to United States and 77 other countries</td>
                                    </tr>
                                    <tr>
                                        <td>Estimated Shipping</td>
                                        <td>Aug 31 - Sep 10</td>
                                    </tr>
                                    <tr>
                                        <td>Return Policy</td>
                                        <td>
                                            You may return all products within 30 days of delivery.
                                            <p class="moreDetailsModal"><a href="">More Details</a></p>

                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>   <!-- /.col -->


                </div>  <!-- /.row -->




            </article>  <!-- /.singleProduct -->
        </div>  <!-- /.col -->
    </div>   <!-- /.row -->
</section>>

<?php include_once('inc/footer.php'); ?>
