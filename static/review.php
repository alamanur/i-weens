<?php include_once('inc/header.php'); ?>
<div class="cartPage">
    <div class="container">

        <div class="row">
            <div class="col s10 offset-s1 l12">
                <div class="row">
                    <aside id="shipTo">
                        <div class="col m12 l5">
                            <div class="row">
                                <div class="col s12 ">
                                    <nav class="">
                                        <div class="nav-wrapper">
                                            <?php include_once 'inc/ship-menu.php'; ?>
                                        </div>
                                    </nav>

                                    <h2 class="orderSummary cartPageHeading">ORDER SUMMARY </h2>
                                    <!--SHIP TO form -->


                                    <aside id="orderSummary">
                                        <div class="col m12 l12">
                                            <div class="row">
                                                <div class="col s12  grey lighten-4">
                                                    <h2 class="orderSummary cartPageHeading"></h2>
                                                    <h6 class="orderSummary itemTotal">Item Total: <span class="pull-right">$49</span></h6>
                                                    <h6 class="orderSummary estimatedShipping">Estimated Shipping: <span class="pull-right">$49</span></h6>
                                                    <h5 class="orderSummary orderTotal">ORDER TOTAL: <span class="pull-right">$49</span></h5>
                                                </div>
                                                <div class="col s12"></div>
                                            </div>
                                            <div class="row">
                                                <a href="checkout.php" class="waves-effect waves-light btn btn-block">Confirm Order</a>
                                            </div>

                                        </div> <!--   /.col  -->
                                    </aside>


                                </div>

                            </div>


                        </div>  <!-- /.col -->
                    </aside>

                    <aside id="shoppingCart">
                        <div class="col m12 l7 offset-l0" style="background: none;">
                            <h2 class="shipPageHeading white lighten-3">SHOPPING CART</h2>
                            <article>

                                <div class="row">
                                    <div class="col s12">
                                        <div class="col s12 m5 l3">

                                            <div class="shipItemThumb" style="background-image: url(images/products/13.jpg); "></div>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons tiny">star</i>

                                        </div>
                                        <div class="col s12 m7 l9 cartItemInfo ">
                                            <h5 class="cartItemTitle" >
                                                Casual Genuine Leather Trainers Comfortable Men Sport Shoes Breathable Sneakers
                                            </h5>
                                            <p class="cartItemSize">Size: 8 <span class="cartItemColor">Color: red</span></p>                                

                                            <p class="shipQuantity">Quantity: 8 </p>      
                                            <p class="cartItemShipping">Shipping : $8.00</p>
                                            <p class="cartItemArrival">Estimated Arrival: Sep 2 - Sep 5</p>
                                            <p class="cartItemPrice"><span class="cartItemPriceOld">$93.00</span> $26.00</p>

                                        </div>
                                    </div>
                                </div>  <!-- /.row -->

                            </article>
                            <hr class="hide-on-med-and-up" />

                            <article>

                                <div class="row">
                                    <div class="col s12">
                                        <div class="col s12 m5 l3">

                                            <div class="shipItemThumb" style="background-image: url(images/products/11.jpg); "></div>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons tiny">star</i>


                                        </div>
                                        <div class="col s12 m7 l9 cartItemInfo ">
                                            <h5 class="cartItemTitle" >
                                                Casual Genuine Leather Trainers Comfortable Men Sport Shoes Breathable Sneakers
                                            </h5>
                                            <p class="cartItemSize">Size: 8 <span class="cartItemColor">Color: red</span></p>                                
                                            <p class="shipQuantity">Quantity: 8 </p>      
                                            <p class="cartItemShipping">Shipping : $8.00</p>
                                            <p class="cartItemArrival">Estimated Arrival: Sep 2 - Sep 5</p>
                                            <p class="cartItemPrice"><span class="cartItemPriceOld">$93.00</span> $26.00</p>

                                        </div>
                                    </div>
                                </div>  <!-- /.row -->

                            </article>
                            <hr class="hide-on-med-and-up" />

                            <article>

                                <div class="row">
                                    <div class="col s12">
                                        <div class="col s12 m5 l3">

                                            <div class="shipItemThumb" style="background-image: url(images/products/12.jpg); "></div>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons tiny">star</i>


                                        </div>
                                        <div class="col s12 m7 l9 cartItemInfo ">
                                            <h5 class="cartItemTitle" >
                                                Casual Genuine Leather Trainers Comfortable Men Sport Shoes Breathable Sneakers
                                            </h5>
                                            <p class="cartItemSize">Size: 8 <span class="cartItemColor">Color: red</span></p>                                
                                            <p class="shipQuantity">Quantity: 8 </p>                                
                                            <p class="cartItemShipping">Shipping : $8.00</p>
                                            <p class="cartItemArrival">Estimated Arrival: Sep 2 - Sep 5</p>
                                            <p class="cartItemPrice"><span class="cartItemPriceOld">$93.00</span> $26.00</p>

                                        </div>
                                    </div>
                                </div>  <!-- /.row -->

                            </article>
                            <hr class="hide-on-med-and-up" />

                        </div>  <!-- /.col -->
                    </aside>

                </div> <!-- /.row -->

            </div> <!-- /.col -->
        </div> <!-- /.row -->

    </div>
</div>

<!--<div class="cartPage">
    <div class="container">
       
            <div class="row">
                <div class="col s10 offset-s1 l12">
                    <div class="row">
                        <aside id="shoppingCart">
                            <div class="col m12 l9">
                                <nav class="">
                                    <div class="nav-wrapper">
<?php //include_once 'inc/ship-menu.php'; ?>
                                    </div>
                                </nav>
                                <h2 class="cartPageHeading">SHOPPING CART</h2>
                                <article>

                                    <div class="row">
                                        <div class="col s12">
                                            <div class="col s12 m5 l4">

                                                <div class="cartItemThumb" style="background-image: url(images/products/13.jpg); "></div>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons tiny">star</i>
                                                ( <span class="totalRatings">1010</span> )

                                            </div>
                                            <div class="col s12 m7 l8 cartItemInfo ">
                                                <h5 class="cartItemTitle" >
                                                    Casual Genuine Leather Trainers Comfortable Men Sport Shoes Breathable Sneakers
                                                </h5>
                                                <p class="cartItemSize">Size: 8 <span class="cartItemColor">Color: red</span></p>                                
                                                <p class="cartItemQuantity">Quantity: 8 </p>                                

                                                <p class="cartItemShipping">Shipping : $8.00</p>
                                                <p class="cartItemArrival">Estimated Arrival: Sep 2 - Sep 5</p>
                                                <p class="cartItemPrice"><span class="cartItemPriceOld">$93.00</span> $26.00</p>

                                            </div>
                                        </div>
                                    </div>   /.row 

                                </article>
                                <hr class="hide-on-med-and-up" />

                                <article>

                                    <div class="row">
                                        <div class="col s12">
                                            <div class="col s12 m5 l4">

                                                <div class="cartItemThumb" style="background-image: url(images/products/11.jpg); "></div>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons tiny">star</i>
                                                ( <span class="totalRatings">1010</span> )

                                            </div>
                                            <div class="col s12 m7 l8 cartItemInfo ">
                                                <h5 class="cartItemTitle" >
                                                    Casual Genuine Leather Trainers Comfortable Men Sport Shoes Breathable Sneakers
                                                </h5>
                                                <p class="cartItemSize">Size: 8 <span class="cartItemColor">Color: red</span></p>                                
                                                <p class="cartItemQuantity">Quantity: 8 </p>                                

                                                <p class="cartItemShipping">Shipping : $8.00</p>
                                                <p class="cartItemArrival">Estimated Arrival: Sep 2 - Sep 5</p>
                                                <p class="cartItemPrice"><span class="cartItemPriceOld">$93.00</span> $26.00</p>

                                            </div>
                                        </div>
                                    </div>   /.row 

                                </article>
                                <hr class="hide-on-med-and-up" />

                                <article>

                                    <div class="row">
                                        <div class="col s12">
                                            <div class="col s12 m5 l4">

                                                <div class="cartItemThumb" style="background-image: url(images/products/12.jpg); "></div>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons tiny">star</i>
                                                ( <span class="totalRatings">1010</span> )

                                            </div>
                                            <div class="col s12 m7 l8 cartItemInfo ">
                                                <h5 class="cartItemTitle" >
                                                    Casual Genuine Leather Trainers Comfortable Men Sport Shoes Breathable Sneakers
                                                </h5>
                                                <p class="cartItemSize">Size: 8 <span class="cartItemColor">Color: red</span></p>                                

                                                <p class="cartItemQuantity">Quantity: 8 </p>                                

                                                <p class="cartItemShipping">Shipping : $8.00</p>
                                                <p class="cartItemArrival">Estimated Arrival: Sep 2 - Sep 5</p>
                                                <p class="cartItemPrice"><span class="cartItemPriceOld">$93.00</span> $26.00</p>

                                            </div>
                                        </div>
                                    </div>   /.row 

                                </article>
                                <hr class="hide-on-med-and-up" />

                            </div>   /.col 
                        </aside>
                        <aside id="orderSummary">
                            <div class="col m12 l3">
                                <div class="row">
                                    <div class="col s12  grey lighten-4">
                                        <h2 class="orderSummary cartPageHeading">ORDER SUMMARY</h2>
                                        <h6 class="orderSummary itemTotal">Item Total: <span class="pull-right">$49</span></h6>
                                        <h6 class="orderSummary estimatedShipping">Estimated Shipping: <span class="pull-right">$49</span></h6>
                                        <h5 class="orderSummary orderTotal">ORDER TOTAL: <span class="pull-right">$49</span></h5>
                                    </div>
                                    <div class="col s12"></div>
                                </div>
                                <div class="row">
                                    <a href="checkout.php" class="waves-effect waves-light btn btn-block">Confirm Order</a>
                                </div>

                            </div>   /.col 
                        </aside>

                    </div>  /.row 

                </div>  /.col 
            </div>  /.row 

    </div>
</div>-->

<?php include_once('inc/footer.php'); ?>
