<?php include_once('inc/header.php'); ?>
<div class="container">
    <div class="row">
        <div class="pageContentSection">
            <div class="col s10  m12 offset-s1 ">
                <h4 class="onPageTitle">Suppliers</h4>

                <h6 class="contentTopic">

                </h6>
                <blockquote class="topicDetails">
                    We want you to be completely satisfied with your purchase on Bixop and our family of apps. Our 5-Star Guarantee means we're committed to providing customers with the most delightful shopping experience possible. We're always available and happy to help with any questions. If you need to request a refund, please contact our Customer Support Team within 30 days of delivery.
                </blockquote>

                <h6 class="contentTopic">
                    How does the return policy work?
                </h6>
                <blockquote class="topicDetails">
                    To start a return, the button below will direct you to your Order History page. From there, simply choose the relevant item(s) and click on ‘Customer Support’ to communicate directly with our customer service team.

                    View Order History

                    <ul>
                        <li>
                            Customers may request a refund on any order within 30 days of delivery.</li>
                        <li>
                            Our customer service team strives to respond to all requests within 72 hours or less. We're always available to help!</li>
                        <li>
                            Once an order has been placed, if you change your mind, you may cancel your order within the Order History section before it's been shipped.</li>
                        <li>
                            After an item has shipped, if you’re unsatisfied for any reason, you can always contact us for further assistance.
                        </li>
                    </ul>
                </blockquote>

                <h6 class="contentTopic">
                    How will I receive my refund?
                </h6>
                <blockquote class="topicDetails">
                    <ul>
                        <li>
                            Refunds are processed back to the original payment method used to purchase the order.
                        </li>
                        <li>
                            Typically, you can expect your payment provider to credit you for your refund within 14 business days. If you are having trouble locating your refund, we recommend contacting your payment provider for further assistance.</li>
                        <li>If you used OXXO or Boleto, Ebanx will issue your refund based on your payment method.</li>
                        <li>
                            All Bixop gift cards are valid as a one-time use only. We unfortunately cannot credit Bixop gift cards back to your account once they’ve been used.
                        </li>
                        <li>Bixop does not provide reimbursement for costs related to return shipping, customs fees, taxes, postal fees or VAT related costs.
                        </li>
                    </ul>
                </blockquote>

                <h6 class="contentTopic">
                    How are my promotions applied?
                </h6>
                <blockquote class="topicDetails">
                    <ul>
                        <li>The balance and expiration date of a Bixop gift card may be viewed in the Rewards section of your account. It will automatically be applied to the cost of your next order for all eligible products.
                        <li>Bixop gift cards cannot be applied to shipping costs. Gift cards expire at midnight on the expiration date.
                        </li>
                        <li>Promotions are limited time offers and valid based on the terms of the offer. Promotions are not reissued once they have expired.
                        </li>
                    </ul>
                </blockquote>


            </div>

        </div>
    </div>
</div>

<?php include_once('inc/footer.php'); ?>
