<?php include_once('inc/header.php'); ?>

<section class="page" id="latestProduct">
    <div class="row">
        <div class="col s11 m10 l10 xl12 offset-l1 offset-m1 offset-s1">

            <div class="row">

                <div class="col s6 m4 l3">
                    <div class="card">
                        <a href="single.php">
                            <div class="card-image  " style="background: url(images/products/1.jpg) left top no-repeat; background-size: contain;   ">
                                <span class="discout">-84%</span>
                                <span class="almostGone">Almost Gone!</span>
                            </div>
                        </a>
                        <div class="card-content">
                            <p class="price">
                                <i class="fa fa-dollar"></i>20 
                                <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                <small class="sold">10k+ bought this</small>
                            </p>
                        </div>
                    </div>
                </div>               

                <div class="col s6 m4 l3">
                    <div class="card">
                        <div class="card-image  " style="background: url(images/products/10.jpg) left top no-repeat; background-size: contain;   ">
                            <span class="discout">-84%</span>
                        </div>
                        <div class="card-content">
                            <p class="price">
                                <i class="fa fa-dollar"></i>20 
                                <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                <small class="sold">10k+ bought this</small>
                            </p>
                        </div>
                    </div>
                </div>  

                <div class="col s6 m4 l3">
                    <div class="card">
                        <div class="card-image  " style="background: url(images/products/11.jpg) left top no-repeat; background-size: contain;   ">
                            <span class="discout">-84%</span>
                        </div>
                        <div class="card-content">
                            <p class="price">
                                <i class="fa fa-dollar"></i>20 
                                <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                <small class="sold">10k+ bought this</small>
                            </p>
                        </div>
                    </div>
                </div>                


                <div class="col s6 m4 l3">
                    <div class="card">
                        <div class="card-image  " style="background: url(images/products/9.jpg) left top no-repeat; background-size: contain;   ">
                            <span class="discout">-84%</span>
                        </div>
                        <div class="card-content">
                            <p class="price">
                                <i class="fa fa-dollar"></i>20 
                                <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                <small class="sold">10k+ bought this</small>
                            </p>
                        </div>
                    </div>
                </div>   

                <div class="col s6 m4 l3">
                    <div class="card">
                        <div class="card-image  " style="background: url(images/products/10.jpg) left top no-repeat; background-size: contain;   ">
                            <span class="discout">-84%</span>
                        </div>
                        <div class="card-content">
                            <p class="price">
                                <i class="fa fa-dollar"></i>20 
                                <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                <small class="sold">10k+ bought this</small>
                            </p>
                        </div>
                    </div>
                </div>               

                <div class="col s6 m4 l3">
                    <div class="card">
                        <div class="card-image  " style="background: url(images/products/11.jpg) left top no-repeat; background-size: contain;   ">
                            <span class="discout">-84%</span>
                            <span class="almostGone">Almost Gone!</span>
                        </div>
                        <div class="card-content">
                            <p class="price">
                                <i class="fa fa-dollar"></i>20 
                                <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                <small class="sold">10k+ bought this</small>
                            </p>
                        </div>
                    </div>
                </div>  

                <div class="col s6 m4 l3">
                    <div class="card">
                        <div class="card-image  " style="background: url(images/products/5.jpg) left top no-repeat; background-size: contain;   ">
                            <span class="discout">-84%</span>
                        </div>
                        <div class="card-content">
                            <p class="price">
                                <i class="fa fa-dollar"></i>20 
                                <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                <small class="sold">10k+ bought this</small>
                            </p>
                        </div>
                    </div>
                </div>                


                <div class="col s6 m4 l3">
                    <div class="card">
                        <div class="card-image  " style="background: url(images/products/13.jpg) left top no-repeat; background-size: contain;   ">
                            <span class="discout">-84%</span>
                            <span class="almostGone">Almost Gone!</span>
                        </div>
                        <div class="card-content">
                            <p class="price">
                                <i class="fa fa-dollar"></i>20 
                                <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                <small class="sold">10k+ bought this</small>
                            </p>
                        </div>
                    </div>
                </div>                       

                <div class="col s6 m4 l3">
                    <div class="card">
                        <div class="card-image  " style="background: url(images/products/1.jpg) left top no-repeat; background-size: contain;   ">
                            <span class="discout">-84%</span>
                            <span class="almostGone">Almost Gone!</span>
                        </div>
                        <div class="card-content">
                            <p class="price">
                                <i class="fa fa-dollar"></i>20 
                                <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                <small class="sold">10k+ bought this</small>
                            </p>
                        </div>  
                    </div>
                </div>               

                <div class="col s6 m4 l3">
                    <div class="card">
                        <div class="card-image  " style="background: url(images/products/2.jpg) left top no-repeat; background-size: contain;   ">
                            <span class="discout">-84%</span>
                        </div>
                        <div class="card-content">
                            <p class="price">
                                <i class="fa fa-dollar"></i>20 
                                <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                <small class="sold">10k+ bought this</small>
                            </p>
                        </div>
                    </div>
                </div>  

                <div class="col s6 m4 l3">
                    <div class="card">
                        <div class="card-image  " style="background: url(images/products/4.jpg) left top no-repeat; background-size: contain;   ">
                            <span class="discout">-84%</span>
                        </div>
                        <div class="card-content">
                            <p class="price">
                                <i class="fa fa-dollar"></i>20 
                                <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                <small class="sold">10k+ bought this</small>
                            </p>
                        </div>
                    </div>
                </div>                


                <div class="col s6 m4 l3">
                    <div class="card">
                        <div class="card-image  " style="background: url(images/products/5.jpg) left top no-repeat; background-size: contain;   ">
                            <span class="discout">-84%</span>
                        </div>
                        <div class="card-content">
                            <p class="price">
                                <i class="fa fa-dollar"></i>20 
                                <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                <small class="sold">10k+ bought this</small>
                            </p>
                        </div>
                    </div>
                </div>   

                <div class="col s6 m4 l3">
                    <div class="card">
                        <div class="card-image  " style="background: url(images/products/14.jpg) left top no-repeat; background-size: contain;   ">
                            <span class="discout">-84%</span>
                        </div>
                        <div class="card-content">
                            <p class="price">
                                <i class="fa fa-dollar"></i>20 
                                <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                <small class="sold">10k+ bought this</small>
                            </p>
                        </div>
                    </div>
                </div>               

                <div class="col s6 m4 l3">
                    <div class="card">
                        <div class="card-image  " style="background: url(images/products/15.jpg) left top no-repeat; background-size: contain;   ">
                            <span class="discout">-84%</span>
                            <span class="almostGone">Almost Gone!</span>
                        </div>
                        <div class="card-content">
                            <p class="price">
                                <i class="fa fa-dollar"></i>20 
                                <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                <small class="sold">10k+ bought this</small>
                            </p>
                        </div>
                    </div>
                </div>  

                <div class="col s6 m4 l3">
                    <div class="card">
                        <div class="card-image  " style="background: url(images/products/13.jpg) left top no-repeat; background-size: contain;   ">
                            <span class="discout">-84%</span>
                        </div>
                        <div class="card-content">
                            <p class="price">
                                <i class="fa fa-dollar"></i>20 
                                <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                <small class="sold">10k+ bought this</small>
                            </p>
                        </div>
                    </div>
                </div>                


                <div class="col s6 m4 l3">
                    <div class="card">
                        <div class="card-image  " style="background: url(images/products/10.jpg) left top no-repeat; background-size: contain;   ">
                            <span class="discout">-84%</span>
                            <span class="almostGone">Almost Gone!</span>
                        </div>
                        <div class="card-content">
                            <p class="price">
                                <i class="fa fa-dollar"></i>20 
                                <small class="oldPrice"><i class="fa fa-dollar "></i>120 </small> <br/>
                                <small class="sold">10k+ bought this</small>
                            </p>
                        </div>
                    </div>
                </div>     
            </div>    
            <div class="row">
                <div class="col s10 offset-s1 m4 offset-m5 l3 offset-l5" style="padding-bottom: 30px; overflow: hidden;">
                    <strong class="loadingMore">Loading More...</strong> <img width="16" height="16" src="images/loader.gif" alt="Loading more products..." />
                </div>
            </div>



        </div>
    </div>
</section>


<?php include_once('inc/footer.php'); ?>
