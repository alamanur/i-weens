<?php include_once('inc/header.php'); ?>
<?php $cart = [1]; ?>
<div class="cartPage">
    <div class="container">

        <div class="row">
            <div class="col s10 offset-s1 l12">
                <div class="row">
                    <aside id="shipTo">
                        <div class="col m12 l5">
                            <div class="row">
                                <div class="col s12 ">

                                        <nav class="">
                                            <div class="nav-wrapper">
                                               <?php include_once 'inc/ship-menu.php'; ?>
                                            </div>
                                        </nav>

                                        <h2 class="orderSummary cartPageHeading">SHIP TO </h2>
                                        <!--SHIP TO form -->
                                        

                                        <div class="row">
                                            <form class="col s12" action="payment.php" method="post">
                                                <div class="row">
                                                    <div class="input-field col s10">
                                                        <input  id="first_name" type="text" class="validate">
                                                        <label for="first_name"> Name <span class="required red-text" >*</span></label>
                                                    </div>
                                                    <div class="input-field col s10">
                                                        <input id="email" type="text" class="validate">
                                                        <label for="email">Email <span class="required red-text" >*</span></label>
                                                    </div>


                                                    <div class="input-field col s10">
                                                        <input id="phoneNumber" type="text" class="validate">
                                                        <label for="phoneNumber">Phone <span class="required red-text" >*</span></label>
                                                    </div>

                                                    <div class="input-field col s10">
                                                        <input id="address2" type="text" class="validate">
                                                        <label for="address2">Address <span class="required red-text" >*</span></label>
                                                    </div>

                                                    <div class="input-field col s10">
                                                        <input id="zipCode" type="text" class="validate">
                                                        <label for="zipCode">Zip Code <span class="required red-text" >*</span></label>
                                                    </div>

                                                    <div class="input-field col s10">
                                                        <input id="city" type="text" class="validate">
                                                        <label for="city">City<span class="required red-text" >*</span></label>
                                                    </div>

                                                    <div class="col s12">
                                                        <button type="submit" class="waves-effect waves-light btn orange lighten-2">Next</button>
                                                    </div>
                                                </div>
                                            </form> 


                                        </div>

                                   
                                </div>

                            </div>


                        </div>  <!-- /.col -->
                    </aside>

                    <aside id="shoppingCart">
                        <div class="col m12 l7 offset-l0" style="background: none;">
                            <h2 class="shipPageHeading white lighten-3">SHOPPING CART</h2>
                            <article>

                                <div class="row">
                                    <div class="col s12">
                                        <div class="col s12 m5 l3">

                                            <div class="shipItemThumb" style="background-image: url(images/products/13.jpg); "></div>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons tiny">star</i>

                                        </div>
                                        <div class="col s12 m7 l9 cartItemInfo ">
                                            <h5 class="cartItemTitle" >
                                                Casual Genuine Leather Trainers Comfortable Men Sport Shoes Breathable Sneakers
                                            </h5>
                                            <p class="cartItemSize">Size: 8 <span class="cartItemColor">Color: red</span></p>                                

                                            <p class="shipQuantity">Quantity: 8 </p>      
                                            <p class="cartItemShipping">Shipping : $8.00</p>
                                            <p class="cartItemArrival">Estimated Arrival: Sep 2 - Sep 5</p>
                                            <p class="cartItemPrice"><span class="cartItemPriceOld">$93.00</span> $26.00</p>

                                        </div>
                                    </div>
                                </div>  <!-- /.row -->

                            </article>
                            <hr class="hide-on-med-and-up" />

                            <article>

                                <div class="row">
                                    <div class="col s12">
                                        <div class="col s12 m5 l3">

                                            <div class="shipItemThumb" style="background-image: url(images/products/11.jpg); "></div>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons tiny">star</i>


                                        </div>
                                        <div class="col s12 m7 l9 cartItemInfo ">
                                            <h5 class="cartItemTitle" >
                                                Casual Genuine Leather Trainers Comfortable Men Sport Shoes Breathable Sneakers
                                            </h5>
                                            <p class="cartItemSize">Size: 8 <span class="cartItemColor">Color: red</span></p>                                
                                            <p class="shipQuantity">Quantity: 8 </p>      
                                            <p class="cartItemShipping">Shipping : $8.00</p>
                                            <p class="cartItemArrival">Estimated Arrival: Sep 2 - Sep 5</p>
                                            <p class="cartItemPrice"><span class="cartItemPriceOld">$93.00</span> $26.00</p>

                                        </div>
                                    </div>
                                </div>  <!-- /.row -->

                            </article>
                            <hr class="hide-on-med-and-up" />

                            <article>

                                <div class="row">
                                    <div class="col s12">
                                        <div class="col s12 m5 l3">

                                            <div class="shipItemThumb" style="background-image: url(images/products/12.jpg); "></div>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons light-blue-text tiny">star</i>
                                            <i class="material-icons tiny">star</i>


                                        </div>
                                        <div class="col s12 m7 l9 cartItemInfo ">
                                            <h5 class="cartItemTitle" >
                                                Casual Genuine Leather Trainers Comfortable Men Sport Shoes Breathable Sneakers
                                            </h5>
                                            <p class="cartItemSize">Size: 8 <span class="cartItemColor">Color: red</span></p>                                
                                            <p class="shipQuantity">Quantity: 8 </p>                                
                                            <p class="cartItemShipping">Shipping : $8.00</p>
                                            <p class="cartItemArrival">Estimated Arrival: Sep 2 - Sep 5</p>
                                            <p class="cartItemPrice"><span class="cartItemPriceOld">$93.00</span> $26.00</p>

                                        </div>
                                    </div>
                                </div>  <!-- /.row -->

                            </article>
                            <hr class="hide-on-med-and-up" />

                        </div>  <!-- /.col -->
                    </aside>

                </div> <!-- /.row -->

            </div> <!-- /.col -->
        </div> <!-- /.row -->

    </div>
</div>

<?php include_once('inc/footer.php'); ?>
