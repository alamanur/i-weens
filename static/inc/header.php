
<!DOCTYPE html>
<html>
    <head>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link  rel="stylesheet" href="css/materialize.min.css"/>
        <link  rel="stylesheet" href="css/font-awesome.min.css"/>
        <link  rel="stylesheet" href="css/pgwslideshow.min.css"/>
        <link  rel="stylesheet" href="css/custom.css"/>
        <link  rel="stylesheet" href="css/responsive.css"/>
        <title>Bixop</title>
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    </head>

    <body id="image">
        <header id="mainHeader">
            <div class="container">
                <div class="row">
                    <div class="col m12 l12">
                        <nav class="nav-extended light-blue lighten-2">
                            <div class="nav-wrapper">
                                <div class="col s6 m6 l4">
                                    <ul id="" class="bannerHeader">
                                        <li><a href="index.php"><img class="responsive-img logoMain" src="images/logo.png" alt="siteLogo" /></a></li>
                                        <!--<li><a href="index.php"><img class="responsive-img apple" src="images/banner_apple.png" alt="siteLogo" /></a></li>-->
                                        <li><a href="index.php"><img class="responsive-img google  hide-on-small-and-down" src="images/banner_google.png" alt="siteLogo" /></a></li>
                                    </ul>
                                </div>
                                <div class="col s6 m6 l4">

                                    <div class="searchButton">
                                        <i class="fa fa-search hide-on-small-and-down"></i>
                                        <input type="text"  placeholder="Search....">    

                                    </div>
                                </div>

                                <a href="#" data-activates="mobile-demo" class="button-collapse mobile-menu-icon"><i class="material-icons">menu</i></a>
                                <div class="col m6 l4 hide-on-med-and-down">

                                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                                        <li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li class="dropdownMenu" >
                                            <a href="#profile"> <i class="fa fa-caret-down"></i> Ingenious!</a>
                                            <ul>
                                                <li><a href="profile.php"><i class="fa fa-user"></i> Profile</a></li>
                                                <li><a href="cart.php"><i class="fa fa-shopping-bag"></i> Shopping Cart</a></li>
                                                <li><a href="rewards.php"><i class="fa fa-gift"></i> Rewards</a></li>
                                                <li><a href="apps.php"><i class="fa fa-mobile-phone"></i> Mobile Apps</a></li>
                                                <li><a href="transection.php"><i class="fa fa-history"></i> Order History</a></li>
                                                <li><a href="settings.php"><i class="fa fa-wrench"></i> Settings</a></li>
                                                <li><a href="help.php"><i class="fa fa-question"></i> Help</a></li>
                                                <li><a href="logout.php"><i class="fa fa-sign-out"></i> Logout</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#cart.php" id="cartCount" ><img class="responsive-img shoppingCartImg" src="images/shopping-cart.png" alt="shopping cart" /><sup>7</sup></a></li>
                                        <li><a href="notifications.php"><i class="fa  fa-bell fa-2x"></i><sup>5</sup></a></li>
                                    </ul>
                                </div>
                                <ul class="side-nav" id="mobile-demo">
                                    <li><a href=""><i class="material-icons">system_update_alt</i>Download App</a></li>
                                    <li><a href=""><i class="material-icons">home</i>Home</a></li>
                                    <li><a href=""><i class="material-icons">shopping_cart</i>Shopping Cart</a></li>
                                    <li><a href=""><i class="material-icons">subject</i>Order History</a></li>
                                    <li><a href=""><i class="material-icons">help</i>Help</a></li>
                                    <li><a href="">Settings</a></li>
                                    <li><a href="">Terms of Service</a></li>
                                    <li><a href="">Privacy Policy</a></li>
                                    <li><a href="">Return Policy</a></li>
                                    <li><a href="">Logout</a></li>
                                </ul>
                            </div>

                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <section id="navTabs">
            <div class="container">
                <div class="row">
                    <div class="col s12 m10 offset-m1">
                        <ul class="mainMenu">
                            <li class="tab col "><a href="index.php">Outlet</a></li>
                            <li class="tab col active "><a href="index.php">Latest</a></li>
                            <li class="tab col "><a href="index.php">Recently Viewed</a></li>
                            <li class="tab col "><a href="index.php">Fashion</a></li>
                            <li class="tab col "><a href="index.php">Gadgets</a></li>
                            <li class="tab col "><a href="index.php">Shoes</a></li>
                            <li class="tab col "><a href="index.php">Home Decor</a></li>
                            <li class="tab col "><a href="index.php">Hobbies</a></li>
                            <li class="tab col "><a href="index.php">Tops</a></li>
                            <li class="tab col "><a href="index.php">Wallets & Bags</a></li>
                            <li class="tab col "><a href="index.php">Accessories</a></li>
                            <li class="tab col "><a href="index.php">Watches</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!--Customer Support start --> 

        <section class="customarSupport">
            <!--<button class="">Customer Support</button>-->
            <ul class="hide-on-small-only">
                <li>C</li>
                <li>u</li>
                <li>s</li>
                <li>t</li>
                <li>o</li>
                <li>m</li>
                <li>e</li>
                <li>r</li>
                <li><br></li>
                <li>S</li>
                <li>u</li>
                <li>p</li>
                <li>p</li>
                <li>o</li>
                <li>r</li>
                <li>t</li>
            </ul>
            <i class="material-icons " title="If you need help, click here?"> help </i>

        </section>

        <!--Customer Support End --> 

        <section id="popupCart">
            <!--<i class="fa fa-times cartClose"></i>-->

            <div class="container">
                <div class="row">
                    <div class="col s12">
                        <div class="row">
                            <div class="col s12">
                                <div class="noOfItemOnCart">
                                    <i class="fa fa-shopping-bag fa-2x"></i> 4 <?php echo strtoupper('items'); ?> 
                                    <button id="cartCloseBtn" class="waves-effect waves-light pull-right grey lighten-2">Close</button>
                                </div>                                

                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col s12">
                                <table class="centered bordered" > 

                                    <tbody>
                                        <tr>
                                            <td width="15%">
                                                <img style="background: url(images/products/11.jpg) top left / contain no-repeat;" class="responsive-img popupImg" />
                                            </td>
                                            <td width="60%">Bashundhara Toilet Tissue</td>
                                            <td width="20%">৳ 100   </td>
                                            <td width="5%"><i class="fa fa-times"></i></td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <img style="background: url(images/products/12.jpg) top left / contain no-repeat;" class="responsive-img popupImg" />
                                            </td>
                                            <td>HRC Clevedon Tea 400 gm (Free Mug Offer)</td>
                                            <td>৳ 100   </td>

                                            <td><i class="fa fa-times"></i></td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <img style="background: url(images/products/11.jpg) top left / contain no-repeat;" class="responsive-img popupImg" />
                                            </td>
                                            <td>NeoCare Baby Diaper (Belt System) L (7-18 kg)</td>
                                            <td>৳ 100   </td>

                                            <td><i class="fa fa-times"></i></td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <img style="background: url(images/products/12.jpg) top left / contain no-repeat;" class="responsive-img popupImg" />
                                            </td>
                                            <td>Onion (Red Indian Piyaj)</td>
                                            <td>৳ 100   </td>

                                            <td><i class="fa fa-times"></i></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img style="background: url(images/products/11.jpg) top left / contain no-repeat;" class="responsive-img popupImg" />
                                            </td>
                                            <td>NeoCare Baby Diaper (Belt System) L (7-18 kg)</td>
                                            <td>৳ 100   </td>

                                            <td><i class="fa fa-times"></i></td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <img style="background: url(images/products/12.jpg) top left / contain no-repeat;" class="responsive-img popupImg" />
                                            </td>
                                            <td>Onion (Red Indian Piyaj)</td>
                                            <td>৳ 100   </td>

                                            <td><i class="fa fa-times"></i></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img style="background: url(images/products/11.jpg) top left / contain no-repeat;" class="responsive-img popupImg" />
                                            </td>
                                            <td>NeoCare Baby Diaper (Belt System) L (7-18 kg)</td>
                                            <td>৳ 100   </td>

                                            <td><i class="fa fa-times"></i></td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <img style="background: url(images/products/12.jpg) top left / contain no-repeat;" class="responsive-img popupImg" />
                                            </td>
                                            <td>Onion (Red Indian Piyaj)</td>
                                            <td>৳ 100   </td>

                                            <td><i class="fa fa-times"></i></td>
                                        </tr>




                                    </tbody>
                                </table>

                            </div>
                        </div> <!-- /.row -->

                        <div class="row" style="">
                            <div class="col s12">
                                <a href="cart.php" class="waves-effect waves-light btn viewCartFixedBtn orange">View Cart</a>     

                            </div>
                        </div>

                    </div> <!-- /.col -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </section> <!-- /#popupCart -->


        <!-- Product Card Start  -->

        <div class="productArea">
