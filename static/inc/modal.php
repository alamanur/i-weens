<!-- Modal Structure -->
<div id="bkashPaymentModal" class="modal modal-fixed-footer" >
    <div class="modal-content">
        <h4 class="center-align">bKash Payment Process</h4>
        <hr />
        <img src="images/cards/bkash-payment.png" alt="payment process on bkash" class="responsive-img" />
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" title="Click outside of the modal to close popup window">Close</a>
    </div>
    

</div>