
</div>

<!-- Product Section End -->
<!--Main Footer Secton--> 
<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col l12 xl10 offset-xl2">
                    <a class="text-lighten-4 left" href="help.php">Need Help?</a>
                    <a class="text-lighten-4 left" href="contact.php">Contact</a>
                    <a class="text-lighten-4 left" href="privacy-policy.php">Privacy Policy</a>
                    <a class="text-lighten-4 left hide-on-small-only" href="terms.php">Terms of Service</a>
                    <a class="text-lighten-4 left hide-on-small-only" href="return.php">Return Policy</a>
                    <a class="text-lighten-4 left" href="supplier.php">Suppliers</a>
                    <a class="text-lighten-4 left hide-on-med-and-down" href="trademark.php">Trademark Protection</a>
                    <span id="footer-like" class=" hide-on-med-and-down" >
                        <div class="fb-like fb_iframe_widget" data-href="https://www.facebook.com/pages/Wish/164302750336862" data-layout="button_count" data-send="false" data-width="90" data-show-faces="false" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=227791440613076&amp;container_width=0&amp;href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FWish%2F164302750336862&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;send=false&amp;show_faces=false&amp;width=90"><span style="vertical-align: bottom; width: 79px; height: 20px;"><iframe name="f4266908c1f40c" width="90px" height="1000px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:like Facebook Social Plugin" src="https://www.facebook.com/v2.2/plugins/like.php?app_id=227791440613076&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FXBwzv5Yrm_1.js%3Fversion%3D42%23cb%3Df3b1ec233e3a07%26domain%3Dwww.wish.com%26origin%3Dhttps%253A%252F%252Fwww.wish.com%252Ff1dc0f24b922b8%26relation%3Dparent.parent&amp;container_width=0&amp;href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FWish%2F164302750336862&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;send=false&amp;show_faces=false&amp;width=90" style="border: none; visibility: visible; width: 79px; height: 20px;" class=""></iframe></span></div>
                    </span>
                </div>
            </div>

        </div>
    </div>
</footer>
<!--Main Footer Secton End --> 

<!-- Modal Include --> 

<?php include_once('inc/modal.php'); ?>

<!-- Modal Include end --> 


<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/pgwslideshow.min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>

<script>
    $(document).ready(function () {
        $(".button-collapse").sideNav();
        $('ul.tabs').tabs();
        $('.scrollspy').scrollSpy();
        $('.modal').modal();
        $('.pgwSlideshow').pgwSlideshow();

        $("#cartCount").click(function () {
            $("#popupCart").css("display", "block");
        });
        $("#cartCloseBtn").click(function () {
            $("#popupCart").css("display", "none");
        });
    });
</script>

</body>
</html>