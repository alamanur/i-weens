<?php include_once('inc/header.php'); ?>

<section class="page" id="rewards">
    <div class="container">
        <div class="row">
            <div class="pageContentSection">
                <div class="col s10 offset-s1 m12">
                    <div class="row">
                        <div class="col s12">
                            <ul class="tabs">
                                <li class="tab col s3"><a href="#gift1"><i class="fa fa-gift"></i> Gift Cards</a></li>
                                <li class="tab col s3"><a href="#reward1"><i class="fa fa-crosshairs"></i> Rewards</a></li>
                            </ul>
                        </div>
                        <div id="gift1" class="col s12">
                            <h4 class="rewardsTitile">You don't have any gift cards right now.</h4>
                        </div>
                        <div id="reward1" class="col s12">
                            <h4 class="rewardsTitile">You don't have any rewards right now.</h4>
                        
                        </div>
                    </div>


                </div>

            </div> <!-- /.pageContentSection -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</section> <!-- /.page -->
<?php include_once('inc/footer.php'); ?>
