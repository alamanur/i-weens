<?php include_once('inc/header.php'); ?>

<section class="page" id="apps">
    <div class="container">
        <div class="row">
            <div class="pageContentSection light-blue accent-2">

                <div class="col s10  offset-s1  m12">
                    <div class="row">
                        <div class="col s12 m6  center-block">
                            <div class="row">
                                <div class="col s12">                                
                                    <a style="font-size: 26px;" class=" light-blue waves-effect waves-light btn-large">Your mobile mall is waiting.</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12">                                
                                    <h5 class="white-text">Shop millions of quality products at deep, deep discounts. Download now!</h5>
                                    <a href="" target="_blank"><img src="images/banner_google.png" alt="app on google play store" width="40%" /></a>
                                    <a href="" target="_blank"><img src="images/banner_apple.png" alt="app on google play store" width="40%" /></a>

                                </div>
                            </div>

                        </div>
                        <div class="col s12 m6">
                            <figure class="appImg">
                                <img src="images/android-app.png" alt="android app design for mobile" width="100%" />
                            </figure>
                        </div>
                    </div>



                </div>
            </div> <!-- /.pageContentSection -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</section> <!-- /.page -->
<?php include_once('inc/footer.php'); ?>
