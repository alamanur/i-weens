<?php include_once('inc/header.php'); ?>

<section class="page" id="profile">
    <div class="container">
        <div class="row">
            <div class="pageContentSection">
                <div class="col s10 offset-s1 m4 l4 xl3">
                    <h4 class="onPageTitle">Profile Name </h4>
                    <div class="profilePic" style="background: url(images/users/profile.jpg) top left / contain no-repeat; height: 200px; width: 200px;">                  </div>

                    <form action="#" method="post" enctype="multipart/form-data">
                        <div class="file-field input-field">
                            <div class="btn grey lighten-1">
                                <span>Upload</span>
                                <input type="file">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text" placeholder="Profile">
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col s10  offset-s1  m8 l8 xl9">
                    <hr class="hide-on-med-and-up"  />
                    <div class="row">
                        <div class="col s12">
                            <a class="waves-effect waves-light btn white  black-text">4 Items</a>                        
                        </div>
                    </div>
                    <div class="row">
                        <div class="col 24"></div>
                        <div class="col 24"></div>
                        <div class="col 24"></div>
                    </div>


                </div>
            </div> <!-- /.pageContentSection -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</section> <!-- /.page -->
<?php include_once('inc/footer.php'); ?>
