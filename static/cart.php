<?php include_once('inc/header.php'); ?>
<?php $cart = [1]; ?>
<div class="cartPage">
    <div class="container">
        <?php if (empty($cart)): ?>
            <div class="row">
                <div class="col s10 offset-s1  l12">
                    <h2 class="cartPageHeading">SHOPPING CART</h2>
                    <div class="col s10 m6 l4 xl3 offset-s1 offset-m4 offset-l4 offset-xl5">
                        <div class="cartPageIcon">
                            <i class=" grey-text fa fa-cart fa-shopping-cart fa-5x"></i>
                        </div>
                        <h5 class="grey-text cartEmpty">Your cart is empty!</h5>
                        <a class="waves-effect waves-light btn light-blue lighten-2" href="http://localhost/code/ingtech/bixop/index.php">Start Shopping</a>
                    </div>
                </div>
            </div>
        <?php else: ?>
            <div class="row">
                <div class="col s10 offset-s1 l12">
                    <div class="row">
                        <aside id="shoppingCart">
                            <div class="col m12 l9" style="background: none;">
                                <h2 class="cartPageHeading">SHOPPING CART</h2>
                                <article>

                                    <div class="row">
                                        <div class="col s12">
                                            <div class="col s12 m5 l4">

                                                <div class="cartItemThumb" style="background-image: url(images/products/13.jpg); "></div>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons tiny">star</i>
                                                ( <span class="totalRatings">1010</span> )

                                            </div>
                                            <div class="col s12 m7 l8 cartItemInfo ">
                                                <h5 class="cartItemTitle" >
                                                    Casual Genuine Leather Trainers Comfortable Men Sport Shoes Breathable Sneakers
                                                </h5>
                                                <p class="cartItemSize">Size: 8 <span class="cartItemColor">Color: red</span></p>                                

                                                <form>
                                                    Quantity : 
                                                    <select>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                    <img src="images/lodar-bar.gif" alt="quantity lodar" />
                                                </form>
                                                <p class="cartItemShipping">Shipping : $8.00</p>
                                                <p class="cartItemArrival">Estimated Arrival: Sep 2 - Sep 5</p>
                                                <p class="cartItemPrice"><span class="cartItemPriceOld">$93.00</span> $26.00</p>
                                                <a href=""><i class="fa fa-times"></i></a>
                                            </div>
                                        </div>
                                    </div>  <!-- /.row -->

                                </article>
                                <hr class="hide-on-med-and-up" />

                                <article>

                                    <div class="row">
                                        <div class="col s12">
                                            <div class="col s12 m5 l4">

                                                <div class="cartItemThumb" style="background-image: url(images/products/11.jpg); "></div>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons tiny">star</i>
                                                ( <span class="totalRatings">1010</span> )

                                            </div>
                                            <div class="col s12 m7 l8 cartItemInfo ">
                                                <h5 class="cartItemTitle" >
                                                    Casual Genuine Leather Trainers Comfortable Men Sport Shoes Breathable Sneakers
                                                </h5>
                                                <p class="cartItemSize">Size: 8 <span class="cartItemColor">Color: red</span></p>                                

                                                <form>
                                                    Quantity : <select>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </form>
                                                <p class="cartItemShipping">Shipping : $8.00</p>
                                                <p class="cartItemArrival">Estimated Arrival: Sep 2 - Sep 5</p>
                                                <p class="cartItemPrice"><span class="cartItemPriceOld">$93.00</span> $26.00</p>
                                                <a href=""><i class="fa fa-times"></i></a>
                                            </div>
                                        </div>
                                    </div>  <!-- /.row -->

                                </article>
                                <hr class="hide-on-med-and-up" />

                                <article>

                                    <div class="row">
                                        <div class="col s12">
                                            <div class="col s12 m5 l4">

                                                <div class="cartItemThumb" style="background-image: url(images/products/12.jpg); "></div>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons light-blue-text tiny">star</i>
                                                <i class="material-icons tiny">star</i>
                                                ( <span class="totalRatings">1010</span> )

                                            </div>
                                            <div class="col s12 m7 l8 cartItemInfo ">
                                                <h5 class="cartItemTitle" >
                                                    Casual Genuine Leather Trainers Comfortable Men Sport Shoes Breathable Sneakers
                                                </h5>
                                                <p class="cartItemSize">Size: 8 <span class="cartItemColor">Color: red</span></p>                                

                                                <form>
                                                    Quantity : <select>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </form>
                                                <p class="cartItemShipping">Shipping : $8.00</p>
                                                <p class="cartItemArrival">Estimated Arrival: Sep 2 - Sep 5</p>
                                                <p class="cartItemPrice"><span class="cartItemPriceOld">$93.00</span> $26.00</p>
                                                <a href=""><i class="fa fa-times"></i></a>
                                            </div>
                                        </div>
                                    </div>  <!-- /.row -->

                                </article>
                                <hr class="hide-on-med-and-up" />

                            </div>  <!-- /.col -->
                        </aside>
                        <aside id="orderSummary">
                            <div class="col m12 l3">
                                <div class="row">
                                    <div class="col s12  grey lighten-4">
                                        <h2 class="orderSummary cartPageHeading">ORDER SUMMARY</h2>
                                        <h6 class="orderSummary itemTotal">Item Total: <span class="pull-right">$49</span></h6>
                                        <h6 class="orderSummary estimatedShipping">Estimated Shipping: <span class="pull-right">$49</span></h6>
                                        <h5 class="orderSummary orderTotal">ORDER TOTAL: <span class="pull-right">$49</span></h5>
                                    </div>
                                    <div class="col s12"></div>
                                </div>
                                <div class="row">
                                    <a href="checkout.php" class="waves-effect waves-light btn btn-block">Checkout</a>
                                </div>

                            </div>  <!-- /.col -->
                        </aside>

                    </div> <!-- /.row -->

                </div> <!-- /.col -->
            </div> <!-- /.row -->

        <?php endif; ?>
    </div>
</div>

<?php include_once('inc/footer.php'); ?>
