<?php include_once('inc/header.php'); ?>
<section class="page" id="settings">
    <div class="container">
        <div class="row">
            <div class="pageContentSection">
                <div class="col s10  m8 offset-s1">
                    <h4 class="onPageTitle">Settings</h4>
                    <section class="emailSettings">
                        <h5 >Email Settings</h5>
                        <div class="col s12">
                            <p>These settings let you control how much we notify you through email. Click the checkbox to save your preference.</p>
                            <p>
                                <input type="checkbox" class="filled-in" id="filled-in-box" />
                                <label for="filled-in-box">Block all notifications</label>
                            </p>
                            <p>
                                <input type="checkbox" class="filled-in" id="filled-in-box2" />
                                <label for="filled-in-box2">Block product recommendation notifications</label>
                            </p>
                            <p>
                                <input type="checkbox" class="filled-in" id="filled-in-box3" />
                                <label for="filled-in-box3">Block information about deals or discounts</label>
                            </p>
                            <p>
                                <input type="checkbox" class="filled-in" id="filled-in-box4" />
                                <label for="filled-in-box4">Block news on products I added to my wishlists</label>
                            </p>
                            <p>
                                <input type="checkbox" class="filled-in" id="filled-in-box5" />
                                <label for="filled-in-box5">Block price alert notifications</label>
                            </p>
                            <p>
                                <input type="checkbox" class="filled-in" id="filled-in-box6" />
                                <label for="filled-in-box6">Block product available notifications</label>
                            </p>
                            <p>
                                <input type="checkbox" class="filled-in" id="filled-in-box7" />
                                <label for="filled-in-box7">Block reminders for expiring gift cards</label>
                            </p>
                            <p>
                                <input type="checkbox" class="filled-in" id="filled-in-box8" />
                                <label for="filled-in-box8">Limit to weekly digest emails</label>
                            </p>
                        </div>
                    </section>
                    <section class="facebookSettings">
                        <h5>Facebook Communication Settings</h5>
                        <div class="col s12">
                            <p>Connect with Facebook to help personalize your experience and share products with friends</p>
                            <a href=""><img src="images/connect-with-facebook.png" alt="Connect with facebook" /></a>
                        </div>
                    </section>

                    <section class="localSettings">
                        <h5>Locale Settings</h5>
                        <div class="col s12">
                            <p>Language of preference for your Wish.</p>
                            <form action="" method="post">
                                <p>
                                    <input class="with-gap" name="localSet" type="radio" id="bangla" checked="checked"  />
                                    <label for="bangla">Bangla</label>
                                </p>
                                <p>
                                    <input class="with-gap" name="localSet" type="radio" id="english"  />
                                    <label for="english">English</label>
                                </p>
                                <p>
                                    <input class="with-gap" name="localSet" type="radio" id="espanol"  />
                                    <label for="espanol">Español</label>
                                </p>
                                <p>
                                    <input class="with-gap" name="localSet" type="radio" id="porgugues"  />
                                    <label for="porgugues">Português</label>
                                </p>
                                <p>
                                    <input class="with-gap" name="localSet" type="radio" id="italiano"  />
                                    <label for="italiano">Italiano</label>
                                </p>
                                <p>
                                    <input class="with-gap" name="localSet" type="radio" id="deutsch"  />
                                    <label for="deutsch">Deutsch</label>
                                </p>
                                <p>
                                    <input class="with-gap" name="localSet" type="radio" id="dansk"  />
                                    <label for="dansk">Dansk</label>
                                </p>
                            </form>
                        </div>
                    </section>

                </div>
                <div class="col s10 m4 offset-s1">
                    <?php include_once('inc/side-menu.php'); ?>

                </div>
            </div>
        </div>
    </div>

</section>

<?php include_once('inc/footer.php'); ?>
