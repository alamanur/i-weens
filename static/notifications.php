<?php include_once('inc/header.php'); ?>
<div class="container">
    <div class="row">
        <div class="pageContentSection">
            <div class="col s10  m8 offset-s1 ">
                <h4 class="onPageTitle">All Notifications</h4>
                
                <div class="row">
                    <div class="col m3 l2">
                        <i class="fa fa-bell-o fa-5x"></i>
                    </div>
                    <div class="col m9 l10">
                        <strong>Good news Ingenious, this item you want is on sale for $5 today... </strong>
                        <p><em class="grey lighten-2" style="padding: 7px;">27 days ago</em></p>
                        <a class="waves-effect waves-light btn pull-right light-blue">View Collection </a>
                    </div>
                </div>

            </div>
            <div class="col s10 m4 offset-s1">    

                <div class="notificationRightBar">
                    <div class="col s12">
                        <h5 class="onPageTitle" style="text-indent: 0px; font-size: 18px; font-weight: 700">What you'll find here</h5>

                    </div>

                    <div class="col s12">
                        <div class="row">
                            <div class="col s3 m12 l3">
                                <i class="fa fa-edit fa-3x"></i>
                            </div>
                            <div class="col s9 m12 l9">
                                <strong>Recommendations</strong>
                                <p>Build wishlists from products your friends and family recommend.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12">
                        <div class="row">
                            <div class="col s3 m12 l3">

                                <i class="fa fa-gift fa-3x"></i>
                            </div>
                            <div class="col s9 m12 l9">
                                <strong>Gift Cards</strong>
                                <p>Receive gift cards for products on your wishlists.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12">
                        <div class="row">
                            <div class="col s3 m12 l3">
                                <i class="fa fa-users fa-3x"></i>
                            </div>
                            <div class="col s9 m12 l9">
                                <strong>New Friends</strong>
                                <p>Get notified when a new friend starts to follow you.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('inc/footer.php'); ?>
