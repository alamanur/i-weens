<?php include_once('inc/header.php'); ?>
<div class="container">
    <div class="row">
        <div class="pageContentSection">
            <div class="col s10  m8 offset-s1 ">
                <h4 class="onPageTitle">Contact Us</h4>
                <div class="location">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3652.2062235169165!2d90.40919071449515!3d23.740024384594538!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b88ac125889f%3A0xc4b8f3c4e35d3bd1!2s1+Shantinagar+Bazar+Rd%2C+Dhaka!5e0!3m2!1sen!2sbd!4v1502177324521" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <address class="address">
                    <h6><i class="fa fa-location-arrow"></i> Headquarters: <small>153/A-1,  Shantinagar Bazar Road</small> </h6>
                </address>
                <address class="address">
                    <h6><i class="fa fa-mobile"></i> Customer Support number: <small> +88-01869848426 </small> </h6>
                </address>
                

            </div>
            <div class="col s10 m4 offset-s1">
                <?php include_once('inc/side-menu.php'); ?>

            </div>
        </div>
    </div>
</div>

<?php include_once('inc/footer.php'); ?>
